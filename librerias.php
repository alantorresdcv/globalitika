<?php 
echo "
		<!-- ***** JAVASCRIPTS ***** -->
		<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js'></script>
  		<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js'></script>
  		<script src='//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js'></script>  
		<!-- Libraries -->
		<script src='$Link/assets/plugins/jquery.min.js'></script>
		<script src='$Link/assets/plugins/bootstrap/popper.min.js'></script>
		<!-- Plugins -->
		<script src='$Link/assets/plugins/bootstrap/bootstrap.min.js'></script>
		<script src='$Link/assets/plugins/appear.min.js'></script>
		<script src='$Link/assets/plugins/easing.min.js'></script>
		<script src='$Link/assets/plugins/retina.min.js'></script>
		<script src='$Link/assets/plugins/countdown.min.js'></script>
		<script src='$Link/assets/plugins/imagesloaded.pkgd.min.js'></script>
		<script src='$Link/assets/plugins/isotope.pkgd.min.js'></script>
		<script src='$Link/assets/plugins/jarallax/jarallax.min.js'></script>
		<script src='$Link/assets/plugins/jarallax/jarallax-video.min.js'></script>
		<script src='$Link/assets/plugins/jarallax/jarallax-element.min.js'></script>
		<script src='$Link/assets/plugins/magnific-popup/magnific-popup.min.js'></script>
		<!-- Place your API Key in 'YOUR_API_KEY'
		You can get Google API key here: 'https://developers.google.com/maps/documentation/javascript/get-api-key' -->
		<script src='https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=initMap'></script>
		<script src='$Link/assets/plugins/gmaps.min.js'></script>
		<!-- Scripts -->
		<script src='$Link/assets/js/functions.js'></script>
		<script src='$Link/assets/vendors/highlight.js'></script>
    	<script src='$Link/assets/js/app.js'></script>
    <script src='$Link/assets/owlcarousel/owl.carousel.js'></script>
    <script src='https://unpkg.com/aos@2.3.1/dist/aos.js'></script>
      

		<script type='text/javascript'>
			function buscar() {
				$('.mostrarbuscar').removeClass('mostrarbuscar').addClass('ocultarbuscar');
				$('.ocultarbuscar').removeClass('ocultarbuscar').addClass('mostrarbuscar');
				$('.iconobuscar').removeClass('iconobuscar').addClass('iconobuscar2');
				setTimeout(function(){ document.getElementById('submit').disabled = false; }, 1000);
				
			}
			function item1(){
        document.getElementById('item1').style.zIndex = '3';
        document.getElementById('item2').style.zIndex = '2';
        document.getElementById('item3').style.zIndex = '1';
        document.getElementById('pitem3').style.opacity = '0';
        document.getElementById('pitem2').style.opacity = '0';
        document.getElementById('pitem1').style.opacity = '1';
        document.getElementById('item1').style.margin = '1% 0 0 0';
        document.getElementById('item2').style.margin = '8% 0% 0% 2%';
        document.getElementById('item3').style.margin = '15% 0 0 4%';
    }
    function item2(){
        document.getElementById('item1').style.zIndex = '1';
        document.getElementById('item2').style.zIndex = '3';
        document.getElementById('item3').style.zIndex = '2';
        document.getElementById('pitem3').style.opacity = '0';
        document.getElementById('pitem2').style.opacity = '1';
        document.getElementById('pitem1').style.opacity = '0';
        document.getElementById('item2').style.margin = '1% 0 0 0';
        document.getElementById('item3').style.margin = '8% 0% 0% 2%';
        document.getElementById('item1').style.margin = '15% 0 0 4%';
    }
    function item3(){
        document.getElementById('item1').style.zIndex = '2';
        document.getElementById('item2').style.zIndex = '1';
        document.getElementById('item3').style.zIndex = '3';
        document.getElementById('pitem3').style.opacity = '1';
        document.getElementById('pitem2').style.opacity = '0';
        document.getElementById('pitem1').style.opacity = '0';
        document.getElementById('item3').style.margin = '1% 0 0 0';
        document.getElementById('item1').style.margin = '8% 0% 0% 2%';
        document.getElementById('item2').style.margin = '15% 0 0 4%';
    }
    function cerrar(){
        document.getElementById('fixed').style.display = 'none';
        document.getElementById('registrar').style.display = 'none';
        document.getElementById('fixedclave').style.display = 'none';
        document.getElementById('fixedclave').style.display = 'none';
        document.getElementById('fondobonito').style.display = 'none';
        document.getElementById('cambiarfoto').style.display = 'none';
        document.getElementById('cambiarnombre').style.display = 'none';
        document.getElementById('cancelarmembresia').style.display = 'none';
    }
    function cerrarclave(){
        document.getElementById('fixedclave').style.display = 'none';
    }
    function cerrarregistrar(){
        document.getElementById('registrar').style.display = 'none';
        document.getElementById('fondobonito').style.display = 'none';
    }
    function abrir(){
        document.getElementById('fixed').style.display = 'initial';
        document.getElementById('registrar').style.display = 'none';
        document.getElementById('fixedclave').style.display = 'none';
    }
    function cambiarfoto(){
        document.getElementById('cambiarfoto').style.display = 'initial';
        document.getElementById('registrar').style.display = 'none';
        document.getElementById('fixedclave').style.display = 'none';
        document.getElementById('fixed').style.display = 'none';
    }
    function cambiarnombre(){
        document.getElementById('cambiarnombre').style.display = 'initial';
    }
    function abrircancelarmembresia(){
        document.getElementById('cancelarmembresia').style.display = 'initial';
    }
    function cerrarfoto(){
        document.getElementById('cambiarfoto').style.display = 'none';
        document.getElementById('registrar').style.display = 'none';
        document.getElementById('fixedclave').style.display = 'none';
        document.getElementById('fixed').style.display = 'none';
    }
    function abrirclave(){
        document.getElementById('fixed').style.display = 'none';
        document.getElementById('fixedclave').style.display = 'initial';
        document.getElementById('registrar').style.display = 'none';
    }
    function abrirregistrar(){
         document.getElementById('fixed').style.display = 'none';
         document.getElementById('fixedclave').style.display = 'none';
        document.getElementById('registrar').style.display = 'initial';
        document.getElementById('fondobonito').style.display = 'initial';
    }
    function cerraro(){
        document.getElementById('fixedos').style.display = 'none';
    }

    function abriro(){
        document.getElementById('fixedos').style.display = 'initial';
    }
    function abriropciones(){
        document.getElementById('opcionesuser').style.display = 'initial';
    }
    function cerraropciones(){
        document.getElementById('opcionesuser').style.display = 'none';
    }
    function memiz1(){
        document.getElementById('memiz1').style.boxShadow =' 0px 0px 10px 0px grey';
        document.getElementById('memiz2').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('memiz3').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('cuadromembre1').style.backgroundColor = '#f0a52d';
        document.getElementById('cuadromembre2').style.backgroundColor = 'transparent';
        document.getElementById('cuadromembre3').style.backgroundColor = 'transparent';
        document.getElementById('divmemder1').style.display = 'inline-block';
        document.getElementById('divmemder2').style.display = 'none';
        document.getElementById('divmemder3').style.display = 'none';
        document.getElementById('divmemder0').style.display = 'none';

    }
    function memiz2(){
        document.getElementById('memiz2').style.boxShadow =' 0px 0px 10px 0px grey';
        document.getElementById('memiz1').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('memiz3').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('cuadromembre1').style.backgroundColor = 'transparent';
        document.getElementById('cuadromembre2').style.backgroundColor = '#4492ff';
        document.getElementById('cuadromembre3').style.backgroundColor = 'transparent';
        document.getElementById('divmemder1').style.display = 'none';
        document.getElementById('divmemder2').style.display = 'inline-block';
        document.getElementById('divmemder3').style.display = 'none';
        document.getElementById('divmemder0').style.display = 'none';

    }
    function memiz3(){
        document.getElementById('memiz3').style.boxShadow =' 0px 0px 10px 0px grey';
        document.getElementById('memiz2').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('memiz1').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('cuadromembre1').style.backgroundColor = 'transparent';
        document.getElementById('cuadromembre2').style.backgroundColor = 'transparent';
        document.getElementById('cuadromembre3').style.backgroundColor = '#3b3b3b';
        document.getElementById('divmemder1').style.display = 'none';
        document.getElementById('divmemder2').style.display = 'none';
        document.getElementById('divmemder3').style.display = 'inline-block';
        document.getElementById('divmemder0').style.display = 'none';

    }
    function estudiantememiz1(){
        document.getElementById('estudiantememiz1').style.boxShadow =' 0px 0px 10px 0px grey';
        document.getElementById('estudiantememiz2').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('estudiantememiz3').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('estudiantecuadromembre1').style.backgroundColor = '#f0a52d';
        document.getElementById('estudiantecuadromembre2').style.backgroundColor = 'transparent';
        document.getElementById('estudiantecuadromembre3').style.backgroundColor = 'transparent';
        document.getElementById('estudiantedivmemder1').style.display = 'inline-block';
        document.getElementById('estudiantedivmemder2').style.display = 'none';
        document.getElementById('estudiantedivmemder3').style.display = 'none';
        document.getElementById('estudiantedivmemder0').style.display = 'none';

    }
    function estudiantememiz2(){
        document.getElementById('estudiantememiz1').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('estudiantememiz2').style.boxShadow =' 0px 0px 10px 0px grey';
        document.getElementById('estudiantememiz3').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('estudiantecuadromembre1').style.backgroundColor = 'transparent';
        document.getElementById('estudiantecuadromembre2').style.backgroundColor = '#f0a52d';
        document.getElementById('estudiantecuadromembre3').style.backgroundColor = 'transparent';
        document.getElementById('estudiantedivmemder1').style.display = 'none';
        document.getElementById('estudiantedivmemder2').style.display = 'inline-block';
        document.getElementById('estudiantedivmemder3').style.display = 'none';
        document.getElementById('estudiantedivmemder0').style.display = 'none';

    }
    function estudiantememiz3(){
        document.getElementById('estudiantememiz1').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('estudiantememiz2').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('estudiantememiz3').style.boxShadow =' 0px 0px 10px 0px grey';
        document.getElementById('estudiantecuadromembre1').style.backgroundColor = 'transparent';
        document.getElementById('estudiantecuadromembre2').style.backgroundColor = 'transparent';
        document.getElementById('estudiantecuadromembre3').style.backgroundColor = '#f0a52d';
        document.getElementById('estudiantedivmemder1').style.display = 'none';
        document.getElementById('estudiantedivmemder2').style.display = 'none';
        document.getElementById('estudiantedivmemder3').style.display = 'inline-block';
        document.getElementById('estudiantedivmemder0').style.display = 'none';

    }
    function profesionalmemiz1(){
        document.getElementById('profesionalmemiz1').style.boxShadow =' 0px 0px 10px 0px grey';
        document.getElementById('profesionalmemiz2').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('profesionalmemiz3').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('profesionalcuadromembre1').style.backgroundColor = '#4492ff';
        document.getElementById('profesionalcuadromembre2').style.backgroundColor = 'transparent';
        document.getElementById('profesionalcuadromembre3').style.backgroundColor = 'transparent';
        document.getElementById('profesionaldivmemder1').style.display = 'inline-block';
        document.getElementById('profesionaldivmemder2').style.display = 'none';
        document.getElementById('profesionaldivmemder3').style.display = 'none';
        document.getElementById('profesionaldivmemder0').style.display = 'none';

    }
    function profesionalmemiz2(){
        document.getElementById('profesionalmemiz1').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('profesionalmemiz2').style.boxShadow =' 0px 0px 10px 0px grey';
        document.getElementById('profesionalmemiz3').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('profesionalcuadromembre1').style.backgroundColor = 'transparent';
        document.getElementById('profesionalcuadromembre2').style.backgroundColor = '#4492ff';
        document.getElementById('profesionalcuadromembre3').style.backgroundColor = 'transparent';
        document.getElementById('profesionaldivmemder1').style.display = 'none';
        document.getElementById('profesionaldivmemder2').style.display = 'inline-block';
        document.getElementById('profesionaldivmemder3').style.display = 'none';
        document.getElementById('profesionaldivmemder0').style.display = 'none';

    }
    function profesionalmemiz3(){
        document.getElementById('profesionalmemiz1').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('profesionalmemiz2').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('profesionalmemiz3').style.boxShadow =' 0px 0px 10px 0px grey';
        document.getElementById('profesionalcuadromembre1').style.backgroundColor = 'transparent';
        document.getElementById('profesionalcuadromembre2').style.backgroundColor = 'transparent';
        document.getElementById('profesionalcuadromembre3').style.backgroundColor = '#4492ff';
        document.getElementById('profesionaldivmemder1').style.display = 'none';
        document.getElementById('profesionaldivmemder2').style.display = 'none';
        document.getElementById('profesionaldivmemder3').style.display = 'inline-block';
        document.getElementById('profesionaldivmemder0').style.display = 'none';

    }
    function empresarialmemiz1(){
        document.getElementById('empresarialmemiz1').style.boxShadow =' 0px 0px 10px 0px grey';
        document.getElementById('empresarialmemiz2').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('empresarialmemiz3').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('empresarialcuadromembre1').style.backgroundColor = '#4492ff';
        document.getElementById('empresarialcuadromembre2').style.backgroundColor = 'transparent';
        document.getElementById('empresarialcuadromembre3').style.backgroundColor = 'transparent';
        document.getElementById('empresarialdivmemder1').style.display = 'inline-block';
        document.getElementById('empresarialdivmemder2').style.display = 'none';
        document.getElementById('empresarialdivmemder3').style.display = 'none';
        document.getElementById('empresarialdivmemder0').style.display = 'none';

    }
    function empresarialmemiz2(){
        document.getElementById('empresarialmemiz1').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('empresarialmemiz2').style.boxShadow =' 0px 0px 10px 0px grey';
        document.getElementById('empresarialmemiz3').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('empresarialcuadromembre1').style.backgroundColor = 'transparent';
        document.getElementById('empresarialcuadromembre2').style.backgroundColor = '#4492ff';
        document.getElementById('empresarialcuadromembre3').style.backgroundColor = 'transparent';
        document.getElementById('empresarialdivmemder1').style.display = 'none';
        document.getElementById('empresarialdivmemder2').style.display = 'inline-block';
        document.getElementById('empresarialdivmemder3').style.display = 'none';
        document.getElementById('empresarialdivmemder0').style.display = 'none';

    }
    function empresarialmemiz3(){
        document.getElementById('empresarialmemiz1').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('empresarialmemiz2').style.boxShadow =' 0px 0px 0px 0px grey';
        document.getElementById('empresarialmemiz3').style.boxShadow =' 0px 0px 10px 0px grey';
        document.getElementById('empresarialcuadromembre1').style.backgroundColor = 'transparent';
        document.getElementById('empresarialcuadromembre2').style.backgroundColor = 'transparent';
        document.getElementById('empresarialcuadromembre3').style.backgroundColor = '#4492ff';
        document.getElementById('empresarialdivmemder1').style.display = 'none';
        document.getElementById('empresarialdivmemder2').style.display = 'none';
        document.getElementById('empresarialdivmemder3').style.display = 'inline-block';
        document.getElementById('empresarialdivmemder0').style.display = 'none';

    }
    function atras(){
        document.getElementById('select1').style.display = 'none';
        document.getElementById('select2').style.display = 'none';
        document.getElementById('select3').style.display = 'none';
        document.getElementById('estudiante1').style.display = 'none';
        document.getElementById('estudiante2').style.display = 'none';
        document.getElementById('estudiante3').style.display = 'none';
        document.getElementById('profesional1').style.display = 'none';
        document.getElementById('profesional2').style.display = 'none';
        document.getElementById('profesional3').style.display = 'none';
        document.getElementById('empresarial1').style.display = 'none';
        document.getElementById('empresarial2').style.display = 'none';
        document.getElementById('empresarial3').style.display = 'none';

        document.getElementById('select').style.display = 'inline-block';
    }
    function estudiante(){
        document.getElementById('select').style.display = 'none';
        document.getElementById('select1').style.display = 'inline-block';
    }
    function profesional(){
        document.getElementById('select').style.display = 'none';
        document.getElementById('select2').style.display = 'inline-block';
    }
    function empresarial(){
        document.getElementById('select').style.display = 'none';
        document.getElementById('select3').style.display = 'inline-block';
    }
    function estudiante1(){
        document.getElementById('select1').style.display = 'none';
        document.getElementById('estudiante1').style.display = 'inline-block';
    }
    function estudiante2(){
        document.getElementById('select1').style.display = 'none';
        document.getElementById('estudiante2').style.display = 'inline-block';
    }
    function estudiante3(){
        document.getElementById('select1').style.display = 'none';
        document.getElementById('estudiante3').style.display = 'inline-block';
    }
    function profesional1(){
        document.getElementById('select2').style.display = 'none';
        document.getElementById('profesional1').style.display = 'inline-block';
    }
    function profesional2(){
        document.getElementById('select2').style.display = 'none';
        document.getElementById('profesional2').style.display = 'inline-block';
    }
    function profesional3(){
        document.getElementById('select2').style.display = 'none';
        document.getElementById('profesional3').style.display = 'inline-block';
    }
    function empresarial1(){
        document.getElementById('select3').style.display = 'none';
        document.getElementById('empresarial1').style.display = 'inline-block';
    }
    function empresarial2(){
        document.getElementById('select3').style.display = 'none';
        document.getElementById('empresarial2').style.display = 'inline-block';
    }
    function empresarial3(){
        document.getElementById('select3').style.display = 'none';
        document.getElementById('empresarial3').style.display = 'inline-block';
    }
			function body(){
				//document.getElementById('position').style.position = 'relative';
				$('.owl-carousel').owlCarousel({
			    loop:true,
			    margin:30,
			    nav:true,
			    //autoplay:true,
    			autoplayTimeout:10000,
    			autoplayHoverPause:true,
			    responsive:{
			        0:{
			            items:1
			        },
			        600:{
			            items:1
			        },
			        1000:{
			            items:3
			        }
			    }
			})
				AOS.init();
			}
			function  Login() 
      {
        var Correo = $('#email').val();
        var Clave = $('#password').val();
        var params = { 'Correo' : Correo, 'Clave' : Clave};
        $.ajax({
            url: '$Link/Register/verificar.php',
            type: 'POST',
            data: params,
            success: function (data) {
                console.log(data);
              if (data == 2) 
              {
                $('#msg').html('Bienvenido');
                location.href = '$Link/cuenta';
              }
              else if (data == 3) 
              {
                alert('debe verificar su correo electronico revice su bandeja de entrada');
              } else {
                $('#msg').html('Clave Incorrecta');
                setTimeout(function(){ document.getElementById('msg').style.opacity = '0'; }, 5000);
              }
            }  
        });
      }
      function  Registrar() 
      { 
            document.getElementById('butregister').innerHTML= 'Guardando...';
            var formData = new FormData(document.getElementById('formuploadajaxadd'));
            formData.append('dato', 'valor');
            //formData.append(f.attr('name'), $(this)[0].files[0]);
            $.ajax({
                url: '$Link/Register/guardar.php',
                type: 'post',
                dataType: 'html',
                data: formData,
                cache: false,
                contentType: false,
         processData: false
            })
                .done(function(res){
                    console.log(res);
                if (res==1) {
                    alert('Todos los campos son requeridos por favor Verifique he intente nuevamente');
                    document.getElementById('butregister').innerHTML= 'Guardar';
                }
                else{alert('registrado satisfactoriamente');
                document.getElementById('butregister').innerHTML= 'Guardado';
                setTimeout(function(){ document.getElementById('butregister').innerHTML= 'Registrar'; }, 3000);
                location.href = '$Link/verificar';
                }
                });
        }
        function  contactanos() 
      { 
        
            var formData = new FormData(document.getElementById('formuploadajaxcontactanos'));
            formData.append('dato', 'valor');
            //formData.append(f.attr('name'), $(this)[0].files[0]);
            $.ajax({
                url: 'contactanos.php',
                type: 'post',
                dataType: 'html',
                data: formData,
                cache: false,
                contentType: false,
         processData: false
            })
                .done(function(res){
                    console.log(res);
                if (res==1) {
                    alert('Todos los campos son requeridos por favor Verifique he intente nuevamente');
                }
                else{alert('mensaje enviado satisfactoriamente');
                
                }
                });
        }
        function  subirfoto() 
      { 
        
            var formData = new FormData(document.getElementById('formuploadajaxaddfoto'));
            formData.append('dato', 'valor');
            //formData.append(f.attr('name'), $(this)[0].files[0]);
            $.ajax({
                url: '../Register/actualizarfoto.php',
                type: 'post',
                dataType: 'html',
                data: formData,
                cache: false,
                contentType: false,
         processData: false
            })
                .done(function(res){
                    console.log(res);
                if (res==1) {
                    alert('Debe seleccionar una foto');

                }
                else{alert('Foto guardada');
            location.href = '$Link/cuenta';}
                });
        }
        function  actualizaruser()
      { 
        
            var formData = new FormData(document.getElementById('formuploadajaxaddnombre'));
            formData.append('dato', 'valor');
            //formData.append(f.attr('name'), $(this)[0].files[0]);
            $.ajax({
                url: '../Register/actualizaruser.php',
                type: 'post',
                dataType: 'html',
                data: formData,
                cache: false,
                contentType: false,
         processData: false
            })
                .done(function(res){
                    console.log(res);
                if (res==1) {
                    alert('');

                }
                else{alert('Cambios Guardados');
            location.href = '$Link/cuenta';}
                });
        }
        function  cancelarmembresia()
      { 
        
            var formData = new FormData(document.getElementById('formcancelarmembresia'));
            formData.append('dato', 'valor');
            //formData.append(f.attr('name'), $(this)[0].files[0]);
            $.ajax({
                url: '../Register/cancelarmembresia.php',
                type: 'post',
                dataType: 'html',
                data: formData,
                cache: false,
                contentType: false,
         processData: false
            })
                .done(function(res){
                    console.log(res);
                if (res==1) {
                    alert('');

                }
                else{alert('Cambios Guardados');
            location.href = '$Link/cuenta';}
                });
        }
    
   function  validaEmail() 
      {
        var params = { 'Correo' : $('#correo').val() };
        $.ajax({
            url: 'administradores/validarmail.php',
            type: 'POST',
            data: params,
            success: function (data) {
              if (data == 1) 
              {
                document.getElementById('smsmail').innerHTML= 'este correo ya existe';
                document.getElementById('but').disabled = true;
              } else {
                document.getElementById('smsmail').innerHTML= 'este correo ya existe';
                document.getElementById('but').disabled = false;
              }
            }  
        });
      }
      function validaEmailregister(){
        var Correo = $('#Correoregister').val();
        var params = { 'Correo' : Correo};
        $.ajax({
            url: '$Link/Register/validarmail.php',
            type: 'POST',
            data: params,
            success: function (data) {
              if (data == 1) 
              {
                document.getElementById('smsmailregistrar').innerHTML= 'este correo ya existe';
                document.getElementById('but').disabled = true;
              } else {
                document.getElementById('smsmailregistrar').innerHTML= '';
                document.getElementById('but').disabled = false;
              }
            }  
        });
      }
      function recuperar(){
        var Correo = $('#emailclave').val();
        var params = { 'Correo' : Correo};
        $.ajax({
            url: '$Link/Register/recuperarclave.php',
            type: 'POST',
            data: params,
            success: function (data) {
              if (data == 1) 
              {
                $('#smsmailregister').html('Su clave se ha enviado');
                
              } else {
                $('#smsmailregister').html('este correo no existe');
                
              }
            }  
        });
      }";?>
      function suscribir(){
        document.getElementById('butsuscribir').innerHTML= 'Guardando...';
        var Correo = $('#correosuscribir').val();
        var params = { 'Correo' : Correo};
         if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(Correo)){  
            <?php echo " 
   $.ajax({
            url: '$Link/Register/suscribir.php',
            type: 'POST',
            data: params,
            success: function (data) {
                
              if (data == 1) 
              {
                alert('Suscrito satisfactoriamente');
                    document.getElementById('butsuscribir').innerHTML= 'Guardado';
                    setTimeout(function(){ 
                    
                    document.getElementById('butsuscribir').innerHTML= 'Guardar';

                }, 3000);

              } 
              else if(data == 2){

                alert('ya se encuentra suscrito');
                setTimeout(function(){ 
                    
                    document.getElementById('butsuscribir').innerHTML= 'Suscrito';
                }, 3000);
              }
              else if(data == 0){
                alert('Debe ingresar un correo');
              }
              
              else{
                alert('error intente nuevamente');
              }
              
            }  
        });
   }
   else{
    alert('La dirección de email ' + Correo + ' es incorrecta!.');
   }
        
      }
      function verclave(){
        var Clave1 = $('#Clave1').val();
        var Clave2 = $('#Clave2').val();
        if (Clave1 == Clave2) {
            document.getElementById('butregister').disabled = false;
            $('#smsmailregister').html('');
        }
        else{
            document.getElementById('butregister').disabled = true;
            $('#smsmailregister').html('las claves no coinciden');
        }
      }
      
      function labelASC(){
        document.getElementById('labelASC').style.border = 'solid 1px #2d7fff';
        document.getElementById('labelASC').style.color = '#2d7fff';
        document.getElementById('labelDESC').style.border = 'solid 1px #b7b7b7';
        document.getElementById('labelDESC').style.color = '#b7b7b7';
        document.getElementById('labelREC').style.border = 'solid 1px #b7b7b7';
        document.getElementById('labelREC').style.color = '#b7b7b7';
        document.getElementById('labelANT').style.border = 'solid 1px #b7b7b7';
        document.getElementById('labelANT').style.color = '#b7b7b7';
      }
      function labelDESC(){
        document.getElementById('labelASC').style.border = 'solid 1px #b7b7b7';
        document.getElementById('labelASC').style.color = '#b7b7b7';
        document.getElementById('labelDESC').style.border = 'solid 1px #2d7fff';
        document.getElementById('labelDESC').style.color = '#2d7fff';
        document.getElementById('labelREC').style.border = 'solid 1px #b7b7b7';
        document.getElementById('labelREC').style.color = '#b7b7b7';
        document.getElementById('labelANT').style.border = 'solid 1px #b7b7b7';
        document.getElementById('labelANT').style.color = '#b7b7b7';
      }
      function labelREC(){
        document.getElementById('labelREC').style.border = 'solid 1px #2d7fff';
        document.getElementById('labelREC').style.color = '#2d7fff';
        document.getElementById('labelDESC').style.border = 'solid 1px #b7b7b7';
        document.getElementById('labelDESC').style.color = '#b7b7b7';
        document.getElementById('labelASC').style.border = 'solid 1px #b7b7b7';
        document.getElementById('labelASC').style.color = '#b7b7b7';
        document.getElementById('labelANT').style.border = 'solid 1px #b7b7b7';
        document.getElementById('labelANT').style.color = '#b7b7b7';
      }
      function labelANT(){
        document.getElementById('labelANT').style.border = 'solid 1px #2d7fff';
        document.getElementById('labelANT').style.color = '#2d7fff';
        document.getElementById('labelDESC').style.border = 'solid 1px #b7b7b7';
        document.getElementById('labelDESC').style.color = '#b7b7b7';
        document.getElementById('labelREC').style.border = 'solid 1px #b7b7b7';
        document.getElementById('labelREC').style.color = '#b7b7b7';
        document.getElementById('labelASC').style.border = 'solid 1px #b7b7b7';
        document.getElementById('labelASC').style.color = '#b7b7b7';
      }

      function investigacion(){
        document.getElementById('tbody').innerHTML = \"<div class='buscando'>Buscando...</div>\";
        setTimeout(function(){  
            
            var formData = new FormData(document.getElementById('investigacion'));
            formData.append('dato', 'valor');
            //formData.append(f.attr('name'), $(this)[0].files[0]);
            $.ajax({
                url: '$Link/investigacion/consultas.php',
                type: 'post',
                dataType: 'html',
                data: formData,
                cache: false,
                contentType: false,
         processData: false
            })
                .done(function(res){
                if (res!=0) {
                    document.getElementById('tbody').innerHTML = res;
                }
            else{
                document.getElementById('tbody').innerHTML = res;
            }
            });
                }, 100);
        
      }
        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var nuevafoto = $('#file-upload').val();
                    if (nuevafoto!='') {
                    document.getElementById('file-preview-zone').style.backgroundImage = \"url('\" + e.target.result + \"')\";
                    document.getElementById('subirfotoperfil').innerHTML= nuevafoto;
                     }
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        var fileUpload = document.getElementById('file-upload');
        fileUpload.onchange = function (e) {
            readFile(e.srcElement);
        }
        function membresia(){
            location.href = '$Link/membresias';
        }
		</script>
";
?>