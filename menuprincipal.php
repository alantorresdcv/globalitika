<?php require('cuenta/consultas.php') ?>
<div class="fondoblanco">
	<div class="container">
				<div class="arriba">
					<div class="divbuscar hidden-phone  inline-block">
							<form method="GET" action="<?php echo $Link; ?>/investigacion">
		      					<div class="col-md-6 inline-block selectbuscar ocultarbuscar" >

			      				<select id="buscar" name="Titulo" class="selectpicker col-md-12 center-block" data-show-subtext="true" data-live-search="true">
			      						<option selected style="color:transparent !important;">Buscar aqui</option>
								<?php

								$con = connect();
								$consulta = "SELECT * FROM l32ddiasfbafbiatreiasdfw WHERE  Estatus='1' AND Fecha <= now() ORDER BY Fecha DESC";
								$resultado = mysqli_query($con , $consulta);
								$contador=0;

								while($misdatos = mysqli_fetch_assoc($resultado)){ $contador++;?>
								<option data-subtext="<?php echo $misdatos["Autor"]; ?>"><?php echo $misdatos["Titulo"]; ?></option>
								<?php }?>
								</select>
							</div>
							<div class="col-md-1 inline-block cursor">
									<button id="submit" type="submit" disabled><label onclick="buscar()" class="iconobuscar cursor"><i class="fa fa-search"></i></label></button>
								</div>
							</form>

		      		</div>
		      		<div class="divbuscar hidden-tablet hidden-desktop">
							<form method="GET" action="<?php echo $Link; ?>/investigacion">
		      					<div class="inline-block selectbuscar buscar movil" >

			      				<select id="buscar" name="Titulo" class="selectpicker col-md-12 center-block" data-show-subtext="true" data-live-search="true">
			      						<option selected style="color:transparent !important;">Buscar aqui</option>
								<?php

								$con = connect();
								$consulta = "SELECT * FROM l32ddiasfbafbiatreiasdfw WHERE  Estatus='1' AND Fecha <= now() ORDER BY Fecha DESC";
								$resultado = mysqli_query($con , $consulta);
								$contador=0;

								while($misdatos = mysqli_fetch_assoc($resultado)){ $contador++;?>
								<option data-subtext="<?php echo $misdatos["Autor"]; ?>"><?php echo $misdatos["Titulo"]; ?></option>
								<?php }?>
								</select>
							</div>
							<div class="inline-block cursor">
									<button id="submit" type="submit"><label onclick="buscar()" class="iconobuscar2 cursor"><i class="fa fa-search"></i></label></button>
								</div>
							</form>

		      		</div>
		      		<div class="sociales">
		      			<i class="iconosociales fab fa-facebook"></i>
		      			<i class="iconosociales fab fa-twitter"></i>
		      			<i class="iconosociales fab fa-linkedin"></i>
		      			<?php
		      			if (isset($_SESSION['Nombre'])) {
		      			echo "
		      			<div onmouseover='abriropciones()' onclick='abriropciones()' onclick='abriropciones()' class='fotoperfil cursor'  style='background-image: url($Foto);'>
		      			</div>
		      			<div id='opcionesuser' onmouseover='abriropciones()' onclick='abriropciones()' onmouseout='cerraropciones()'>
		      				<ul>
		      					<a class='cursor' href='$Link/cuenta'><li><i class='fas fa-user-circle'></i><span>Mi cuenta</span></li></a>
		      					<a class='cursor' href='$Link/cuenta'><li><i class='far fa-question-circle'></i><span>Ayuda</span></li></a>
		      					<a class='cursor' href='$Link/Register/cerrarsesion.php'><li><i class='fas fa-sign-out-alt'></i><span>Cerrar Sesión</span></li></a>
		      				</ul>

		      			</div>";
		      		}

		      		else{
		      			echo "
		      			<img onmouseover='abriropciones()' onclick='abriropciones()'  class='fotoperfil cursor' src='$Link/assets/images/user.png'>
		      			<div id='opcionesuser' onmouseover='abriropciones()' onclick='abriropciones()' onmouseout='cerraropciones()'>
		      				<ul>

		      					<a onclick='abrir()' class='cursor'><li><i class='fas fa-sign-in-alt'></i><span>Iniciar sesión</span></li></a>
		      					<a onclick='abrirregistrar()' class='cursor'><li><i class='fas fa-edit'></i><span>Registrar</span></li></a>
		      				</ul>

		      			</div>";
		      			}
		      			?>


		      		</div>

				</div>
			</div>
			<header>
				<!-- navbar -->
				<nav class='navbar'><!-- add 'navbar-dark/navbar-grey/navbar-transparent/navbar-transparent-dark' -->
					<div class='container'>
						<a class='navbar-brand' href="<?php echo $Link; ?>">
						<img src="../assets/images/logo-globalitika.png" alt="Logo Globalitika">
						</a>
						<ul class="nav"><!-- add 'dropdown-dark/dropdown-grey/dropdown-transparent/dropdown-transparent-dark' -->
							<!-- Basic link -->
							<li class="nav-item">
								<a class="nav-link active" href="<?php echo $Link; ?>" >Home</a>
							</li>

							<!-- Dropdown -->
							<li class="nav-item nav-megadropdown">
							<a class="nav-link" href="<?php echo $Link; ?>/investigacion">Investigación</a>
							<div class="mega-menu">
								<div class="row">
									<!-- Mega menu column 1 -->
									<div class="col-12 col-lg-6" style="padding: 0 50px;">
										<h6 class="heading-uppercase margin-bottom-20">Categorias</h6>
										<ul class="list-unstyled">
											<?php
											$categorias=mysql_query("SELECT * FROM coewerw9hfwefouwefsdf ORDER BY Nombre_Categoria");
										while($fila=mysql_fetch_assoc($categorias)){
											@$ID=$fila["ID_Categoria"];
					                        @$Nombre=$fila["Nombre_Categoria"];
					                       echo" <li><a href='$Link/investigacion/?Categoria=$ID'>$Nombre</a></li>";
					                   }
					                         ?>
										</ul>
									</div>
									<!-- Mega menu column 2 -->
									<div class="col-12 col-lg-6" style="padding: 0 50px;">
										<h6 class="heading-uppercase margin-bottom-20">Temas populares</h6>
										<ul class="list-unstyled">
											<?php
											$temas=mysql_query("SELECT * FROM tr02j0wer82350j2asdw ORDER BY Nombre_Tema");
										while($fila=mysql_fetch_assoc($temas)){
											@$ID=$fila["ID_Tema"];
					                        @$Nombre=$fila["Nombre_Tema"];
					                       echo" <li><a href='$Link/investigacion/?Tema=$ID'>$Nombre</a></li>";
					                   }
					                         ?>
										</ul>
									</div>

								</div><!-- end row -->
							</div>
						</li>

							<!-- Sub Dropdown -->
							<li class="nav-item ">
								<a class="nav-link" href="<?php echo"$Link/"; ?>membresias">Membresías</a>
							</li>

							<!-- Mega Dropdown -->
							<li class="nav-item ">
								<a class="nav-link" href="<?php echo"$Link/#Nosotros"; ?>">Sobre nosotros</a>
							</li>
							<li class="nav-item ">
								<a class="nav-link" href="<?php echo"$Link/#sectioncontacto"; ?>">Contacto</a>
							</li>
						</ul><!-- end nav -->
						<!-- Nav Toggle button -->
						<button class="nav-toggle-btn">
				            <span class="lines"></span>
				        </button><!-- toggle button will show when screen resolution is less than 992px -->
					</div><!-- end container -->
				</nav><!-- end navbar -->
			</header>

			<!-- Scroll to top button -->
			<div class="scrolltotop">
				<a class="button-circle button-circle-sm button-circle-dark" href="#"><i class="ti-arrow-up"></i></a>
			</div>
			<!-- end Scroll to top button -->
			</div>
			<div id='fixed' style="top: 0;display: none;">
			<div class='container'>
				<div id="fondosesion">
					<div class="col-md-1 inline-block"></div>
					<div class="col-md-5 inline-block" style="padding: 50px 5px;">
						<h2 class="white">¿No tienes una cuenta?</h2>
						<p class="white font17">¡Crear una cuenta es gratis! Si quieres tener todos los beneficios de Globalitika, suscribete con la membresia que mas te guste.</p><br><br>
						<button class="botoncrear" onclick='abrirregistrar()'>Crear cuenta</button>
					</div>
					<div class="col-md-5 inline-block divform">

							<label>Ingresa</label>
							<div class="colorbajo" style=""></div><br><br>
							<input autocomplete="off" id="email" type="text" name="" placeholder="Correo">
							<input autocomplete="off" id="password" type="password" name="" placeholder="Contraseña">
							<a class="olvidaste cursor" onclick="abrirclave()">Olvidaste tu contraseña?</a>
							<br> <span id="msg" style="color: red;text-shadow: none;font-size: 15px;position: absolute;margin-top: 10px;margin-left: 20px;"></span>
							<button onclick="Login()" class="ingresar">Ingresar</button>

					</div>
				</div>
				<br><br>
				<p onclick='cerrar()' class='cerrar'>X</p>

			</div>
			</div>
			<div id='fixedclave' style="top: 0;display: none;">
			<div class='container'>
				<div id="fondosesion">
					<div class="col-md-1 inline-block"></div>
					<div class="col-md-5 inline-block" style="padding: 50px 5px;">
						<h2 class="white">¿No tienes una cuenta?</h2>
						<p class="white font17">¡Crear una cuenta es gratis! Si quieres tener todos los beneficios de Globalitika, suscribete con la membresia que mas te guste.</p><br><br>
						<button class="botoncrear" onclick='abrirregistrar()'>Crear cuenta</button>
					</div>
					<div class="col-md-5 inline-block divform">

							<label>Recuperar Contraseña</label>
							<div class="colorbajo" style=""></div><br><br>

							<input autocomplete="off" id="emailclave" type="email" name="" placeholder="Correo">


							<br> <span id="smsmailregister" style="color: red;text-shadow: none;font-size: 15px;margin-top: 10px;margin-left: 20px;"></span>
							<button id="butclave" onclick="recuperar()" class="ingresar">Recuperar Contraseña</button>

					</div>
				</div>
				<br><br>
				<p onclick='cerrarclave()clave' class='cerrar'>X</p>

			</div>
			</div>
			<div class='registrar' id='fondobonito'>
			<div id='registrar' style="top: 0;display: none;">

			<div class='container' style="margin-top: 15vh;">
				<div class="col-md-12 inline-block divform divform2">
						<label style="font-weight: 3
						00;">Regístro de usuarios</label><br>
					</div>
				<div class="divregistrar">
    <form enctype='multipart/form-data' id='formuploadajaxadd' method='POST'>

					<div style="margin-right: -2px;background:transparent;" class="col-md-12 inline-block divform divform2"><br><br><br><br>
							<label for='file-upload' style='display: block;'><div style="width: 35%;margin: 0 auto;" >
								<div class="nuevafoto nuevafotores" id="file-preview-zone" style="background-image: url(<?php echo $Link; ?>/usuarios/fotoperfil/colerico.jpg);">
									<i onclick='cambiarfoto()' class='fas fa-camera camara'></i>
								</div>


							</div></label>
							<input autocomplete="off" type="text" name="Nombre" placeholder="Nombre">
							<input autocomplete="off" type="text" name="Apellido" placeholder="Apellido">
							<label for="file-upload" class="labelfoto text-left" id="subirfotoperfil">Subir foto de perfil</label>
							<input autocomplete="off" style="display: none;" id="file-upload" type="file" name="Foto" accept="image/*" />
						<input autocomplete="off" type="email" id="Correoregister" onkeyup="validaEmailregister()" name="Correo" placeholder="Correo">
						<input autocomplete="off" id="Clave1" type="password" name="Clave" placeholder="Contraseña">
						<br><br>
						<span id="smsmailregistrar" style="color: white;width: 100%;display: block;text-shadow: none;text-align: center;font-size: 15px;margin-top: -20px;margin-bottom: 20px;"></span><br>
							<button type="button" id="butregister" onclick="Registrar()" class="ingresar ingresar2">Registrar</button><br>

					</div>
				</form>
				</div>
				<br><br>
				<p onclick='cerrarregistrar()' class='cerrar'>X</p>

			</div>
			</div>
			</div>
