<?php session_start() ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php 
	include "../Config.php";
	require('../head.php');
	?>

</head>
	<body data-preloader="2" onload="body()">

		<?php require('../menuprincipal.php');?>

		<!-- Home section -->
		<!-- Home section -->
		<div class="section-lg bg-image parallax" style="background-image: url(../assets/images/portada2.jpg);background-position: center;">
			<div class="portada2" style="background: linear-gradient(to bottom, rgba(0, 0, 0, 0.1) 0%, rgba(160, 157, 157, 0.28) 35%, rgba(0, 0, 0, 1) 100%);">
				<div class="text-center">
					<!-- end row -->
				</div><!-- end container -->
			</div>
		</div>	
		<section class="container">
			<div><br><br>
				<h2 class="text-center">TÉRMINOS DE USO Y PRIVACIDAD</h2>
			<div class="colorbajo" style="margin: 0 auto;"></div>
			</div><br><br><br><br><br>
			<div class="hoja">
				<h2>Términos y Condiciones</h2>
				<br><br>
				<p>Lorem ipsum dolor sit amet</p>
				<p>consectetur adipiscing elit.</p>
				<br>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porttitor rhoncus arcu vel sollicitudin. Cras vehicula dui in lobortis pharetra. Aenean at euismod odio. Nulla facilisi. Cras eu felis pharetra, porta est et, fringilla arcu. Praesent at tristique ex. Aliquam blandit pharetra gravida. Pellentesque vestibulum felis a neque efficitur eleifend. Phasellus ut consectetur eros, quis convallis ipsum.</p>
				<br>
				<p>Integer feugiat interdum erat. Pellentesque finibus fermentum leo et ultricies. Nunc ut ipsum rutrum, accumsan odio ut, mollis lectus. Pellentesque nec dictum magna. Nunc ullamcorper eu justo at bibendum. Fusce volutpat, libero ut finibus imperdiet, nibh lacus aliquet leo, id placerat magna purus eu nisi. Nam posuere eros nec sapien vestibulum, volutpat commodo dui sollicitudin. Pellentesque blandit metus nunc, nec ultricies eros posuere vel. Sed ultrices velit elit, et tempus eros pretium ac. Vivamus tristique condimentum sapien vitae tristique. Fusce et ante quis ante semper ultricies. Nam interdum augue in finibus fringilla. Integer feugiat fringilla varius. Praesent fringilla accumsan lorem, mollis facilisis nunc lacinia porttitor. Vivamus commodo magna viverra, dapibus mauris non, posuere felis. Sed tincidunt fringilla tortor vel rhoncus.</p>
				<br>
				<p>Suspendisse vitae enim in neque tempor faucibus. Aliquam dictum ligula nisi, eleifend fringilla diam convallis et. Morbi vel velit in erat iaculis venenatis. Quisque dapibus sapien eros, quis gravida tellus rutrum at. Morbi feugiat tortor ac rutrum maximus. Nullam erat lacus, mattis et metus ut, elementum iaculis ex. Etiam finibus erat non nulla rutrum lobortis. Nullam tellus libero, fermentum eu ipsum sollicitudin, varius ultricies risus. Ut non nisi diam. Nullam eget mauris facilisis orci sodales laoreet at id tortor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec commodo massa mi, ut placerat libero faucibus non. Vivamus quis magna nec dui volutpat imperdiet. Pellentesque aliquam dolor ac orci luctus scelerisque. Suspendisse efficitur laoreet rhoncus.</p>
				<br>
				<p>Etiam malesuada nulla at ultricies pretium. Quisque dapibus posuere rutrum. Vivamus condimentum laoreet est sed viverra. Fusce ac nisi tincidunt, luctus sem a, ultrices sem. Nullam feugiat eros vitae rhoncus convallis. Aenean rutrum efficitur tristique. Etiam vel purus at enim suscipit maximus id non erat. Vestibulum quis tincidunt ipsum. Phasellus at sollicitudin libero. Vestibulum venenatis dignissim facilisis. Aenean sit amet lacus vulputate, sagittis tortor ut, ultrices leo. Donec quis eros a massa semper efficitur. Mauris porta imperdiet nisi, ac condimentum dui sodales eu. Suspendisse auctor magna non massa posuere, sit amet bibendum orci blandit.</p>
				<br>
				<p>Praesent sed urna eget est dignissim efficitur. Nullam metus enim, placerat tincidunt placerat eu, mollis non erat. Sed a ex tempus, lacinia sapien non, sollicitudin nibh. Aenean non mauris libero. Nullam varius aliquam erat vitae consectetur. Donec mollis leo mi, vitae consectetur augue vehicula convallis. Praesent orci ligula, egestas eget hendrerit eget, egestas vel urna. Sed erat libero, rhoncus ut efficitur sit amet, luctus at tellus. In scelerisque, ante quis convallis semper, libero arcu lacinia magna, quis tempor sem enim eu ex. Praesent tincidunt sapien sed neque imperdiet auctor.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porttitor rhoncus arcu vel sollicitudin. Cras vehicula dui in lobortis pharetra. Aenean at euismod odio. Nulla facilisi. Cras eu felis pharetra, porta est et, fringilla arcu. Praesent at tristique ex. Aliquam blandit pharetra gravida. Pellentesque vestibulum felis a neque efficitur eleifend. Phasellus ut consectetur eros, quis convallis ipsum.</p>
				<br>
				<p>Integer feugiat interdum erat. Pellentesque finibus fermentum leo et ultricies. Nunc ut ipsum rutrum, accumsan odio ut, mollis lectus. Pellentesque nec dictum magna. Nunc ullamcorper eu justo at bibendum. Fusce volutpat, libero ut finibus imperdiet, nibh lacus aliquet leo, id placerat magna purus eu nisi. Nam posuere eros nec sapien vestibulum, volutpat commodo dui sollicitudin. Pellentesque blandit metus nunc, nec ultricies eros posuere vel. Sed ultrices velit elit, et tempus eros pretium ac. Vivamus tristique condimentum sapien vitae tristique. Fusce et ante quis ante semper ultricies. Nam interdum augue in finibus fringilla. Integer feugiat fringilla varius. Praesent fringilla accumsan lorem, mollis facilisis nunc lacinia porttitor. Vivamus commodo magna viverra, dapibus mauris non, posuere felis. Sed tincidunt fringilla tortor vel rhoncus.</p>
				<br>
				<p>Suspendisse vitae enim in neque tempor faucibus. Aliquam dictum ligula nisi, eleifend fringilla diam convallis et. Morbi vel velit in erat iaculis venenatis. Quisque dapibus sapien eros, quis gravida tellus rutrum at. Morbi feugiat tortor ac rutrum maximus. Nullam erat lacus, mattis et metus ut, elementum iaculis ex. Etiam finibus erat non nulla rutrum lobortis. Nullam tellus libero, fermentum eu ipsum sollicitudin, varius ultricies risus. Ut non nisi diam. Nullam eget mauris facilisis orci sodales laoreet at id tortor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec commodo massa mi, ut placerat libero faucibus non. Vivamus quis magna nec dui volutpat imperdiet. Pellentesque aliquam dolor ac orci luctus scelerisque. Suspendisse efficitur laoreet rhoncus.</p>
				<br>
				<p>Etiam malesuada nulla at ultricies pretium. Quisque dapibus posuere rutrum. Vivamus condimentum laoreet est sed viverra. Fusce ac nisi tincidunt, luctus sem a, ultrices sem. Nullam feugiat eros vitae rhoncus convallis. Aenean rutrum efficitur tristique. Etiam vel purus at enim suscipit maximus id non erat. Vestibulum quis tincidunt ipsum. Phasellus at sollicitudin libero. Vestibulum venenatis dignissim facilisis. Aenean sit amet lacus vulputate, sagittis tortor ut, ultrices leo. Donec quis eros a massa semper efficitur. Mauris porta imperdiet nisi, ac condimentum dui sodales eu. Suspendisse auctor magna non massa posuere, sit amet bibendum orci blandit.</p>
				<br>
				<p>Praesent sed urna eget est dignissim efficitur. Nullam metus enim, placerat tincidunt placerat eu, mollis non erat. Sed a ex tempus, lacinia sapien non, sollicitudin nibh. Aenean non mauris libero. Nullam varius aliquam erat vitae consectetur. Donec mollis leo mi, vitae consectetur augue vehicula convallis. Praesent orci ligula, egestas eget hendrerit eget, egestas vel urna. Sed erat libero, rhoncus ut efficitur sit amet, luctus at tellus. In scelerisque, ante quis convallis semper, libero arcu lacinia magna, quis tempor sem enim eu ex. Praesent tincidunt sapien sed neque imperdiet auctor.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porttitor rhoncus arcu vel sollicitudin. Cras vehicula dui in lobortis pharetra. Aenean at euismod odio. Nulla facilisi. Cras eu felis pharetra, porta est et, fringilla arcu. Praesent at tristique ex. Aliquam blandit pharetra gravida. Pellentesque vestibulum felis a neque efficitur eleifend. Phasellus ut consectetur eros, quis convallis ipsum.</p>
				<br>
				<p>Integer feugiat interdum erat. Pellentesque finibus fermentum leo et ultricies. Nunc ut ipsum rutrum, accumsan odio ut, mollis lectus. Pellentesque nec dictum magna. Nunc ullamcorper eu justo at bibendum. Fusce volutpat, libero ut finibus imperdiet, nibh lacus aliquet leo, id placerat magna purus eu nisi. Nam posuere eros nec sapien vestibulum, volutpat commodo dui sollicitudin. Pellentesque blandit metus nunc, nec ultricies eros posuere vel. Sed ultrices velit elit, et tempus eros pretium ac. Vivamus tristique condimentum sapien vitae tristique. Fusce et ante quis ante semper ultricies. Nam interdum augue in finibus fringilla. Integer feugiat fringilla varius. Praesent fringilla accumsan lorem, mollis facilisis nunc lacinia porttitor. Vivamus commodo magna viverra, dapibus mauris non, posuere felis. Sed tincidunt fringilla tortor vel rhoncus.</p>
				<br>
				<p>Suspendisse vitae enim in neque tempor faucibus. Aliquam dictum ligula nisi, eleifend fringilla diam convallis et. Morbi vel velit in erat iaculis venenatis. Quisque dapibus sapien eros, quis gravida tellus rutrum at. Morbi feugiat tortor ac rutrum maximus. Nullam erat lacus, mattis et metus ut, elementum iaculis ex. Etiam finibus erat non nulla rutrum lobortis. Nullam tellus libero, fermentum eu ipsum sollicitudin, varius ultricies risus. Ut non nisi diam. Nullam eget mauris facilisis orci sodales laoreet at id tortor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec commodo massa mi, ut placerat libero faucibus non. Vivamus quis magna nec dui volutpat imperdiet. Pellentesque aliquam dolor ac orci luctus scelerisque. Suspendisse efficitur laoreet rhoncus.</p>
				<br>
				<p>Etiam malesuada nulla at ultricies pretium. Quisque dapibus posuere rutrum. Vivamus condimentum laoreet est sed viverra. Fusce ac nisi tincidunt, luctus sem a, ultrices sem. Nullam feugiat eros vitae rhoncus convallis. Aenean rutrum efficitur tristique. Etiam vel purus at enim suscipit maximus id non erat. Vestibulum quis tincidunt ipsum. Phasellus at sollicitudin libero. Vestibulum venenatis dignissim facilisis. Aenean sit amet lacus vulputate, sagittis tortor ut, ultrices leo. Donec quis eros a massa semper efficitur. Mauris porta imperdiet nisi, ac condimentum dui sodales eu. Suspendisse auctor magna non massa posuere, sit amet bibendum orci blandit.</p>
				<br>
				<p>Praesent sed urna eget est dignissim efficitur. Nullam metus enim, placerat tincidunt placerat eu, mollis non erat. Sed a ex tempus, lacinia sapien non, sollicitudin nibh. Aenean non mauris libero. Nullam varius aliquam erat vitae consectetur. Donec mollis leo mi, vitae consectetur augue vehicula convallis. Praesent orci ligula, egestas eget hendrerit eget, egestas vel urna. Sed erat libero, rhoncus ut efficitur sit amet, luctus at tellus. In scelerisque, ante quis convallis semper, libero arcu lacinia magna, quis tempor sem enim eu ex. Praesent tincidunt sapien sed neque imperdiet auctor.</p>
				<br><br><br>
			</div>
		
		</section><br><br><br>
<?php require('../footer.php');?>
<?php require('../librerias.php');?>

<?php require('../modals.php');?>
</body>
</html>
