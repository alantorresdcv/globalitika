		<footer>
			<div class="footer">
				<div class="container">
					<div class="row">
						<div class="col-12 col-md-6 col-lg-3 col-xs-6 col-sm-6 logo-footer">
							<img src="../assets/images/logo-globalitika.png" alt="Logo Globalitika">
							<div>
				      			<i class="iconosociales fab fa-facebook"></i>
				      			<i class="iconosociales fab fa-twitter"></i>
				      			<i class="iconosociales fab fa-linkedin"></i>
				      </div>
						</div>
						<div class="col-12 col-md-6 col-lg-3 col-xs-6 col-sm-6">
							<h6 class="heading-uppercase text-medium"><b>Enlaces principales</b></h6>
							<ul class="list-icon">
								<li>
								<a class=" active " href="<?php echo"$Link/"; ?>" >Home</a>
							</li>

							<!-- Dropdown -->
							<li class="nav-item nav-megadropdown">
								<a class="" href="<?php echo"$Link/investigacion"; ?>">Investigación</a>
							</li>

							<!-- Sub Dropdown -->
							<li>
								<a class="" href="<?php echo"$Link/membresias"; ?>">Membresías</a>
							</li>

							<!-- Mega Dropdown -->

							</ul>
						</div>
						<div class="col-12 col-md-6 col-lg-3 col-xs-6 col-sm-6">
							<h6 class="heading-uppercase text-medium"><b>Enlaces adicionales</b></h6>
							<ul class="list-icon">
								<li>
								<a class="" href="<?php echo"$Link/#Nosotros"; ?>">Sobre nosotros</a>
							</li>
							<li>
								<a class="" href="<?php echo"$Link/#sectioncontacto"; ?>">Contacto</a>
							</li>
							<li>
								<a class="" href="<?php echo"$Link/"; ?>legales">Legales</a>
							</li>
							</ul>
						</div>
						<div class="col-12 col-md-6 col-lg-3 col-xs-6 col-sm-6">
							<h6 class="heading-uppercase text-medium"><b>Contact Info</b></h6>
							<ul class="list-unstyled">
								<!--<li>121 King St, Melbourne VIC 3000</li>-->
								<li>example@email.com</li>
								<li>+(123) 456 789 01</li>
							</ul>
						</div>
					</div><!-- end row -->
				</div><!-- end container -->
			</div><!-- end footer -->
		</footer>
