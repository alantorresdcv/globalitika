-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 29-12-2018 a las 06:40:26
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `globalitika`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `autores`
--

CREATE TABLE IF NOT EXISTS `autores` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) NOT NULL,
  `Ocupacion` varchar(2000) NOT NULL,
  `CantidadArticulos` int(11) NOT NULL,
  `FotoAutor` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `autores`
--

INSERT INTO `autores` (`ID`, `Nombre`, `Ocupacion`, `CantidadArticulos`, `FotoAutor`) VALUES
(1, 'kathryn e. clark', 'Ceo de telewormas, investigadora, escritora con sede en CDMX, Mexico', 327, 'http://tuprogramador.com.ve/globalitika/Libros/autor2.jpg'),
(2, 'paulo coelho', 'Ceo de telewormas, investigadora, escritora con sede en CDMX, Mexico', 327, 'http://tuprogramador.com.ve/globalitika/Libros/autor1.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
  `ID_Categoria` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Nombre_Categoria` varchar(100) NOT NULL,
  `Portada_Categoria` varchar(100) NOT NULL,
  PRIMARY KEY (`ID_Categoria`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`ID_Categoria`, `Nombre_Categoria`, `Portada_Categoria`) VALUES
(1, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(2, 'Novela', 'https://picsum.photos/300/200/?image=33'),
(3, 'Arte', 'https://picsum.photos/300/200/?image=33'),
(4, 'Comedia', 'https://picsum.photos/300/200/?image=33'),
(5, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(6, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(7, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(8, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(9, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(10, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(11, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(12, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(13, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(14, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(15, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(16, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(17, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(18, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(19, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(20, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(21, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(22, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(23, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(24, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(25, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(26, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(27, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(28, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(29, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(30, 'Ficcion', 'https://picsum.photos/300/200/?image=33'),
(31, 'Ficcion', 'https://picsum.photos/300/200/?image=33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas`
--

CREATE TABLE IF NOT EXISTS `facturas` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IDUsuario` int(11) NOT NULL,
  `Factura` varchar(200) NOT NULL,
  `MetodoPago` varchar(200) NOT NULL,
  `Fecha` varchar(200) NOT NULL,
  `Estatus` varchar(200) NOT NULL,
  `Total` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `facturas`
--

INSERT INTO `facturas` (`ID`, `IDUsuario`, `Factura`, `MetodoPago`, `Fecha`, `Estatus`, `Total`) VALUES
(1, 1, 'http://tuprogramador.com.ve/globalitika/libros/11', '****9586', 'Sep 9, 2018', 'Pagado', 'MX$99,00'),
(2, 1, 'http://tuprogramador.com.ve/globalitika/libros/11', '****9586', 'Sep 9, 2018', 'Pagado', 'MX$99,00'),
(3, 1, 'http://tuprogramador.com.ve/globalitika/libros/11', '****9586', 'Sep 9, 2018', 'Pagado', 'MX$99,00'),
(4, 1, 'http://tuprogramador.com.ve/globalitika/libros/11', '****9586', 'Sep 9, 2018', 'Pagado', 'MX$99,00'),
(5, 1, 'http://tuprogramador.com.ve/globalitika/libros/11', '****9586', 'Sep 9, 2018', 'Pendiente', 'MX$99,00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libros`
--

CREATE TABLE IF NOT EXISTS `libros` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Autor` varchar(50) NOT NULL,
  `Categoria` varchar(50) NOT NULL,
  `Fecha` date NOT NULL,
  `Titulo` varchar(100) NOT NULL,
  `DestacadoPortada` int(10) NOT NULL,
  `DescripcionCorta` varchar(350) NOT NULL,
  `DescripcionLarga` mediumtext NOT NULL,
  `Portada` varchar(200) NOT NULL,
  `DestacadoCarrusel` int(10) NOT NULL,
  `Premium` int(1) NOT NULL,
  `FotoAutor` varchar(100) NOT NULL,
  `Pdf` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=247 ;

--
-- Volcado de datos para la tabla `libros`
--

INSERT INTO `libros` (`ID`, `Autor`, `Categoria`, `Fecha`, `Titulo`, `DestacadoPortada`, `DescripcionCorta`, `DescripcionLarga`, `Portada`, `DestacadoCarrusel`, `Premium`, `FotoAutor`, `Pdf`) VALUES
(1, 'kathryn e. clark', '1', '2018-12-18', 'Once minutos', 1, 'Once minutos es un libro del escritor brasileno Paulo Coelho, publicado en el ano 2003. La novela relata la vida de Maria, una joven de una aldea remota de Brasil. "Habia una vez una prostituta llamada Maria..." Como un cuento de hadas contado para adultos, asi comienza la novela que conmovio al mundo. ', '<h2>Once minutos </h2><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent fringilla, justo ut laoreet luctus, lacus lacus feugiat augue, vulputate laoreet massa dui a mi. Ut bibendum molestie vestibulum. Integer convallis commodo dolor imperdiet molestie. Aliquam sit amet luctus dolor. Vestibulum vel iaculis nunc, quis scelerisque metus. Nunc sodales eros at diam ullamcorper consequat. Nulla posuere gravida lorem, eu dictum sem fringilla vitae. Ut non magna sit amet dui lacinia tincidunt. Donec ultrices, metus id iaculis tincidunt, mi nunc gravida urna, non lacinia neque est id dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent fringilla, justo ut laoreet luctus, lacus lacus feugiat augue, vulputate laoreet massa dui a mi. Ut bibendum molestie vestibulum. Integer convallis commodo dolor imperdiet molestie. Aliquam sit amet luctus dolor. Vestibulum vel iaculis nunc, quis scelerisque metus. Nunc sodales eros at diam ullamcorper consequat. Nulla posuere gravida lorem, eu dictum sem fringilla vitae. Ut non magna sit amet dui lacinia tincidunt. Donec ultrices, metus id iaculis tincidunt, mi nunc gravida urna, non lacinia neque est id dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent fringilla, justo ut laoreet luctus, lacus lacus feugiat augue, vulputate laoreet massa dui a mi. Ut bibendum molestie vestibulum. Integer convallis commodo dolor imperdiet molestie. Aliquam sit amet luctus dolor. Vestibulum vel iaculis nunc, quis scelerisque metus. Nunc sodales eros at diam ullamcorper consequat. Nulla posuere gravida lorem, eu dictum sem fringilla vitae. Ut non magna sit amet dui lacinia tincidunt. Donec ultrices, metus id iaculis tincidunt, mi nunc gravida urna, non lacinia neque est id dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent fringilla, justo ut laoreet luctus, lacus lacus feugiat augue, vulputate laoreet massa dui a mi. Ut bibendum molestie vestibulum. Integer convallis commodo dolor imperdiet molestie. Aliquam sit amet luctus dolor. Vestibulum vel iaculis nunc, quis scelerisque metus. Nunc sodales eros at diam ullamcorper consequat. Nulla posuere gravida lorem, eu dictum sem fringilla vitae. Ut non magna sit amet dui lacinia tincidunt. Donec ultrices, metus id iaculis tincidunt, mi nunc gravida urna, non lacinia neque est id dui.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent fringilla, justo ut laoreet luctus, lacus lacus feugiat augue, vulputate laoreet massa dui a mi. Ut bibendum molestie vestibulum. Integer convallis commodo dolor imperdiet molestie. Aliquam sit amet luctus dolor. Vestibulum vel iaculis nunc, quis scelerisque metus. Nunc sodales eros at diam ullamcorper consequat. Nulla posuere gravida lorem, eu dictum sem fringilla vitae. Ut non magna sit amet dui lacinia tincidunt. Donec ultrices, metus id iaculis tincidunt, mi nunc gravida urna, non lacinia neque est id dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent fringilla, justo ut laoreet luctus, lacus lacus feugiat augue, vulputate laoreet massa dui a mi. Ut bibendum molestie vestibulum. Integer convallis commodo dolor imperdiet molestie. Aliquam sit amet luctus dolor. Vestibulum vel iaculis nunc, quis scelerisque metus. Nunc sodales eros at diam ullamcorper consequat. Nulla posuere gravida lorem, eu dictum sem fringilla vitae. Ut non magna sit amet dui lacinia tincidunt. Donec ultrices, metus id iaculis tincidunt, mi nunc gravida urna, non lacinia neque est id dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent fringilla, justo ut laoreet luctus, lacus lacus feugiat augue, vulputate laoreet massa dui a mi. Ut bibendum molestie vestibulum. Integer convallis commodo dolor imperdiet molestie. Aliquam sit amet luctus dolor. Vestibulum vel iaculis nunc, quis scelerisque metus. Nunc sodales eros at diam ullamcorper consequat. Nulla posuere gravida lorem, eu dictum sem fringilla vitae. Ut non magna sit amet dui lacinia tincidunt. Donec ultrices, metus id iaculis tincidunt, mi nunc gravida urna, non lacinia neque est id dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent fringilla, justo ut laoreet luctus, lacus lacus feugiat augue, vulputate laoreet massa dui a mi. Ut bibendum molestie vestibulum. Integer convallis commodo dolor imperdiet molestie. Aliquam sit amet luctus dolor. Vestibulum vel iaculis nunc, quis scelerisque metus. Nunc sodales eros at diam ullamcorper consequat. Nulla posuere gravida lorem, eu dictum sem fringilla vitae. Ut non magna sit amet dui lacinia tincidunt. Donec ultrices, metus id iaculis tincidunt, mi nunc gravida urna, non lacinia neque est id dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent fringilla, justo ut laoreet luctus, lacus lacus feugiat augue, vulputate laoreet massa dui a mi. Ut bibendum molestie vestibulum. Integer convallis commodo dolor imperdiet molestie. Aliquam sit amet luctus dolor. Vestibulum vel iaculis nunc, quis scelerisque metus. Nunc sodales eros at diam ullamcorper consequat. Nulla posuere gravida lorem, eu dictum sem fringilla vitae. Ut non magna sit amet dui lacinia tincidunt. Donec ultrices, metus id iaculis tincidunt, mi nunc gravida urna, non lacinia neque est id dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent fringilla, justo ut laoreet luctus, lacus lacus feugiat augue, vulputate laoreet massa dui a mi. Ut bibendum molestie vestibulum. Integer convallis commodo dolor imperdiet molestie. Aliquam sit amet luctus dolor. Vestibulum vel iaculis nunc, quis scelerisque metus. Nunc sodales eros at diam ullamcorper consequat. Nulla posuere gravida lorem, eu dictum sem fringilla vitae. Ut non magna sit amet dui lacinia tincidunt. Donec ultrices, metus id iaculis tincidunt, mi nunc gravida urna, non lacinia neque est id dui.</p>', 'http://tuprogramador.com.ve/globalitika/Libros/11.jpg', 1, 1, 'http://tuprogramador.com.ve/globalitika/Libros/autor1.jpg', 'http://tuprogramador.com.ve/globalitika/libros/Once-minutos.pdf'),
(241, 'paulo coelho', '2', '2018-12-18', 'El alquimista', 0, 'El alquimista (O Alquimista, 1988) es un libro escrito por el brasileño Paulo Coelho que ha sido traducido a más de 63 lenguas y publicado en 150 países, y que ha vendido 65 millones de copias en todo el mundo (2012).1? El libro trata sobre los sueños y los medios que utilizamos para alcanzarlos, sobre el azar en nuestra vida y las señales que se p', 'El alquimista (O Alquimista, 1988) es un libro escrito por el brasileño Paulo Coelho que ha sido traducido a más de 63 lenguas y publicado en 150 países, y que ha vendido 65 millones de copias en todo el mundo (2012).1? El libro trata sobre los sueños y los medios que utilizamos para alcanzarlos, sobre el azar en nuestra vida y las señales que se presentan a lo largo de esta.', 'http://tuprogramador.com.ve/globalitika/Libros/el.jpg', 0, 0, 'http://tuprogramador.com.ve/globalitika/Libros/autor1.jpg', 'http://tuprogramador.com.ve/globalitika/libros/Once-minutos.pdf'),
(242, 'paulo coelho', '1', '2018-12-18', 'Once minutos', 1, 'Once minutos es un libro del escritor brasileno Paulo Coelho, publicado en el ano 2003. La novela relata la vida de Maria, una joven de una aldea remota de Brasil. "Habia una vez una prostituta llamada Maria..." Como un cuento de hadas contado para adultos, asi comienza la novela que conmovio al mundo. ', 'Once minutos es un libro del escritor brasileno Paulo Coelho, publicado en el ano 2003. La novela relata la vida de Maria, una joven de una aldea remota de Brasil. "Habia una vez una prostituta llamada Maria..." Como un cuento de hadas contado para adultos, asi comienza la novela que conmovio al mundo. A partir de desengaños amorosos en una etapa pronta de adolescentes, María desarrolla un odio por el amor y decide buscar fortuna en Suiza. Allí descubre que la realidad es mucho más difícil de lo que esperaba. Después de trabajar en un club nocturno como bailarina de samba durante un tiempo, se da cuenta de que esto no es lo que quiere. Durante de una acalorada discusión con su gerente, ella sale corriendo y comienza a buscar una carrera como modelo. Realiza una larga búsqueda de trabajo sin resultado, y como ella comienza a quedarse sin dinero, se vende, por 1.000 francos para "una noche" con un hombre árabe.  Encantada con el dinero fácil y después de comprometerse con su alma, ella aterriza en un burdel de la rue de Berne, el corazón de la luz roja de Ginebra del distrito. Allí se hace amiga de Nyah que le da consejos sobre su "nueva profesión", y Milán, el dueño del burdel, que le enseña trucos del oficio. María comienza a trabajar con su cuerpo y la mente cerrando todas las puertas para el amor y manteniendo su corazón abierto sólo para su diario. Su carrera se convierte un éxito y famosos y sus colegas comienzan a envidiarla.  Pasan los meses y María se convierte en una prostituta profesional preparada, que no sólo relaja la mente de sus clientes, sino también calma su alma al hablar con ellos acerca de sus problemas. Su mundo gira al revés cuando conoce a Ralf Hart, un pintor. María ahora está dividida entre sus fantasías sexuales y verdadero amor por Ralf. Eventualmente, ella decide que es hora de irse de Ginebra con su memoria de Ralf, porque se da cuenta que son de mundos distintos. Pero antes de irse, ella decide volver a encender el muerto fuego sexual en', 'http://tuprogramador.com.ve/globalitika/Libros/11.jpg', 1, 1, 'http://tuprogramador.com.ve/globalitika/Libros/autor1.jpg', 'http://tuprogramador.com.ve/globalitika/libros/Once-minutos.pdf'),
(243, 'paulo coelho', '1', '2018-12-18', 'Once minutos', 1, 'Once minutos es un libro del escritor brasileno Paulo Coelho, publicado en el ano 2003. La novela relata la vida de Maria, una joven de una aldea remota de Brasil. "Habia una vez una prostituta llamada Maria..." Como un cuento de hadas contado para adultos, asi comienza la novela que conmovio al mundo. ', 'Once minutos es un libro del escritor brasileno Paulo Coelho, publicado en el ano 2003. La novela relata la vida de Maria, una joven de una aldea remota de Brasil. "Habia una vez una prostituta llamada Maria..." Como un cuento de hadas contado para adultos, asi comienza la novela que conmovio al mundo. A partir de desengaños amorosos en una etapa pronta de adolescentes, María desarrolla un odio por el amor y decide buscar fortuna en Suiza. Allí descubre que la realidad es mucho más difícil de lo que esperaba. Después de trabajar en un club nocturno como bailarina de samba durante un tiempo, se da cuenta de que esto no es lo que quiere. Durante de una acalorada discusión con su gerente, ella sale corriendo y comienza a buscar una carrera como modelo. Realiza una larga búsqueda de trabajo sin resultado, y como ella comienza a quedarse sin dinero, se vende, por 1.000 francos para "una noche" con un hombre árabe.  Encantada con el dinero fácil y después de comprometerse con su alma, ella aterriza en un burdel de la rue de Berne, el corazón de la luz roja de Ginebra del distrito. Allí se hace amiga de Nyah que le da consejos sobre su "nueva profesión", y Milán, el dueño del burdel, que le enseña trucos del oficio. María comienza a trabajar con su cuerpo y la mente cerrando todas las puertas para el amor y manteniendo su corazón abierto sólo para su diario. Su carrera se convierte un éxito y famosos y sus colegas comienzan a envidiarla.  Pasan los meses y María se convierte en una prostituta profesional preparada, que no sólo relaja la mente de sus clientes, sino también calma su alma al hablar con ellos acerca de sus problemas. Su mundo gira al revés cuando conoce a Ralf Hart, un pintor. María ahora está dividida entre sus fantasías sexuales y verdadero amor por Ralf. Eventualmente, ella decide que es hora de irse de Ginebra con su memoria de Ralf, porque se da cuenta que son de mundos distintos. Pero antes de irse, ella decide volver a encender el muerto fuego sexual en', 'http://tuprogramador.com.ve/globalitika/Libros/11.jpg', 1, 1, 'http://tuprogramador.com.ve/globalitika/Libros/autor1.jpg', 'http://tuprogramador.com.ve/globalitika/libros/Once-minutos.pdf'),
(244, 'paulo coelho', '2', '2018-12-18', 'El alquimista', 0, 'El alquimista (O Alquimista, 1988) es un libro escrito por el brasileño Paulo Coelho que ha sido traducido a más de 63 lenguas y publicado en 150 países, y que ha vendido 65 millones de copias en todo el mundo (2012).1? El libro trata sobre los sueños y los medios que utilizamos para alcanzarlos, sobre el azar en nuestra vida y las señales que se p', 'El alquimista (O Alquimista, 1988) es un libro escrito por el brasileño Paulo Coelho que ha sido traducido a más de 63 lenguas y publicado en 150 países, y que ha vendido 65 millones de copias en todo el mundo (2012).1? El libro trata sobre los sueños y los medios que utilizamos para alcanzarlos, sobre el azar en nuestra vida y las señales que se presentan a lo largo de esta.', 'http://tuprogramador.com.ve/globalitika/Libros/el.jpg', 0, 0, 'http://tuprogramador.com.ve/globalitika/Libros/autor1.jpg', 'http://tuprogramador.com.ve/globalitika/libros/Once-minutos.pdf'),
(245, 'paulo coelho', '1', '2018-12-18', 'Once minutos', 1, 'Once minutos es un libro del escritor brasileno Paulo Coelho, publicado en el ano 2003. La novela relata la vida de Maria, una joven de una aldea remota de Brasil. "Habia una vez una prostituta llamada Maria..." Como un cuento de hadas contado para adultos, asi comienza la novela que conmovio al mundo. ', 'Once minutos es un libro del escritor brasileno Paulo Coelho, publicado en el ano 2003. La novela relata la vida de Maria, una joven de una aldea remota de Brasil. "Habia una vez una prostituta llamada Maria..." Como un cuento de hadas contado para adultos, asi comienza la novela que conmovio al mundo. A partir de desengaños amorosos en una etapa pronta de adolescentes, María desarrolla un odio por el amor y decide buscar fortuna en Suiza. Allí descubre que la realidad es mucho más difícil de lo que esperaba. Después de trabajar en un club nocturno como bailarina de samba durante un tiempo, se da cuenta de que esto no es lo que quiere. Durante de una acalorada discusión con su gerente, ella sale corriendo y comienza a buscar una carrera como modelo. Realiza una larga búsqueda de trabajo sin resultado, y como ella comienza a quedarse sin dinero, se vende, por 1.000 francos para "una noche" con un hombre árabe.  Encantada con el dinero fácil y después de comprometerse con su alma, ella aterriza en un burdel de la rue de Berne, el corazón de la luz roja de Ginebra del distrito. Allí se hace amiga de Nyah que le da consejos sobre su "nueva profesión", y Milán, el dueño del burdel, que le enseña trucos del oficio. María comienza a trabajar con su cuerpo y la mente cerrando todas las puertas para el amor y manteniendo su corazón abierto sólo para su diario. Su carrera se convierte un éxito y famosos y sus colegas comienzan a envidiarla.  Pasan los meses y María se convierte en una prostituta profesional preparada, que no sólo relaja la mente de sus clientes, sino también calma su alma al hablar con ellos acerca de sus problemas. Su mundo gira al revés cuando conoce a Ralf Hart, un pintor. María ahora está dividida entre sus fantasías sexuales y verdadero amor por Ralf. Eventualmente, ella decide que es hora de irse de Ginebra con su memoria de Ralf, porque se da cuenta que son de mundos distintos. Pero antes de irse, ella decide volver a encender el muerto fuego sexual en', 'http://tuprogramador.com.ve/globalitika/Libros/11.jpg', 1, 1, 'http://tuprogramador.com.ve/globalitika/Libros/autor1.jpg', 'http://tuprogramador.com.ve/globalitika/libros/Once-minutos.pdf'),
(246, 'paulo coelho', '2', '2018-12-18', 'El alquimista', 0, 'El alquimista (O Alquimista, 1988) es un libro escrito por el brasileño Paulo Coelho que ha sido traducido a más de 63 lenguas y publicado en 150 países, y que ha vendido 65 millones de copias en todo el mundo (2012).1? El libro trata sobre los sueños y los medios que utilizamos para alcanzarlos, sobre el azar en nuestra vida y las señales que se p', 'El alquimista (O Alquimista, 1988) es un libro escrito por el brasileño Paulo Coelho que ha sido traducido a más de 63 lenguas y publicado en 150 países, y que ha vendido 65 millones de copias en todo el mundo (2012).1? El libro trata sobre los sueños y los medios que utilizamos para alcanzarlos, sobre el azar en nuestra vida y las señales que se presentan a lo largo de esta.', 'http://tuprogramador.com.ve/globalitika/Libros/el.jpg', 0, 0, 'http://tuprogramador.com.ve/globalitika/Libros/autor1.jpg', 'http://tuprogramador.com.ve/globalitika/libros/Once-minutos.pdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(200) NOT NULL,
  `Apellido` varchar(200) NOT NULL,
  `Correo` varchar(200) NOT NULL,
  `Clave` varchar(200) NOT NULL,
  `Foto` varchar(200) NOT NULL,
  `Plan` varchar(200) NOT NULL,
  `Ciclo` varchar(200) NOT NULL,
  `Estatus` varchar(200) NOT NULL,
  `UltimoPago` varchar(255) NOT NULL,
  `SiguientePago` varchar(255) NOT NULL,
  `FechaExpiracion` varchar(200) NOT NULL,
  `Acceso` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`ID`, `Nombre`, `Apellido`, `Correo`, `Clave`, `Foto`, `Plan`, `Ciclo`, `Estatus`, `UltimoPago`, `SiguientePago`, `FechaExpiracion`, `Acceso`) VALUES
(1, 'daniel ', 'h. nelson', 'd.nelson@nelsonsays.com', '1234', 'http://tuprogramador.com.ve/globalitika/usuarios/fotoperfil/colerico.jpg', 'estudiante', 'anual', 'activa', 'abril 25, 2018', 'abril 25, 2019', 'abril 25, 2019', 'Acceso a todo');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
