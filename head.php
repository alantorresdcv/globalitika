<?php
echo "

<meta charset='utf-8'>
<meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
<meta name='description' content=''>
<meta name='keywords' content=''>
<title>$title</title>

<!-- Favicon -->
<link href='$Link/assets/images/favicon.png' rel='shortcut icon'>
<!-- CSS -->
<link href='https://unpkg.com/aos@2.3.1/dist/aos.css' rel='stylesheet'>
<link rel='stylesheet' href='$Link/assets/owlcarousel/assets/owl.carousel.min.css'>
<link rel='stylesheet' href='$Link/assets/owlcarousel/assets/owl.theme.default.min.css'>
<link href='$Link/assets/css/bootstrap.css' rel='stylesheet'>

<link rel='stylesheet prefetch' href='$Link/assets/css/bootstrap-select.min.css'>
<link href='$Link/assets/plugins/bootstrap/bootstrap.min.css' rel='stylesheet'>
<link href='$Link/assets/plugins/magnific-popup/magnific-popup.min.css' rel='stylesheet'>
<!-- Fonts/Icons -->
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.6.3/css/all.css' integrity='sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/' crossorigin='anonymous'>


<link href='$Link/assets/plugins/themify/themify-icons.min.css' rel='stylesheet'>
<link href='$Link/assets/css/app.css' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Libre+Baskerville' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900' rel='stylesheet'>

<link href='$Link/assets/css/main.css' rel='stylesheet'>
<link href='$Link/assets/css/responsive.css' rel='stylesheet'>


";
?>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
