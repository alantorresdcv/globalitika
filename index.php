<?php session_start();
$title="Globalitika México"; 

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
	include "Config.php";
	require('head.php');
	?>
</head>
	<body data-preloader="2" onload="body()">

		<?php require('menuprincipal.php');?>
		<?php require('consultas.php') ?>

		<!-- Home section -->
		<div class="section-lg bg-image parallax" style="background-image: url(assets/images/portada.jpg);background-position: bottom;">
			<div class="portada"  onclick='cerraropciones()'>
				<div class="text-center">
					<div class="fondo">
						<div class="ultimanoticia margin-bottom-50">
							<div class="col-md-2">
								<img class='hidden-phone' src="<?php echo "$FotoAutorDestacadoPortada"; ?>">
								<img style='border-radius: 5px;' class='hidden-desktop hidden-tablet' src="<?php echo "$PortadaDestacadoPortada"; ?>">
							</div>
							<div class="col-md-10">
								<h1 class='centrar-movil'>¡Nuestro artículo destacado!</h1>
								<?php echo "
								<h2 class='centrar-movil'>$TituloDestacadoPortada</h2>
								<h3 class='centrar-movil'>$CategoriaDestacadoPortada | $TemaDestacadoPortada</h3>
								<p class='text-justify centrar-movil'>$DescripcionCortaDestacadoPortada</p>

								<p class='centrar-movil text-underline'><a class='white' href='articulo/?Titulo=$TituloDestacadoPortada&ID=$IDDestacadoPortada'>Leer más...</a></p>";
								?>
							</div>
						</div>
					</div>
					<div class="recientes">
						<h2 class="text-left">Recientes</h2>
						<?php
						//Recientes en el banner de la portada
						$RecientesPortada=mysql_query("SELECT * FROM l32ddiasfbafbiatreiasdfw INNER JOIN coewerw9hfwefouwefsdf ON ID_Categoria=Categoria INNER JOIN aewrwf9345233dasde ON Autor = ID_Autor WHERE Estatus='1' AND Fecha <= now() ORDER BY Fecha DESC LIMIT 0,6");
							while($fila=mysql_fetch_assoc($RecientesPortada)){
								@$IDRecientesPortada=$fila["ID"];
								@$AutorRecientesPortada=$fila["Nombre_Autor"];
								@$CategoriaRecientesPortada=$fila["Nombre_Categoria"];
								@$FechaRecientesPortada=$fila["Fecha"];
								@$TituloRecientesPortada=$fila["Titulo"];
								@$DescripcionCortaRecientesPortada=$fila["DescripcionCorta"];
								@$DescripcionLargaRecientesPortada=$fila["DescripcionLarga"];
								@$PortadaRecientesPortada=$fila["Portada"];
								@$PremiumRecientesPortada=$fila["Premium"];
								if ($PremiumRecientesPortada=="1") {
									$Premium="premium";
								}
								else{
									$Premium="free";
								}

								echo "
								<a href='articulo/?Titulo=$TituloRecientesPortada&ID=$IDRecientesPortada'>
								<div class='text-left marginitem col-md-12 nopadding'>
									<div class='col-md-4 inline-block nopadding imgrecientes' style='background-image: url($PortadaRecientesPortada);'>
										<div class='$Premium estrella2' >
								    		<img src='assets/images/estrella.png'>
								    	</div>
									</div>
									<div class='col-md-8 inline-block paddingre'>
										<strong class='strongitem text-uppercase'>Autor: ".$AutorRecientesPortada."<br>Título: ".$TituloRecientesPortada."<br>Categoría: $CategoriaRecientesPortada</strong>
									</div>
								</div>
								</a>
								";

							}

						?>

					</div>
					<!-- end row -->
				</div><!-- end container -->
			</div>
		</div>
		<!-- end Home section -->
		<section class='hidden-phone'>
		<div data-aos-duration="500" data-aos="fade-up" class="container">
			<h2 class="texto-azul-01 margin-top-20 margin-bottom-20">DESTACADO</h2>
			<div class="colorbajo"></div>
			<div class="owl-carousel owl-theme">
				<?php
						//Recientes en el carrusel
						$RecientesCarrusel=mysql_query("SELECT *,DATE_FORMAT(Fecha,'%d/%m/%Y') AS FechaSola FROM l32ddiasfbafbiatreiasdfw INNER JOIN coewerw9hfwefouwefsdf ON ID_Categoria=Categoria INNER JOIN aewrwf9345233dasde ON Autor = ID_Autor WHERE Estatus='1' AND Destacado='1' AND Fecha <= now() ORDER BY Fecha DESC LIMIT 0,6");
							while($fila=mysql_fetch_assoc($RecientesCarrusel)){
								@$IDRecientesCarrusel=$fila["ID"];
								@$AutorRecientesCarrusel=$fila["Nombre_Autor"];
								@$CategoriaRecientesCarrusel=$fila["Nombre_Categoria"];
								@$FechaRecientesCarrusel=$fila["FechaSola"];
								@$TituloRecientesCarrusel=$fila["Titulo"];
								@$DescripcionCortaRecientesCarrusel=$fila["DescripcionCorta"];
								@$DescripcionLargaRecientesCarrusel=$fila["DescripcionLarga"];
								@$PortadaRecientesCarrusel=$fila["Portada"];
								@$PremiumRecientesCarrusel=$fila["Premium"];
								@$FotoAutorRecientesCarrusel=$fila["FotoAutor"];
								if ($PremiumRecientesCarrusel=="1") {
									$Premium="premium";
								}
								else{
									$Premium="free";
								}

								echo "
								<a href='articulo/?Titulo=$TituloRecientesCarrusel&ID=$IDRecientesCarrusel'>
								    <div class='item'>
								    	<div class='imgportada' style='background-image: url($PortadaRecientesCarrusel);'>
								    		<div class='padre' style='height:270px;'>
								    			<span class='titulo'>$TituloRecientesCarrusel</span>
								    		</div>
								    	</div>
								    	<div class='$Premium margin-bottom-20'><img src='assets/images/estrella.png'></div>
								    	<div class='texto-destacados'>
									    	<div class='padding3 col-md-3'><img id='autor' src='$FotoAutorRecientesCarrusel'></div>
									    	<div class='col-md-9 padding9'>
									    		<p class='nombre'>$AutorRecientesCarrusel</p>
									    		<p class='titulobanner'>$TituloRecientesCarrusel</p>
									    		<h4 class='categoria'>CATEGORIA | $CategoriaRecientesCarrusel</h4>
									    		<span class='fecha'>$FechaRecientesCarrusel</span>
									    	</div>
								    	</div>
								    </div>
									</a>
								";
							}
						?>
			</div>
		</div>
		</section>
		<section class="m10"  data-aos-duration="500" data-aos="fade-right">
		<div class="container">
			<h2 class="texto-azul-01 margin-top-20 margin-bottom-20">OTRAS CATEGORÍAS</h2>
			<div class="colorbajo"></div>
			<br><br><br>
			<div class="categorias">
				<?php
						//categorias
						$categorias=mysql_query("SELECT * FROM coewerw9hfwefouwefsdf LIMIT 0,12");
							while($fila=mysql_fetch_assoc($categorias)){
								@$ID=$fila["ID_Categoria"];
								@$Nombre_Categoria=$fila["Nombre_Categoria"];
								@$Portada_Categorias=$fila["Portada_Categoria"];

								echo "
								<a href='$Link/investigacion/?Categoria=$ID'>
								<div class='itemimgcategoria' style='background-image: url($Portada_Categorias);'>
									<h1>$Nombre_Categoria</h1>
								</div>
								</a>
								";
							}
						?>

			</div>
		</div>

		<br id="Nosotros"><br>
		</section>
		<section  data-aos-duration="500" data-aos="zoom-in" class="m10 container">
			<div class="container">
				<h2 class="texto-azul-01 margin-top-20 margin-bottom-20">SOBRE NOSOTROS</h2>
				<div class="colorbajo"></div>
				<br><br><br>
				<div class="quienes">
					<div id="item1" onclick="item1()">
						<h2 class="hidden-desktop hidden-tablet">¿Quiénes Somos?</h2>
						<p id="pitem1">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc et justo est. Cras ullamcorper laoreet dolor eu vestibulum. Sed vehicula condimentum leo. Sed eu dapibus neque, a finibus lectus. Praesent eu tortor consequat, pretium justo iaculis, feugiat enim. Nunc vel neque at ex vehicula venenatis vitae vel nunc.</p>
						<h2 class="hidden-phone">¿Quiénes Somos?</h2>
					</div>
					<div id="item2" onclick="item2()">
						<h2 class="hidden-desktop hidden-tablet">¿Qué hacemos?</h2>
						<p id="pitem2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc et justo est. Cras ullamcorper laoreet dolor eu vestibulum. Sed vehicula condimentum leo. Sed eu dapibus neque, a finibus lectus. Praesent eu tortor consequat, pretium justo iaculis, feugiat enim. Nunc vel neque at ex vehicula venenatis vitae vel nunc.</p>
						<h2 class="hidden-phone">¿Qué hacemos?</h2>
					</div>
					<div id="item3" onclick="item3()">
						<h2  class="hidden-desktop hidden-tablet">Nuestro equipo</h2>
						<p id="pitem3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc et justo est. Cras ullamcorper laoreet dolor eu vestibulum. Sed vehicula condimentum leo. Sed eu dapibus neque, a finibus lectus. Praesent eu tortor consequat, pretium justo iaculis, feugiat enim. Nunc vel neque at ex vehicula venenatis vitae vel nunc.</p>
						<h2  class="hidden-phone">Nuestro equipo</h2>
					</div>
				</div>
			</div>
		</section>
		<br><br><br>
		<section data-aos-duration="500" data-aos="flip-left" class="m10 container">
			<div class="container">
				<h2 class="texto-azul-01 margin-top-20 margin-bottom-20">MEMBRESÍAS</h2>
				<div class="colorbajo"></div>
				<br><br><br>
				<div class="m1">
					<h2 class="font-30 estudiante">Estudiante</h2>
					<p class="margin-bottom-20 text-left">
						<ul>
							<li>-Favoritos ilimitados</li>
							<li>-Acceso a todos los artículos</li>
							<li>-Agregar amigos</li>
							<li>-Acceso temprano a artículos</li>
						</ul>
					</p>
					<p class="fecha margin-top-30">pago mensual de</p>
					<label class=" estudiante"><b>$99</b></label>
					<button class="bamarillo" onclick='membresia()'><b>Comenzar</b></button>
				</div>
				<div class="m2">
					<h2 class="font-30 profesional">Profesional</h2>
					<p class="margin-bottom-20 text-left">
						<ul>
							<li>-Favoritos ilimitados</li>
							<li>-Acceso a todos los artículos</li>
							<li>-Agregar amigos</li>
							<li>-Acceso temprano a artículos</li>
							<li>-Acceso a todos los artículos</li>
							<li>-Agregar amigos</li>
							<li>-Acceso temprano a artículos</li>
						</ul>
					</p>
					<p class="fecha margin-top-30">pago mensual de</p>
					<label class="profesional"><b>$99</b></label>
					<button class="bazul" onclick='membresia()'><b>Comenzar</b></button>
				</div>

				<div class="m3">
					<h2 class="font-30 empresarial">Empresarial</h2>
					<p class="margin-bottom-20 text-left">
						<ul>
							<li>-Favoritos ilimitados</li>
							<li>-Acceso a todos los artículos</li>
							<li>-Agregar amigos</li>
							<li>-Acceso temprano a artículos</li>
							<li>-Acceso a todos los artículos</li>
							<li>-Agregar amigos</li>
							<li>-Acceso temprano a artículos</li>
						</ul>
					</p>
					<p class="fecha margin-top-30">pago mensual de </p>
					<label class="empresarial"><b>$99</b></label>
					<button class="bnegro" onclick='membresia()'><b>Comenzar</b></button>
				</div>

			</div>
	</section>
	<section data-aos-duration="500" data-aos="flip-right" class="m10 padding0">
			<div class="container padding0">
				<div class='hidden-desktop hidden-tablet'>
					<br><br><br>
				</div>
				<h2 class="texto-azul-01 margin-top-20 margin-bottom-20">NEWSLETTER</h2>
				<div class="colorbajo"></div>
				<br><br><br>
			</div>
			<div class="newsletter">
				<div class="container newfondo">
					<div class="col-md-7 p6 float-none inline-block text-center roboto">
					<br><br>
						<h2 class="texto-azul-01 margin-top-20 margin-bottom-20">SUSCRÍBETE</h3>
						<p class="font-12 margin-bottom-20">Mantente al tanto de todos los nuevos artículos relevantes.<br>
						Escoge qué tan seguido quieres recibir correos y actualizaciones con nuevo contenido.</p>
						<input type="email" name="correo" id="correosuscribir" required placeholder="Correo electrónico">
						<br>
						<span id="smsmailsuscribir" style="color: black;text-shadow: none;font-size: 15px;"></span><br>
						<button type="button" onclick="suscribir()" id="butsuscribir">Guardar</button>
					</div>
					<div class="col-md-5 p7 float-none inline-block text-center roboto">
						<h4 class="texto-azul-01 margin-top-20 margin-bottom-20"><b>¿Y qué ganas?</b></h4>
						<p class="roboto fecha">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sed blandit velit. Ut nec sapien a lorem semper pretium sit amet nec erat. Ut faucibus bibendum faucibus.</p>
					</div>
				</div>
			</div>
	</section>
	<br><br><br><br id="sectioncontacto">
	<section  data-aos-duration="500" data-aos="fade-up" class="m10 contacto" >
			<div class="container">
				<h2 class="texto-azul-01 margin-top-20 margin-bottom-20">CONTÁCTANOS</h2>
				<div class="colorbajo"></div>
				<br><br><br>

				<div class="col-md-7 inline-block">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sed blandit velit. Ut nec sapien a lorem semper pretium sit amet nec erat. Ut faucibus bibendum faucibus.</p><br><br>
					<ul class="contact-footer">
						<!--<li><i class="fa fa-envelope mazul text-medium"></i> 121 King St, Melbourne VIC 3000</li><br>-->
						<li><i class="fa fa-phone mazul text-medium"></i> example@email.com</li><br>
						<li><i class="fa fa-map-marker mazul text-medium"></i> +(123) 456 789 01</li><br>
					</ul>
				</div>
				<div class="col-md-5 inline-block">
					<form enctype='multipart/form-data' id='formuploadajaxcontactanos' method='POST'>
						<input type="text" name="Nombre" placeholder="Tu nombre">
						<input type="text" name="Correo" placeholder="Tu correo">
						<textarea name="Mensaje" style="height: 200px;" placeholder="Mensaje"></textarea>
						<span id="smscontactanos" style="color: black;text-shadow: none;font-size: 15px;"></span>
					</form>
					<form id='demo-form' action="?" method="POST">
      					<div style="width: 100%;" class="g-recaptcha" data-sitekey="6LdLpo8UAAAAALAEBXD0gDfIC5dGoGxf9OQ2KjQm"></div><br>
      					<button type="button" onclick="contactanos()" id="botonc"><b>ENVIAR</b></button>
    				</form>
				</div>
			</div>
			<div class="fondoazul"></div>

	</section>
		<br><br><br>

<?php require('footer.php');?>
<?php require('librerias.php');?>

<?php require('modals.php');
echo "
<script type='text/javascript'>
	function membresia() {
		location.href = '$Link/membresias';
	}
	setInterval(function(){
      var str = $( '#demo-form' ).serialize();
      if (str.length > 40) {
        document.getElementById('botonc').disabled = false;
      }
      else{
        document.getElementById('botonc').disabled = true;
      }
  }, 1000);
</script>";
?>
</body>
</html>
