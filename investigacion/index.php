<?php session_start(); 
$title="Globalitika | Investigacion"; 
?>
<!DOCTYPE html>
<html lang="en">
<head>

	<?php 
	include "../Config.php";
	require('../head.php');
	echo "<link rel='stylesheet' type='text/css' href='$Link/css/investigacion.css'>";
	@$Tema=$_GET['Tema'];
	@$Categoria=$_GET['Categoria'];
	@$Titulo=$_GET['Titulo'];
	@$Autor=$_GET['Autor'];
	if ($Tema!=NULL) {
		$busqueda=$Tema;
	}
	elseif ($Categoria!=NULL) {
		$busqueda=$Categoria;
	}
	elseif ($Titulo!=NULL) {
		$busqueda=$Titulo;
	}
	elseif ($Autor!=NULL) {
		$busqueda=$Autor;
	}
	else{
		$busqueda='';
	}
	?>

</head>
	<body data-preloader="2" onload="body()">

		<?php require('../menuprincipal.php');?>

		<!-- Home section -->
		<!-- Home section -->
		<div class="section-lg bg-image parallax" style="background-image: url(../assets/images/portada2.jpg);background-position: center;">
			<div class="portada2" style="background: linear-gradient(to bottom, rgba(0, 0, 0, 0.1) 0%, rgba(160, 157, 157, 0.28) 35%, rgba(0, 0, 0, 1) 100%);">
				<div class="text-center">
					<!-- end row -->
				</div><!-- end container -->
			</div>
		</div>	
		<section class="container">
			<div><br><br>
				<h2 class="text-center">INVESTIGACIÓN</h2>
			<div class="colorbajo" style="margin: 0 auto;"></div>
			</div><br><br><br><br><br>
			<div class="col-md-3 inline-block">
				<div class="filtro">
					<form enctype='multipart/form-data' id='investigacion' method='POST'>
						<input onkeyup="investigacion()" type="text" name="nombre" id="nombre" placeholder="Buscar nombre...">
						<br>
						<select id="categoria" onchange="myFunction();investigacion()" name='categoria' class='form-control' style='height: auto;'>
							<option  value='' selected >Todas las categorias</option>                            <?php
                                $Pubicaciones=mysql_query("SELECT * FROM coewerw9hfwefouwefsdf");
								while($fila=mysql_fetch_assoc($Pubicaciones)){
								@$IDCategoria=$fila["ID_Categoria"];
								@$Nombre=$fila["Nombre_Categoria"];
								echo"
								<option value='$IDCategoria'>$Nombre</option>
								";}?>
								</select>

								<br>
								<select onchange="investigacion()" id="tema" name='tema' class='form-control' style='height: auto;'>
									<option  value='' selected >Todas los temas</option>
								<?php
								$tema=mysql_query("SELECT * FROM tr02j0wer82350j2asdw");
								while($fila=mysql_fetch_assoc($tema)){
								@$ID_Tema=$fila["ID_Tema"];
								@$Nombre=$fila["Nombre_Tema"];
								@$Categoria=$fila["IDCategoria"];
								echo"
								<option  style='display: none;' class='opcion$Categoria' value='$ID_Tema'>$Nombre</option>
								";}?>
								
								</select>
						<p>Organizar</p>
						<label class="botonlabel" onclick="labelASC();investigacion()" id="labelASC" for="ASC">A-Z <i class="rotar fas fa-exchange-alt"></i></label>
						<label class="botonlabel" onclick="labelDESC();investigacion()" id="labelDESC" for="DESC" class="derecha">Z-A <i class="rotar fas fa-exchange-alt"></i></label>
						<label class="botonlabel" onclick="labelREC();investigacion()" id="labelREC" for="REC">Recientes</label>
						<label class="botonlabel" onclick="labelANT();investigacion()" id="labelANT" for="ANT">Antiguos</label>
						<input type="radio" name="ordenletra" value="ASC" id="ASC" style="display: none;">
						<input type="radio" name="ordenletra" value="DESC" id="DESC" style="display: none;">
						<input type="radio" name="ordenletra" value="REC" id="REC" style="display: none;">
						<input type="radio" name="ordenletra" value="ANT" id="ANT" style="display: none;">
						<p>Características</p>
						<div class="input__row">
  <ul class="buttons">
    <li>
      <input id="radiobtn_1" class="radiobtn" name="premium" type="radio" value="" tabindex="1">
      <span></span>
      <label for="radiobtn_1" onclick="investigacion()">Todos</label>
    </li>
    <li>
      <input id="radiobtn_2" class="radiobtn" name="premium" type="radio" value="1" tabindex="2">
      <span></span>
      <label for="radiobtn_2" onclick="investigacion()">Premium</label>
    </li>
    <li>
      <input id="radiobtn_3" class="radiobtn" name="premium" type="radio" value="0" tabindex="3">
      <span></span>
      <label for="radiobtn_3" onclick="investigacion()">Gratis</label></li>
</ul>
</div>
					</form>
				</div>
				
			</div>
			<div class="col-md-9 inline-block" id="tbody">
				
				<?php 
						//Recientes en el carrusel
						$RecientesCarrusel=mysql_query("SELECT *,DATE_FORMAT(Fecha,'%d/%m/%Y') AS FechaSola FROM l32ddiasfbafbiatreiasdfw INNER JOIN coewerw9hfwefouwefsdf ON Categoria = ID_Categoria INNER JOIN tr02j0wer82350j2asdw ON Tema = ID_Tema INNER JOIN aewrwf9345233dasde ON Autor = ID_Autor WHERE Nombre_Autor LIKE '%$busqueda%' OR Tema LIKE '%$busqueda%' OR Categoria LIKE '%$busqueda%' OR Titulo LIKE '%$busqueda%' AND Estatus='1' AND Fecha <= now() ORDER BY Fecha DESC");
							while($fila=mysql_fetch_assoc($RecientesCarrusel)){
								@$IDRecientesCarrusel=$fila["ID"];
								@$AutorRecientesCarrusel=$fila["Nombre_Autor"];
								@$CategoriaRecientesCarrusel=$fila["Nombre_Categoria"];
								@$FechaRecientesCarrusel=$fila["FechaSola"];
								@$TituloRecientesCarrusel=$fila["Titulo"];
								@$DescripcionCortaRecientesCarrusel=$fila["DescripcionCorta"];
								@$DescripcionLargaRecientesCarrusel=$fila["DescripcionLarga"];
								@$Tema=$fila["Nombre_Tema"];
								@$PortadaRecientesCarrusel=$fila["Portada"];
								@$PremiumRecientesCarrusel=$fila["Premium"];
								@$FotoAutorRecientesCarrusel=$fila["FotoAutor"];
								if ($PremiumRecientesCarrusel=="1") {
									$Premium="premium";
								}
								else{
									$Premium="free";
								}

								echo "
								<div class='col-md-4 inline-block' style='margin-top:20px;'>
								<a href='$Link/articulo/?Titulo=$TituloRecientesCarrusel&ID=$IDRecientesCarrusel'>
								    <div class='item item2'>
								    	<div class='imgportada' style='background-image: url($PortadaRecientesCarrusel);'>
								    		<div class='padre'>
								    			<span class='titulo'>$TituloRecientesCarrusel</span>
								    		</div>
								    	</div>
								    	<div class='$Premium'>
								    		<img src='../assets/images/estrella.png'>
								    	</div><br>
								    	<div class='col-md-12'>
								    		<p class='nombre'>$AutorRecientesCarrusel</p>
								    		<p class='titulobanner'>$TituloRecientesCarrusel</p>
								    		<h4 class='categoria'>$CategoriaRecientesCarrusel | $Tema</h4>
								    		<span class='fecha'>$FechaRecientesCarrusel</span>
								    		<br>
								    	</div>
								    	
								    </div>
									</a>
									</div>
								";
							}
						?>
			
			</div>
		</div>
		
		</section><br><br><br>
<?php require('../footer.php');?>
<?php require('../librerias.php');
				echo"
    <script src='https://code.jquery.com/jquery-1.11.1.min.js'></script>
    <script type='text/javascript'>
        function myFunction() {
    var select = document.getElementById('categoria').value;";
    $Pubicaciones=mysql_query("SELECT * FROM coewerw9hfwefouwefsdf");
    while($fila=mysql_fetch_assoc($Pubicaciones)){
    @$ID=$fila["ID_Categoria"];


echo"
    if (select == '$ID') {
        var option = document.getElementsByClassName('opcion$ID');
        var i;
        for (i = 0; i < option.length; i++) {
        option[i].style.display = 'initial';
      }
    }
    else{
        var option = document.getElementsByClassName('opcion$ID');
        var i;
        for (i = 0; i < option.length; i++) {
        option[i].style.display = 'none';
      }
    }";
}
    echo"}";?>
			</script>

<?php require('../modals.php');?>
</body>
</html>
