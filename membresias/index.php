<?php session_start();
$title="Globalitika | Membresias"; 
@$Correo=$_SESSION['Correo'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
	include "../Config.php";
	require('../head.php');
	?>

</head>
	<body data-preloader="2" onload="body()">

		<?php require('../menuprincipal.php');?>

		<!-- Home section -->
		<!-- Home section -->
		<div class="section-lg bg-image parallax" style="background-image: url(../assets/images/portada2.jpg);background-position: center;">
			<div class="portada2" style="background: linear-gradient(to bottom, rgba(0, 0, 0, 0.1) 0%, rgba(160, 157, 157, 0.28) 35%, rgba(0, 0, 0, 1) 100%);">
				<div class="text-center">
					<!-- end row -->
				</div><!-- end container -->
			</div>
		</div>
		<section>
		<div data-aos-duration="500" data-aos="fade-up" class="container">
			<br><br><br><br><br>
			<h2 class="text-center">MEMBRESÍAS</h2>
			<div class="colorbajo" style="margin-left: calc(42% - 50px);"></div>
			<br><br><br>
			<div class="membresiasgra" id="select">

				<p class="pmem"><span class="uno">1</span> Selecciona tu plan</p>
				<div class="col-md-7 divmemiz">
					<div class="col-md-12 memiz cursor" id="memiz1" onclick="memiz1()">
						<div class="col-md-4 inline-block">
							<p class="font-30 estudiante"><b>Estudiante</b></p>
							<p class="font17">Acceso a todo</p>
						</div>
						<div class="col-md-3 inline-block no-padding  text-right">
							<p class="gris precio empresarial" >99$ </p>
						</div>
						<div class="col-md-1 inline-block">
							<span class="font17 almes empresarial"><br> al<br>mes</span>
						</div>
						<div class="col-md-4 inline-block no-padding  text-right">
							<button class="bamarillo botonmembresia" onclick="estudiante()">comenzar</button>
						</div>
						<div id="cuadromembre1" class="cuadromembre">
						</div>
					</div>
					<div class="col-md-12 memiz cursor" id="memiz2" onclick="memiz2()" style="margin-top: 0;
    border-radius: 0;border-top:none;">
						<div class="col-md-4 inline-block">
							<p class="font-30 profesional"><b>Profesional</b></p>
							<p class="font17">Acceso a todo</p>
						</div>
						<div class="col-md-3 inline-block no-padding  text-right">
							<p class="gris precio empresarial" >179$ </p>
						</div>
						<div class="col-md-1 inline-block  ">
							<span class="font17 almes empresarial"><br> al<br>mes</span>
						</div>
						<div class="col-md-4 inline-block no-padding  text-right">
							<button class="bazul botonmembresia" onclick="profesional()">Comenzar</button>
						</div>
						<div id="cuadromembre2" class="cuadromembre">
						</div>
					</div>

					<div class="col-md-12 memiz cursor" id="memiz3" onclick="memiz3()" style="margin-top: 0;
	    border-radius: 0px 0px 6px 6px;border-top:none;">
							<div class="col-md-4 inline-block">
								<p class="font-30 empresarial"><b>Empresarial</b></p>
								<p class="font17">Acceso a todo</p>
							</div>
							<div class="col-md-3 inline-block no-padding  text-right">
								<p class="gris precio empresarial" >249$ </p>
							</div>
							<div class="col-md-1 inline-block  ">
								<span class="font17 almes empresarial"><br> al<br>mes</span>
							</div>
							<div class="col-md-4 inline-block no-padding  text-right">
								<button class="bnegro botonmembresia" onclick="empresarial()">Comenzar</button>
							</div>
							<div id="cuadromembre3" class="cuadromembre">
						</div>
						</div>

					</div>
					<div class="inline-block container-membresias">


					<div id="divmemder0" class="divmemder" style="box-shadow: none; display: inline-block;">
						<div class="padding20" style="height: 80px;">
							<p class="font-30 "></p>
							<p class="font17"></p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;height: 220px;" class="padding20">
							<p class="font17"></p>
							<ul style="padding-left: 20px;">
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>

							</ul>
							<br>
						</div>
					</div>
					<div id="divmemder1" class="divmemder">
						<div class="padding20">
							<p class="font-30 estudiante"><b>Estudiante</b></p>
							<p class="font17">Acceso a todo</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;" class="padding20">
							<p class="estudiante font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
					</div>
					<div id="divmemder2" class="divmemder">
						<div class="padding20">
							<p class="font-30 profesional"><b>Profesional</b></p>
							<p class="font17">Acceso a todo</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;" class="padding20">
							<p class="profesional font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
					</div>
					<div id="divmemder3" class="divmemder">
						<div class="padding20">
							<p class="font-30 empresarial"><b>Empresarial</b></p>
								<p class="font17">Acceso a todo</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;" class="padding20">
							<p class="empresarial font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
					</div>
					</div>
			</div>
			<!-- inicio del estudiante 1-->
			<div class="membresiasgra" id="select1">
				<i onclick="atras()" class="fas fa-arrow-left atras"></i>
				<p class="pmem"><span class="uno">2</span> Selecciona el periodo</p>
				<div class="col-md-7 divmemiz">
					<div class="col-md-12 memiz cursor" id="estudiantememiz1" onclick="estudiantememiz1()">
						<div class="col-md-4 inline-block">
							<p class="font-30 empresarial">2 Años</p>
							<p class="font17">$70 por 24 meses</p>
						</div>
						<div class="col-md-4 inline-block no-padding  text-center">
							<span class="descuento">-28%</span>
						</div>
						<div class="col-md-4 inline-block no-padding  text-right">
							<button class="bamarillo botonmembresia botonmembresia2" onclick="estudiante1()">Seleccionar</button>
						</div>
						<div id="estudiantecuadromembre1" class="cuadromembre">
						</div>
					</div>
					<div class="col-md-12 memiz cursor" id="estudiantememiz2" onclick="estudiantememiz2()" style="margin-top: 0;
    border-radius: 0;border-top:none;">
						<div class="col-md-4 inline-block">
							<p class="font-30 empresarial">Anual</p>
							<p class="font17">$80 por 12 meses</p>
						</div>
						<div class="col-md-4 inline-block no-padding  text-center">
							<span class="descuento">-17%</span>
						</div>
						<div class="col-md-4 inline-block no-padding  text-right">
							<button class="bazul botonmembresia botonmembresia2" onclick="estudiante2()">Seleccionar</button>
						</div>
						<div id="estudiantecuadromembre2" class="cuadromembre">
						</div>
					</div>

					<div class="col-md-12 memiz cursor" id="estudiantememiz3" onclick="estudiantememiz3()" style="margin-top: 0;
	    border-radius: 0px 0px 6px 6px;;border-top:none;">
							<div class="col-md-4 inline-block">
								<p class="font-30 empresarial">Mensual</p>
								<p class="font17">$99 cada mes</p>
							</div>
							<div class="col-md-4 inline-block no-padding  text-center">
								<span class="descuento">-0%</span>
							</div>

							<div class="col-md-4 inline-block no-padding  text-right">
								<button class="bnegro botonmembresia botonmembresia2" onclick="estudiante3()">Seleccionar</button>
							</div>
							<div id="estudiantecuadromembre3" class="cuadromembre">
						</div>
						</div>

					</div>
					<div class="inline-block container-membresias">
					<div id="estudiantedivmemder0" class="divmemder" style="display: inline-block;opacity: 1;">
						<div class="padding20">
							<p class="font-30 estudiante">Estudiante</p>
							<p class="font17">Acceso a todos</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;border-bottom:solid 1px #c7c7c7;" class="padding20">
							<p class="estudiante font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
					</div>
					<div id="estudiantedivmemder1" class="divmemder">
						<div class="padding20">
							<p class="font-30 estudiante">Estudiante</p>
							<p class="font17">Acceso a todos</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;border-bottom:solid 1px #c7c7c7;" class="padding20">
							<p class="estudiante font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
						<div class="padding20">
							<p class="font-30 estudiante"><b>Total</b></p>
							<p class="font17">($99 x 24 meses) = 2376</p>
							<p class="font17"><span class="descuento">-28%</span> = <span class="total">1972,08</span></p>
						</div>
					</div>
					<div id="estudiantedivmemder2" class="divmemder">
						<div class="padding20">
							<p class="font-30 estudiante">Estudiante</p>
							<p class="font17">Acceso a todos</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;border-bottom:solid 1px #c7c7c7;" class="padding20">
							<p class="estudiante font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
						<div class="padding20">
							<p class="font-30 estudiante">Total</p>
							<p class="font17">($99 x 24 meses) = 2376</p>
							<p class="font17"><span class="descuento">-17%</span> = 1972,08</p>
						</div>
					</div>
					<div id="estudiantedivmemder3" class="divmemder">
						<div class="padding20">
							<p class="font-30 estudiante">Estudiante</p>
							<p class="font17">Acceso a todos</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;border-bottom:solid 1px #c7c7c7;" class="padding20">
							<p class="estudiante font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
						<div class="padding20">
							<p class="font-30 estudiante">Total</p>
							<p class="font17">($99 x 24 meses) = 2376</p>
							<p class="font17"><span class="descuento">-0%</span> = 2376</p>
						</div>
					</div>
					</div>
			</div>
			<!-- inicio del profesional 1-->
			<div class="membresiasgra" id="select2">
				<i onclick="atras()" class="fas fa-arrow-left atras"></i>
				<p class="pmem"><span class="uno">2</span> Selecciona el periodo</p>
				<div class="col-md-7 divmemiz">
					<div class="col-md-12 memiz cursor" id="profesionalmemiz1" onclick="profesionalmemiz1()">
						<div class="col-md-4 inline-block">
							<p class="font-30 empresarial">2 Años</p>
							<p class="font17">$160 por 24 meses</p>
						</div>
						<div class="col-md-4 inline-block no-padding  text-center">
							<span class="descuento">-28%</span>
						</div>
						<div class="col-md-4 inline-block no-padding  text-right">
							<button class="bamarillo botonmembresia botonmembresia2" onclick="profesional1()">Seleccionar</button>
						</div>
						<div id="profesionalcuadromembre1" class="cuadromembre">
						</div>
					</div>
					<div class="col-md-12 memiz cursor" id="profesionalmemiz2" onclick="profesionalmemiz2()" style="margin-top: 0;
    border-radius: 0;border-top:none;">
						<div class="col-md-4 inline-block">
							<p class="font-30 empresarial">Anual</p>
							<p class="font17">$170 por 12 meses</p>
						</div>
						<div class="col-md-4 inline-block no-padding  text-center">
							<span class="descuento">-17%</span>
						</div>
						<div class="col-md-4 inline-block no-padding  text-right">
							<button class="bazul botonmembresia botonmembresia2" onclick="profesional2()">Seleccionar</button>
						</div>
						<div id="profesionalcuadromembre2" class="cuadromembre">
						</div>
					</div>

					<div class="col-md-12 memiz cursor" id="profesionalmemiz3" onclick="profesionalmemiz3()" style="margin-top: 0;
	    border-radius: 0px 0px 6px 6px;;border-top:none;">
							<div class="col-md-4 inline-block">
								<p class="font-30 empresarial">Mensual</p>
								<p class="font17">$179 cada mes</p>
							</div>
							<div class="col-md-4 inline-block no-padding  text-center">
								<span class="descuento">-0%</span>
							</div>

							<div class="col-md-4 inline-block no-padding  text-right">
								<button class="bnegro botonmembresia botonmembresia2" onclick="profesional3()">Seleccionar</button>
							</div>
							<div id="profesionalcuadromembre3" class="cuadromembre">
						</div>
						</div>

					</div>
					<div class="inline-block container-membresias">
					<div id="profesionaldivmemder0" class="divmemder" style="display: inline-block;opacity: 1;">
						<div class="padding20">
							<p class="font-30 profesional">Profesional</p>
							<p class="font17">Acceso a todos</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;border-bottom:solid 1px #c7c7c7;" class="padding20">
							<p class="profesional font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
					</div>
					<div id="profesionaldivmemder1" class="divmemder">
						<div class="padding20">
							<p class="font-30 profesional">Profesional</p>
							<p class="font17">Acceso a todos</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;border-bottom:solid 1px #c7c7c7;" class="padding20">
							<p class="profesional font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
						<div class="padding20">
							<p class="font-30 profesional">Total</p>
							<p class="font17">($99 x 24 meses) = 2376</p>
							<p class="font17"><span class="descuento">-28%</span> = 1972,08</p>
						</div>
					</div>
					<div id="profesionaldivmemder2" class="divmemder">
						<div class="padding20">
							<p class="font-30 profesional">Profesional</p>
							<p class="font17">Acceso a todos</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;border-bottom:solid 1px #c7c7c7;" class="padding20">
							<p class="profesional font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
						<div class="padding20">
							<p class="font-30 profesional">Total</p>
							<p class="font17">($99 x 24 meses) = 2376</p>
							<p class="font17"><span class="descuento">-17%</span> = 1972,08</p>
						</div>
					</div>
					<div id="profesionaldivmemder3" class="divmemder">
						<div class="padding20">
							<p class="font-30 profesional">Profesional</p>
							<p class="font17">Acceso a todos</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;border-bottom:solid 1px #c7c7c7;" class="padding20">
							<p class="profesional font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
						<div class="padding20">
							<p class="font-30 profesional">Total</p>
							<p class="font17">($99 x 24 meses) = 2376</p>
							<p class="font17"><span class="descuento">-0%</span> = 2376</p>
						</div>
					</div>
					</div>
			</div>
			<!-- inicio del empresarial 1-->
			<div class="membresiasgra" id="select3">
				<i onclick="atras()" class="fas fa-arrow-left atras"></i>
				<p class="pmem"><span class="uno">2</span> Selecciona el periodo</p>
				<div class="col-md-7 divmemiz">
					<div class="col-md-12 memiz cursor" id="empresarialmemiz1" onclick="empresarialmemiz1()">
						<div class="col-md-4 inline-block">
							<p class="font-30 empresarial">2 Años</p>
							<p class="font17">$160 por 24 meses</p>
						</div>
						<div class="col-md-4 inline-block no-padding  text-center">
							<span class="descuento">-28%</span>
						</div>
						<div class="col-md-4 inline-block no-padding  text-right">
							<button class="bamarillo botonmembresia botonmembresia2" onclick="empresarial1()">Seleccionar</button>
						</div>
						<div id="empresarialcuadromembre1" class="cuadromembre">
						</div>
					</div>
					<div class="col-md-12 memiz cursor" id="empresarialmemiz2" onclick="empresarialmemiz2()" style="margin-top: 0;
    border-radius: 0;border-top:none;">
						<div class="col-md-4 inline-block">
							<p class="font-30 empresarial">Anual</p>
							<p class="font17">$170 por 12 meses</p>
						</div>
						<div class="col-md-4 inline-block no-padding  text-center">
							<span class="descuento">-17%</span>
						</div>
						<div class="col-md-4 inline-block no-padding  text-right">
							<button class="bazul botonmembresia botonmembresia2" onclick="empresarial2()">Seleccionar</button>
						</div>
						<div id="empresarialcuadromembre2" class="cuadromembre">
						</div>
					</div>

					<div class="col-md-12 memiz cursor" id="empresarialmemiz3" onclick="empresarialmemiz3()" style="margin-top: 0;
	    border-radius: 0px 0px 6px 6px;;border-top:none;">
							<div class="col-md-4 inline-block">
								<p class="font-30 empresarial">Mensual</p>
								<p class="font17">$179 cada mes</p>
							</div>
							<div class="col-md-4 inline-block no-padding  text-center">
								<span class="descuento">-0%</span>
							</div>

							<div class="col-md-4 inline-block no-padding  text-right">
								<button class="bnegro botonmembresia botonmembresia2" onclick="empresarial3()">Seleccionar</button>
							</div>
							<div id="empresarialcuadromembre3" class="cuadromembre">
						</div>
						</div>

					</div>
					<div class="inline-block container-membresias">
					<div id="empresarialdivmemder0" class="divmemder" style="display: inline-block;opacity: 1;">
						<div class="padding20">
							<p class="font-30 empresarial">Empresarial</p>
							<p class="font17">Acceso a todos</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;border-bottom:solid 1px #c7c7c7;" class="padding20">
							<p class="empresarial font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
					</div>
					<div id="empresarialdivmemder1" class="divmemder">
						<div class="padding20">
							<p class="font-30 empresarial">Empresarial</p>
							<p class="font17">Acceso a todos</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;border-bottom:solid 1px #c7c7c7;" class="padding20">
							<p class="empresarial font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
						<div class="padding20">
							<p class="font-30 empresarial">Total</p>
							<p class="font17">($99 x 24 meses) = 2376</p>
							<p class="font17"><span class="descuento">-28%</span> = 1972,08</p>
						</div>
					</div>
					<div id="empresarialdivmemder2" class="divmemder">
						<div class="padding20">
							<p class="font-30 empresarial">Empresarial</p>
							<p class="font17">Acceso a todos</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;border-bottom:solid 1px #c7c7c7;" class="padding20">
							<p class="empresarial font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
						<div class="padding20">
							<p class="font-30 empresarial">Total</p>
							<p class="font17">($99 x 24 meses) = 2376</p>
							<p class="font17"><span class="descuento">-17%</span> = 1972,08</p>
						</div>
					</div>
					<div id="empresarialdivmemder3" class="divmemder">
						<div class="padding20">
							<p class="font-30 empresarial">Empresarial</p>
							<p class="font17">Acceso a todos</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;border-bottom:solid 1px #c7c7c7;" class="padding20">
							<p class="empresarial font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
						<div class="padding20">
							<p class="font-30 empresarial">Total</p>
							<p class="font17">($99 x 24 meses) = 2376</p>
							<p class="font17"><span class="descuento">-0%</span> = 2376</p>
						</div>
					</div>
					</div>
			</div>
			<!-- inicio del pago estudiante 1-->
			<div class="membresiasgra" id="estudiante1">
				<i onclick="atras()" class="fas fa-arrow-left atras"></i>
				<p class="pmem"><span class="uno">3</span> Completa tu suscripción</p>
				<div class="col-md-7 divmemiz">
					<div class="col-md-12 memiz cursor" style="border-radius:6px;">
						<div class="col-md-8 inline-block">
							<p class="font-30 empresarial">Paypal</p>
							<?php if (isset($_SESSION['Correo'])){
							echo "<p class='font17'>Cuenta $Correo</p>
							";
						}
						else{
							echo "<p class='font17'>Cuenta tucorreo@gmail.com</p>";
						}
						?>

						</div>
						<div class="col-md-4 inline-block no-padding  text-center">
							<img src="../assets/images/paypal.png" style="width: 50px;">
						</div>
						<div id="estudiantecuadromembre1" class="cuadromembre">
						</div>

					</div><br><br><br>
					<div class="col-md-12 memiz cursor" style="border-radius:0px;border:none;border-top: solid 1px #c7c7c7;">
							<p>Para garantizar un servicio ininterrumpido mensualmente Puede desactivar la renovación automática o cancelar su suscripción en cualquier mo- o, su suscripción se renovará automáticamente por MXN 178800 nto a través de su cuenta <br><br>Al hacer clic en Enviar compra, usted acepta nuestros Términos de uso y Poltica de privacidad</p>
						</div>
					</div>
					<div style="margin-left: 45px;width: 30%;margin-top: 2%;" class="inline-block">
					<div id="pagoestudiantedivmemder1" class="divmemder" style="display: inline-block;">
						<div class="padding20">
							<p class="font-30 estudiante">Estudiante</p>
							<p class="font17">Acceso a todos</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;border-bottom:solid 1px #c7c7c7;" class="padding20">
							<p class="estudiante font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
						<div class="padding20">
							<p class="font-30 estudiante">Total</p>
							<p class="font17">($99 x 24 meses) = 2376</p>
							<p class="font17"><span class="descuento">-28%</span> = 1972,08</p>

						</div>
						<?php if (isset($_SESSION['Correo'])){
							echo "
							<form action='https://www.paypal.com/cgi-bin/webscr' method='post' target='_top'>
						<input type='hidden' name='cmd' value='_s-xclick'>
						<input type='hidden' name='hosted_button_id' value='3DVJPX2VD5S3E'>
						<input class='imagepaypal' type='image' src='https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif' border='0' name='submit' alt='PayPal, la forma más segura y rápida de pagar en línea.'>
						<img class='cursor' alt=' border='0' src='https://www.paypalobjects.com/es_XC/i/scr/pixel.gif' width='1' height='1'>
						</form>";
						}
						else{
							echo "<br><img onclick='abrir()' style='margin: 0 auto;
    display: block;' alt=' border='0' src='https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif' width='100$' height='auto'><br>";
						}
						?>


					</div>
				</div>
			</div>
				<!-- inicio del pago estudiante 2-->
			<div class="membresiasgra" id="estudiante2">
				<i onclick="atras()" class="fas fa-arrow-left atras"></i>
				<p class="pmem"><span class="uno">3</span> Completa tu suscripción</p>
				<div class="col-md-7 divmemiz">
					<div class="col-md-12 memiz cursor" style="border-radius:6px;">
						<div class="col-md-8 inline-block">
							<p class="font-30 empresarial">Paypal</p>
							<?php if (isset($_SESSION['Correo'])){
							echo "
							<form action='https://www.paypal.com/cgi-bin/webscr' method='post' target='_top'>
						<input type='hidden' name='cmd' value='_s-xclick'>
						<input type='hidden' name='hosted_button_id' value='3DVJPX2VD5S3E'>
						<input class='imagepaypal' type='image' src='https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif' border='0' name='submit' alt='PayPal, la forma más segura y rápida de pagar en línea.'>
						<img class='cursor' alt=' border='0' src='https://www.paypalobjects.com/es_XC/i/scr/pixel.gif' width='1' height='1'>
						</form>";
						}
						else{
							echo "<br><img onclick='abrir()' style='margin: 0 auto;
    display: block;' alt=' border='0' src='https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif' width='100$' height='auto'><br>";
						}
						?>
						</div>
						<div class="col-md-4 inline-block no-padding  text-center">
							<img src="../assets/images/paypal.png" style="width: 50px;">
						</div>
						<div id="estudiantecuadromembre1" class="cuadromembre">
						</div>

					</div><br><br><br>
					<div class="col-md-12 memiz cursor" style="border-radius:0px;border:none;border-top: solid 1px #c7c7c7;">
							<p>Para garantizar un servicio ininterrumpido mensualmente Puede desactivar la renovación automática o cancelar su suscripción en cualquier mo- o, su suscripción se renovará automáticamente por MXN 178800 nto a través de su cuenta <br><br>Al hacer clic en Enviar compra, usted acepta nuestros Términos de uso y Poltica de privacidad</p>
						</div>
					</div>
					<div style="margin-left: 45px;width: 30%;margin-top: 2%;" class="inline-block">
					<div id="pagoestudiantedivmemder1" class="divmemder" style="display: inline-block;">
						<div class="padding20">
							<p class="font-30 estudiante">Estudiante</p>
							<p class="font17">Acceso a todos</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;border-bottom:solid 1px #c7c7c7;" class="padding20">
							<p class="estudiante font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
						<div class="padding20">
							<p class="font-30 estudiante">Total</p>
							<p class="font17">($99 x 24 meses) = 2376</p>
							<p class="font17"><span class="descuento">-17%</span> = 1972,08</p>
						</div>
					</div>
				</div>
			</div>
				<!-- inicio del pago estudiante 3-->
			<div class="membresiasgra" id="estudiante3">
				<i onclick="atras()" class="fas fa-arrow-left atras"></i>
				<p class="pmem"><span class="uno">3</span> Completa tu suscripción</p>
				<div class="col-md-7 divmemiz">
					<div class="col-md-12 memiz cursor" style="border-radius:6px;">
						<div class="col-md-8 inline-block">
							<p class="font-30 empresarial">Paypal</p>
							<?php if (isset($_SESSION['Correo'])){
							echo "
							<form action='https://www.paypal.com/cgi-bin/webscr' method='post' target='_top'>
						<input type='hidden' name='cmd' value='_s-xclick'>
						<input type='hidden' name='hosted_button_id' value='3DVJPX2VD5S3E'>
						<input class='imagepaypal' type='image' src='https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif' border='0' name='submit' alt='PayPal, la forma más segura y rápida de pagar en línea.'>
						<img class='cursor' alt=' border='0' src='https://www.paypalobjects.com/es_XC/i/scr/pixel.gif' width='1' height='1'>
						</form>";
						}
						else{
							echo "<br><img onclick='abrir()' style='margin: 0 auto;
    display: block;' alt=' border='0' src='https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif' width='100$' height='auto'><br>";
						}
						?>
						</div>
						<div class="col-md-4 inline-block no-padding  text-center">
							<img src="../assets/images/paypal.png" style="width: 50px;">
						</div>
						<div id="estudiantecuadromembre1" class="cuadromembre">
						</div>

					</div><br><br><br>
					<div class="col-md-12 memiz cursor" style="border-radius:0px;border:none;border-top: solid 1px #c7c7c7;">
							<p>Para garantizar un servicio ininterrumpido mensualmente Puede desactivar la renovación automática o cancelar su suscripción en cualquier mo- o, su suscripción se renovará automáticamente por MXN 178800 nto a través de su cuenta <br><br>Al hacer clic en Enviar compra, usted acepta nuestros Términos de uso y Poltica de privacidad</p>
						</div>
					</div>
					<div style="margin-left: 45px;width: 30%;margin-top: 2%;" class="inline-block">
					<div id="pagoestudiantedivmemder1" class="divmemder" style="display: inline-block;">
						<div class="padding20">
							<p class="font-30 estudiante">Estudiante</p>
							<p class="font17">Acceso a todos</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;border-bottom:solid 1px #c7c7c7;" class="padding20">
							<p class="estudiante font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
						<div class="padding20">
							<p class="font-30 estudiante">Total</p>
							<p class="font17">($99 x 24 meses) = 2376</p>
							<p class="font17"><span class="descuento">-0%</span> = 1972,08</p>
						</div>
					</div>
				</div>
			</div>
			<!-- inicio del pago profesional 1-->
			<div class="membresiasgra" id="profesional1">
				<i onclick="atras()" class="fas fa-arrow-left atras"></i>
				<p class="pmem"><span class="uno">3</span> Completa tu suscripción</p>
				<div class="col-md-7 divmemiz">
					<div class="col-md-12 memiz cursor" style="border-radius:6px;">
						<div class="col-md-8 inline-block">
							<p class="font-30 empresarial">Paypal</p>
							<?php if (isset($_SESSION['Correo'])){
							echo "
							<form action='https://www.paypal.com/cgi-bin/webscr' method='post' target='_top'>
						<input type='hidden' name='cmd' value='_s-xclick'>
						<input type='hidden' name='hosted_button_id' value='3DVJPX2VD5S3E'>
						<input class='imagepaypal' type='image' src='https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif' border='0' name='submit' alt='PayPal, la forma más segura y rápida de pagar en línea.'>
						<img class='cursor' alt=' border='0' src='https://www.paypalobjects.com/es_XC/i/scr/pixel.gif' width='1' height='1'>
						</form>";
						}
						else{
							echo "<br><img onclick='abrir()' style='margin: 0 auto;
    display: block;' alt=' border='0' src='https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif' width='100$' height='auto'><br>";
						}
						?>
						</div>
						<div class="col-md-4 inline-block no-padding  text-center">
							<img src="../assets/images/paypal.png" style="width: 50px;">
						</div>
						<div id="profesionalcuadromembre1" class="cuadromembre">
						</div>

					</div><br><br><br>
					<div class="col-md-12 memiz cursor" style="border-radius:0px;border:none;border-top: solid 1px #c7c7c7;">
							<p>Para garantizar un servicio ininterrumpido mensualmente Puede desactivar la renovación automática o cancelar su suscripción en cualquier mo- o, su suscripción se renovará automáticamente por MXN 178800 nto a través de su cuenta <br><br>Al hacer clic en Enviar compra, usted acepta nuestros Términos de uso y Poltica de privacidad</p>
						</div>
					</div>
					<div style="margin-left: 45px;width: 30%;margin-top: 2%;" class="inline-block">
					<div id="pagoprofesionaldivmemder1" class="divmemder" style="display: inline-block;">
						<div class="padding20">
							<p class="font-30 profesional">Profesional</p>
							<p class="font17">Acceso a todos</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;border-bottom:solid 1px #c7c7c7;" class="padding20">
							<p class="profesional font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
						<div class="padding20">
							<p class="font-30 profesional">Total</p>
							<p class="font17">($99 x 24 meses) = 2376</p>
							<p class="font17"><span class="descuento">-28%</span> = 1972,08</p>
						</div>
					</div>
				</div>
			</div>
				<!-- inicio del pago profesional 2-->
			<div class="membresiasgra" id="profesional2">
				<i onclick="atras()" class="fas fa-arrow-left atras"></i>
				<p class="pmem"><span class="uno">3</span> Completa tu suscripción</p>
				<div class="col-md-7 divmemiz">
					<div class="col-md-12 memiz cursor" style="border-radius:6px;">
						<div class="col-md-8 inline-block">
							<p class="font-30 empresarial">Paypal</p>
							<?php if (isset($_SESSION['Correo'])){
							echo "
							<form action='https://www.paypal.com/cgi-bin/webscr' method='post' target='_top'>
						<input type='hidden' name='cmd' value='_s-xclick'>
						<input type='hidden' name='hosted_button_id' value='3DVJPX2VD5S3E'>
						<input class='imagepaypal' type='image' src='https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif' border='0' name='submit' alt='PayPal, la forma más segura y rápida de pagar en línea.'>
						<img class='cursor' alt=' border='0' src='https://www.paypalobjects.com/es_XC/i/scr/pixel.gif' width='1' height='1'>
						</form>";
						}
						else{
							echo "<br><img onclick='abrir()' style='margin: 0 auto;
    display: block;' alt=' border='0' src='https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif' width='100$' height='auto'><br>";
						}
						?>
						</div>
						<div class="col-md-4 inline-block no-padding  text-center">
							<img src="../assets/images/paypal.png" style="width: 50px;">
						</div>
						<div id="profesionalcuadromembre1" class="cuadromembre">
						</div>

					</div><br><br><br>
					<div class="col-md-12 memiz cursor" style="border-radius:0px;border:none;border-top: solid 1px #c7c7c7;">
							<p>Para garantizar un servicio ininterrumpido mensualmente Puede desactivar la renovación automática o cancelar su suscripción en cualquier mo- o, su suscripción se renovará automáticamente por MXN 178800 nto a través de su cuenta <br><br>Al hacer clic en Enviar compra, usted acepta nuestros Términos de uso y Poltica de privacidad</p>
						</div>
					</div>
					<div style="margin-left: 45px;width: 30%;margin-top: 2%;" class="inline-block">
					<div id="pagoprofesionaldivmemder1" class="divmemder" style="display: inline-block;">
						<div class="padding20">
							<p class="font-30 profesional">Profesional</p>
							<p class="font17">Acceso a todos</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;border-bottom:solid 1px #c7c7c7;" class="padding20">
							<p class="profesional font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
						<div class="padding20">
							<p class="font-30 profesional">Total</p>
							<p class="font17">($99 x 24 meses) = 2376</p>
							<p class="font17"><span class="descuento">-17%</span> = 1972,08</p>
						</div>
					</div>
				</div>
			</div>
				<!-- inicio del pago profesional 3-->
			<div class="membresiasgra" id="profesional3">
				<i onclick="atras()" class="fas fa-arrow-left atras"></i>
				<p class="pmem"><span class="uno">3</span> Completa tu suscripción</p>
				<div class="col-md-7 divmemiz">
					<div class="col-md-12 memiz cursor" style="border-radius:6px;">
						<div class="col-md-8 inline-block">
							<p class="font-30 empresarial">Paypal</p>
							<?php if (isset($_SESSION['Correo'])){
							echo "
							<form action='https://www.paypal.com/cgi-bin/webscr' method='post' target='_top'>
						<input type='hidden' name='cmd' value='_s-xclick'>
						<input type='hidden' name='hosted_button_id' value='3DVJPX2VD5S3E'>
						<input class='imagepaypal' type='image' src='https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif' border='0' name='submit' alt='PayPal, la forma más segura y rápida de pagar en línea.'>
						<img class='cursor' alt=' border='0' src='https://www.paypalobjects.com/es_XC/i/scr/pixel.gif' width='1' height='1'>
						</form>";
						}
						else{
							echo "<br><img onclick='abrir()' style='margin: 0 auto;
    display: block;' alt=' border='0' src='https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif' width='100$' height='auto'><br>";
						}
						?>
						</div>
						<div class="col-md-4 inline-block no-padding  text-center">
							<img src="../assets/images/paypal.png" style="width: 50px;">
						</div>
						<div id="profesionalcuadromembre1" class="cuadromembre">
						</div>

					</div><br><br><br>
					<div class="col-md-12 memiz cursor" style="border-radius:0px;border:none;border-top: solid 1px #c7c7c7;">
							<p>Para garantizar un servicio ininterrumpido mensualmente Puede desactivar la renovación automática o cancelar su suscripción en cualquier mo- o, su suscripción se renovará automáticamente por MXN 178800 nto a través de su cuenta <br><br>Al hacer clic en Enviar compra, usted acepta nuestros Términos de uso y Poltica de privacidad</p>
						</div>
					</div>
					<div style="margin-left: 45px;width: 30%;margin-top: 2%;" class="inline-block">
					<div id="pagoprofesionaldivmemder1" class="divmemder" style="display: inline-block;">
						<div class="padding20">
							<p class="font-30 profesional">Profesional</p>
							<p class="font17">Acceso a todos</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;border-bottom:solid 1px #c7c7c7;" class="padding20">
							<p class="profesional font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
						<div class="padding20">
							<p class="font-30 profesional">Total</p>
							<p class="font17">($99 x 24 meses) = 2376</p>
							<p class="font17"><span class="descuento">-0%</span> = 1972,08</p>
						</div>
					</div>
				</div>
			</div>
			<!-- inicio del pago empresarial 1-->
			<div class="membresiasgra" id="empresarial1">
				<i onclick="atras()" class="fas fa-arrow-left atras"></i>
				<p class="pmem"><span class="uno">3</span> Completa tu suscripción</p>
				<div class="col-md-7 divmemiz">
					<div class="col-md-12 memiz cursor" style="border-radius:6px;">
						<div class="col-md-8 inline-block">
							<p class="font-30 empresarial">Paypal</p>
							<?php if (isset($_SESSION['Correo'])){
							echo "
							<form action='https://www.paypal.com/cgi-bin/webscr' method='post' target='_top'>
						<input type='hidden' name='cmd' value='_s-xclick'>
						<input type='hidden' name='hosted_button_id' value='3DVJPX2VD5S3E'>
						<input class='imagepaypal' type='image' src='https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif' border='0' name='submit' alt='PayPal, la forma más segura y rápida de pagar en línea.'>
						<img class='cursor' alt=' border='0' src='https://www.paypalobjects.com/es_XC/i/scr/pixel.gif' width='1' height='1'>
						</form>";
						}
						else{
							echo "<br><img onclick='abrir()' style='margin: 0 auto;
    display: block;' alt=' border='0' src='https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif' width='100$' height='auto'><br>";
						}
						?>
						</div>
						<div class="col-md-4 inline-block no-padding  text-center">
							<img src="../assets/images/paypal.png" style="width: 50px;">
						</div>
						<div id="empresarialcuadromembre1" class="cuadromembre">
						</div>

					</div><br><br><br>
					<div class="col-md-12 memiz cursor" style="border-radius:0px;border:none;border-top: solid 1px #c7c7c7;">
							<p>Para garantizar un servicio ininterrumpido mensualmente Puede desactivar la renovación automática o cancelar su suscripción en cualquier mo- o, su suscripción se renovará automáticamente por MXN 178800 nto a través de su cuenta <br><br>Al hacer clic en Enviar compra, usted acepta nuestros Términos de uso y Poltica de privacidad</p>
						</div>
					</div>
					<div style="margin-left: 45px;width: 30%;margin-top: 2%;" class="inline-block">
					<div id="pagoempresarialdivmemder1" class="divmemder" style="display: inline-block;">
						<div class="padding20">
							<p class="font-30 empresarial">Empresarial</p>
							<p class="font17">Acceso a todos</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;border-bottom:solid 1px #c7c7c7;" class="padding20">
							<p class="empresarial font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
						<div class="padding20">
							<p class="font-30 empresarial">Total</p>
							<p class="font17">($99 x 24 meses) = 2376</p>
							<p class="font17"><span class="descuento">-28%</span> = 1972,08</p>
						</div>
					</div>
				</div>
			</div>
				<!-- inicio del pago empresarial 2-->
			<div class="membresiasgra" id="empresarial2">
				<i onclick="atras()" class="fas fa-arrow-left atras"></i>
				<p class="pmem"><span class="uno">3</span> Completa tu suscripción</p>
				<div class="col-md-7 divmemiz">
					<div class="col-md-12 memiz cursor" style="border-radius:6px;">
						<div class="col-md-8 inline-block">
							<p class="font-30 empresarial">Paypal</p>
							<?php if (isset($_SESSION['Correo'])){
							echo "
							<form action='https://www.paypal.com/cgi-bin/webscr' method='post' target='_top'>
						<input type='hidden' name='cmd' value='_s-xclick'>
						<input type='hidden' name='hosted_button_id' value='3DVJPX2VD5S3E'>
						<input class='imagepaypal' type='image' src='https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif' border='0' name='submit' alt='PayPal, la forma más segura y rápida de pagar en línea.'>
						<img class='cursor' alt=' border='0' src='https://www.paypalobjects.com/es_XC/i/scr/pixel.gif' width='1' height='1'>
						</form>";
						}
						else{
							echo "<br><img onclick='abrir()' style='margin: 0 auto;
    display: block;' alt=' border='0' src='https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif' width='100$' height='auto'><br>";
						}
						?>
						</div>
						<div class="col-md-4 inline-block no-padding  text-center">
							<img src="../assets/images/paypal.png" style="width: 50px;">
						</div>
						<div id="empresarialcuadromembre1" class="cuadromembre">
						</div>

					</div><br><br><br>
					<div class="col-md-12 memiz cursor" style="border-radius:0px;border:none;border-top: solid 1px #c7c7c7;">
							<p>Para garantizar un servicio ininterrumpido mensualmente Puede desactivar la renovación automática o cancelar su suscripción en cualquier mo- o, su suscripción se renovará automáticamente por MXN 178800 nto a través de su cuenta <br><br>Al hacer clic en Enviar compra, usted acepta nuestros Términos de uso y Poltica de privacidad</p>
						</div>
					</div>
					<div style="margin-left: 45px;width: 30%;margin-top: 2%;" class="inline-block">
					<div id="pagoempresarialdivmemder1" class="divmemder" style="display: inline-block;">
						<div class="padding20">
							<p class="font-30 empresarial">Empresarial</p>
							<p class="font17">Acceso a todos</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;border-bottom:solid 1px #c7c7c7;" class="padding20">
							<p class="empresarial font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
						<div class="padding20">
							<p class="font-30 empresarial">Total</p>
							<p class="font17">($99 x 24 meses) = 2376</p>
							<p class="font17"><span class="descuento">-17%</span> = 1972,08</p>
						</div>
					</div>
				</div>
			</div>
				<!-- inicio del pago empresarial 3-->
			<div class="membresiasgra" id="empresarial3">
				<i onclick="atras()" class="fas fa-arrow-left atras"></i>
				<p class="pmem"><span class="uno">3</span> Completa tu suscripción</p>
				<div class="col-md-7 divmemiz">
					<div class="col-md-12 memiz cursor" style="border-radius:6px;">
						<div class="col-md-8 inline-block">
							<p class="font-30 empresarial">Paypal</p>
							<?php if (isset($_SESSION['Correo'])){
							echo "
							<form action='https://www.paypal.com/cgi-bin/webscr' method='post' target='_top'>
						<input type='hidden' name='cmd' value='_s-xclick'>
						<input type='hidden' name='hosted_button_id' value='3DVJPX2VD5S3E'>
						<input class='imagepaypal' type='image' src='https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif' border='0' name='submit' alt='PayPal, la forma más segura y rápida de pagar en línea.'>
						<img class='cursor' alt=' border='0' src='https://www.paypalobjects.com/es_XC/i/scr/pixel.gif' width='1' height='1'>
						</form>";
						}
						else{
							echo "<br><img onclick='abrir()' style='margin: 0 auto;
    display: block;' alt=' border='0' src='https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif' width='100$' height='auto'><br>";
						}
						?>
						</div>
						<div class="col-md-4 inline-block no-padding  text-center">
							<img src="../assets/images/paypal.png" style="width: 50px;">
						</div>
						<div id="empresarialcuadromembre1" class="cuadromembre">
						</div>

					</div><br><br><br>
					<div class="col-md-12 memiz cursor" style="border-radius:0px;border:none;border-top: solid 1px #c7c7c7;">
							<p>Para garantizar un servicio ininterrumpido mensualmente Puede desactivar la renovación automática o cancelar su suscripción en cualquier mo- o, su suscripción se renovará automáticamente por MXN 178800 nto a través de su cuenta <br><br>Al hacer clic en Enviar compra, usted acepta nuestros Términos de uso y Poltica de privacidad</p>
						</div>
					</div>
					<div style="margin-left: 45px;width: 30%;margin-top: 2%;" class="inline-block">
					<div id="pagoempresarialdivmemder1" class="divmemder" style="display: inline-block;">
						<div class="padding20">
							<p class="font-30 empresarial">Empresarial</p>
							<p class="font17">Acceso a todos</p>
						</div>
						<div style="width: 100%;border-top: solid 1px #c7c7c7;border-bottom:solid 1px #c7c7c7;" class="padding20">
							<p class="empresarial font17">Beneficios</p>
							<ul>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>
								<li>Lorem ipsum dolor sit amet,</li>

							</ul>
							<br>
						</div>
						<div class="padding20">
							<p class="font-30 empresarial">Total</p>
							<p class="font17">($99 x 24 meses) = 2376</p>
							<p class="font17"><span class="descuento">-0%</span> = 1972,08</p>
						</div>
					</div>
				</div>
			</div>
</section><br><br><br>
<?php require('../footer.php');?>
<?php require('../librerias.php');?>

<?php require('../modals.php');?>
</body>
</html>
