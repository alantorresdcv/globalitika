<?php session_start(); 
if (isset($_SESSION['administrador'])) {
    header("Location:../consola/administradores.php");
}
else{
    header("Location:../consola/login/");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include('head.php');?>
</head>

<body class="fix-header fix-sidebar card-no-border">
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>

    <div id="main-wrapper">

        <header class="topbar">
            <?php include('menu-top.php');?>
        </header>

        <aside class="left-sidebar">
            <?php include('menu-lateral.php');?>
        </aside>

        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Admins</h3>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <!-- .left-right-aside-column-->
                            <div class="contact-page-aside">
                                <div>
                                    <div class="right-page-header">
                                        <div class="d-flex">
                                            <div class="align-self-center">
                                                <h4 class="card-title m-t-10"></h4></div>
                                            <div class="ml-auto">
                                                <input type="text" id="demo-input-search2" placeholder="Buscar" class="form-control"> </div>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="demo-foo-addrow" class="table m-t-30 table-hover no-wrap contact-list" data-page-size="10">
                                            <thead>
                                                <tr>
                                                    <th>ID #</th>
                                                    <th>Nombre</th>
                                                    <th>Apellido</th>
                                                    <th>Agregado</th>
                                                    <th>Correo electronico</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>0003458</td>
                                                    <td>jonathan</td>
                                                    <td>edison</td>
                                                    <td>12/10/2018</td>
                                                    <td>jonathaeison@gmail.com</td>
                                                    <td>
                                                        <button class="botontransparente verde"><i class="fas fa-pencil-alt"></i></button>
                                                        <button class="botontransparente azul"><i class="fas fa-eye"></i></button>
                                                        <button class="botontransparente rojo"><i class="far fa-trash-alt"></i></button>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>0003458</td>
                                                    <td>jonathan</td>
                                                    <td>edison</td>
                                                    <td>12/10/2018</td>
                                                    <td>jonathaeison@gmail.com</td>
                                                    <td>
                                                        <button class="botontransparente verde"><i class="fas fa-pencil-alt"></i></button>
                                                        <button class="botontransparente azul"><i class="fas fa-eye"></i></button>
                                                        <button class="botontransparente rojo"><i class="far fa-trash-alt"></i></button>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>0003458</td>
                                                    <td>jonathan</td>
                                                    <td>edison</td>
                                                    <td>12/10/2018</td>
                                                    <td>jonathaeison@gmail.com</td>
                                                    <td>
                                                        <button class="botontransparente verde"><i class="fas fa-pencil-alt"></i></button>
                                                        <button class="botontransparente azul"><i class="fas fa-eye"></i></button>
                                                        <button class="botontransparente rojo"><i class="far fa-trash-alt"></i></button>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>0003458</td>
                                                    <td>jonathan</td>
                                                    <td>edison</td>
                                                    <td>12/10/2018</td>
                                                    <td>jonathaeison@gmail.com</td>
                                                    <td>
                                                        <button class="botontransparente verde"><i class="fas fa-pencil-alt"></i></button>
                                                        <button class="botontransparente azul"><i class="fas fa-eye"></i></button>
                                                        <button class="botontransparente rojo"><i class="far fa-trash-alt"></i></button>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>0003458</td>
                                                    <td>jonathan</td>
                                                    <td>edison</td>
                                                    <td>12/10/2018</td>
                                                    <td>jonathaeison@gmail.com</td>
                                                    <td>
                                                        <button class="botontransparente verde"><i class="fas fa-pencil-alt"></i></button>
                                                        <button class="botontransparente azul"><i class="fas fa-eye"></i></button>
                                                        <button class="botontransparente rojo"><i class="far fa-trash-alt"></i></button>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>0003458</td>
                                                    <td>jonathan</td>
                                                    <td>edison</td>
                                                    <td>12/10/2018</td>
                                                    <td>jonathaeison@gmail.com</td>
                                                    <td>
                                                        <button class="botontransparente verde"><i class="fas fa-pencil-alt"></i></button>
                                                        <button class="botontransparente azul"><i class="fas fa-eye"></i></button>
                                                        <button class="botontransparente rojo"><i class="far fa-trash-alt"></i></button>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>0003458</td>
                                                    <td>jonathan</td>
                                                    <td>edison</td>
                                                    <td>12/10/2018</td>
                                                    <td>jonathaeison@gmail.com</td>
                                                    <td>
                                                        <button class="botontransparente verde"><i class="fas fa-pencil-alt"></i></button>
                                                        <button class="botontransparente azul"><i class="fas fa-eye"></i></button>
                                                        <button class="botontransparente rojo"><i class="far fa-trash-alt"></i></button>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>0003458</td>
                                                    <td>jonathan</td>
                                                    <td>edison</td>
                                                    <td>12/10/2018</td>
                                                    <td>jonathaeison@gmail.com</td>
                                                    <td>
                                                        <button class="botontransparente verde"><i class="fas fa-pencil-alt"></i></button>
                                                        <button class="botontransparente azul"><i class="fas fa-eye"></i></button>
                                                        <button class="botontransparente rojo"><i class="far fa-trash-alt"></i></button>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                                                    </td>
                                                </tr>

                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="2">
                                                        <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#add-contact">Add New Contact</button>
                                                    </td>
                                                    <div id="add-contact" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel">Add New Contact</h4>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <from class="form-horizontal form-material">
                                                                        <div class="form-group">
                                                                            <div class="col-md-12 m-b-20">
                                                                                <input type="text" class="form-control" placeholder="Type name"> </div>
                                                                            <div class="col-md-12 m-b-20">
                                                                                <input type="text" class="form-control" placeholder="Email"> </div>
                                                                            <div class="col-md-12 m-b-20">
                                                                                <input type="text" class="form-control" placeholder="Phone"> </div>
                                                                            <div class="col-md-12 m-b-20">
                                                                                <input type="text" class="form-control" placeholder="Designation"> </div>
                                                                            <div class="col-md-12 m-b-20">
                                                                                <input type="text" class="form-control" placeholder="Age"> </div>
                                                                            <div class="col-md-12 m-b-20">
                                                                                <input type="text" class="form-control" placeholder="Date of joining"> </div>
                                                                            <div class="col-md-12 m-b-20">
                                                                                <input type="text" class="form-control" placeholder="Salary"> </div>
                                                                            <div class="col-md-12 m-b-20">
                                                                                <div class="fileupload btn btn-danger btn-rounded waves-effect waves-light"><span><i class="ion-upload m-r-5"></i>Upload Contact Image</span>
                                                                                    <input type="file" class="upload"> </div>
                                                                            </div>
                                                                        </div>
                                                                    </from>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Save</button>
                                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>
                                                    <td colspan="7">
                                                        <div class="text-right">
                                                            <ul class="pagination"> </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <!-- .left-aside-column-->
                                </div>
                                <!-- /.left-right-aside-column-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
            </div>

            <?php include('footer.php');?>

        </div>
    </div>
    <?php include('librerias.php');?>
</body>
</html>