<!DOCTYPE html>
<html lang="en">
<head>
<?php include('../head.php');?>
</head>

<body>
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>

    <section id="wrapper">
        <div class="login-register" style="background-image: url(../assets/images/big/bg-login.jpg);background-attachment: fixed;">
            <div class="login-box card">
                <div class="card-body">
                    <form class="form-horizontal form-material" id="loginform" action="index.php">
                        <img style="width: 100%;" src="../assets/images/logo-icon.png" alt="">
                        <h3 class="box-title m-b-20">Iniciar Sesión</h3>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" required type="email" id="email" placeholder="Correo"> </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" type="password" id="password" placeholder="Contraseña"> </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="checkbox checkbox-primary pull-left p-t-0">
                                    <span id="msg"></span>
                                </div> 
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button id="but" onclick="Loginadmin()" class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="button">Iniciar sesión</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <?php include('../librerias.php');?>
</body>

</html>