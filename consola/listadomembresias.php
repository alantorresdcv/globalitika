
<?php session_start(); 
if (isset($_SESSION['administrador'])) {

}
else{
    header("Location:../consola/login/");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include('head.php');?>
</head>

<body class="fix-header fix-sidebar card-no-border" onload="
busqueda()">
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>

    <div id="main-wrapper">

        <header class="topbar">
            <?php include('menu-top.php');?>
        </header>

        <aside class="left-sidebar">
            <?php include('menu-lateral.php');?>
        </aside>

        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Listado de membresias</h3>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <!-- .left-right-aside-column-->
                            <div class="contact-page-aside">
                                <div>
                                    <div class="right-page-header">
                                        <div class="d-flex">
                                            <div class="align-self-center">
                                                <h4 class="card-title m-t-10"></h4></div>
                                            <div class="ml-auto">
                                                <input type="text" id="busqueda" onkeyup="busqueda()" placeholder="Buscar" class="form-control"> </div>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="demo-foo-addrow" class="table m-t-30 table-hover no-wrap contact-list" data-page-size="10">
                                            <thead>
                                                <tr>
                                                    <th>Transacción</th>
                                                    <th>Usuario</th>
                                                    <th>Correo</th>
                                                    <th>Membresia</th>
                                                    <th>Acciones</th>
                                                    <th>Estatus</th>
                                                </tr>
                                            </thead>
                                            <tbody  id="tbody">
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- .left-aside-column-->
                                </div>
                                <!-- /.left-right-aside-column-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
            </div>

            <?php include('footer.php');?>

        </div>
    </div>
    <button class='boton' id='boton'>
                <span id='guardando'>Guardando...</span>
                <span id='guardado'>Guardado Exitosamente</span>
                <span id='error'>Error Al Guardar Verifique Su Conexion</span>
            </button>
    <?php include('librerias.php');?>
    <script type='text/javascript'>
    <?php 
    $Pubicaciones=mysql_query("SELECT * FROM uwqeruwbriwqh8423bw");
    while($fila=mysql_fetch_assoc($Pubicaciones)){
    @$ID=$fila["ID"];

echo"
      function  off".$ID."() 
      { 
        document.getElementById('on".$ID."').style.display = 'inherit';
        document.getElementById('off".$ID."').style.display = 'none';
        document.getElementById('on2".$ID."').style.display = 'inherit';
        document.getElementById('off2".$ID."').style.display = 'none';
        guardando.style.display = 'inherit';
        guardado.style.display = 'none';
        error.style.display = 'none';
        var ID = '".$ID."';
        var parametrosaguardar = {
                'ID' : ID
        };
        $.ajax({
            url: 'transaccioneslistado/on.php',
            type: 'POST',
            data: parametrosaguardar,
            success: function (data) {
            
            
                guardando.style.display = 'none';
                guardado.style.display = 'inherit';
                error.style.display = 'none';
                setTimeout(function(){ 
                guardado.style.display = 'none'; 
            },5000);     
            },
            error: function(){
                error.style.display = 'inherit';
                guardando.style.display = 'none';
                guardado.style.display = 'none';            
            }  
        });
    }
    function  on".$ID."() 
      { 
        document.getElementById('on".$ID."').style.display = 'none';
        document.getElementById('off".$ID."').style.display = 'inherit';
        document.getElementById('on2".$ID."').style.display = 'none';
        document.getElementById('off2".$ID."').style.display = 'inherit';
        guardando.style.display = 'inherit';
        guardado.style.display = 'none';
        error.style.display = 'none';
        var ID = '".$ID."';
        var parametrosaguardar = {
                'ID' : ID
        };
        $.ajax({
            url: 'transaccioneslistado/off.php',
            type: 'POST',
            data: parametrosaguardar,
            success: function (data) {
            
            
                guardando.style.display = 'none';
                guardado.style.display = 'inherit';
                error.style.display = 'none';
                setTimeout(function(){ 
                guardado.style.display = 'none'; 
            },5000);     
            },
            error: function(){
                error.style.display = 'inherit';
                guardando.style.display = 'none';
                guardado.style.display = 'none';            
            }  
        });
    }
    function  Actualizar".$ID."() 
      { 
        guardando.style.display = 'inherit';
        guardado.style.display = 'none';
        error.style.display = 'none';
        var nombre = $('#nombre".$ID."').val();
        var apellido = $('#apellido".$ID."').val();
        var correo = $('#correo".$ID."').val();
        var membresia = $('#membresia".$ID."').val();
        var ID = '".$ID."';
        var parametrosaguardar = {
                'nombre' : nombre,
                'apellido' : apellido,
                'correo' : correo,
                'membresia' : membresia,
                'ID' : ID
        };
        $.ajax({
            url: 'transaccioneslistado/actualizar.php',
            type: 'POST',
            data: parametrosaguardar,
            success: function (data) {
                var busqueda = document.getElementById('busqueda').value;
        var parametrosbuscar = {
            busqueda : busqueda
        };
        $.ajax({
            url: 'transaccioneslistado/consultas.php',
            type: 'GET',
            data: parametrosbuscar,
            success: function (data) {
                if (data!=0) {
                document.getElementById('tbody').innerHTML = data;
            }
            else{
                document.getElementById('tbody').innerHTML = 'No se encontraron resultados';
            }       
            },
            error: function(){
                document.getElementById('tbody').innerHTML = 'Error';
            }});
                guardando.style.display = 'none';
                guardado.style.display = 'inherit';
                error.style.display = 'none';
                setTimeout(function(){ 
                guardado.style.display = 'none'; 
            },5000);
                var tema = document.getElementById('Tema');
                tema.value = '';        
            },
            error: function(){
                error.style.display = 'inherit';
                guardando.style.display = 'none';
                guardado.style.display = 'none';            
            }  
        });
    }";
  }

  echo "
   function  validaEmail() 
      {
        var params = { 'Correo' : $('#correo').val() };
        $.ajax({
            url: 'transaccioneslistado/validarmail.php',
            type: 'POST',
            data: params,
            success: function (data) {
              if (data == 1) 
              {
                $('#smsmail').html('este correo ya existe');
                document.getElementById('but').disabled = true;
              } else {
                document.getElementById('but').disabled = false;
              }
            }  
        });
      }
        function  busqueda() 
      { 
        var busqueda = document.getElementById('busqueda').value;
        var parametrosbuscar = {
            busqueda : busqueda
        };
        $.ajax({
            url: 'transaccioneslistado/consultas.php',
            type: 'GET',
            data: parametrosbuscar,
            success: function (data) {
                if (data!=0) {
                document.getElementById('tbody').innerHTML = data;
            }
            else{
                document.getElementById('tbody').innerHTML = 'No se encontraron resultados';
            }       
            },
            error: function(){
                document.getElementById('tbody').innerHTML = 'Error';
            }});
        }
</script>";
     ?>

     
</body>
</html>