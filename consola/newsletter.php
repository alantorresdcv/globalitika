<?php session_start(); 
if (isset($_SESSION['administrador'])) {

}
else{
    header("Location:../consola/login/");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include('head.php');?>
</head>

<body class="fix-header fix-sidebar card-no-border" onload="
busqueda()">
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>

    <div id="main-wrapper">

        <header class="topbar">
            <?php include('menu-top.php');?>
        </header>

        <aside class="left-sidebar">
            <?php include('menu-lateral.php');?>
        </aside>

        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Newsletter</h3>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <!-- .left-right-aside-column-->
                            <div class="contact-page-aside">
                                <div>
                                    <div class="right-page-header">
                                        <div class="d-flex">
                                            <div class="align-self-center">
                                                <h4 class="card-title m-t-10"></h4></div>
                                            <div class="ml-auto">
                                                <input type="text" id="busqueda" onkeyup="busqueda()" placeholder="Buscar" class="form-control"> </div>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="demo-foo-addrow" class="table m-t-30 table-hover no-wrap contact-list" data-page-size="10">
                                            <thead>
                                                <tr>
                                                    <th>ID #</th>
                                                    <th>Correo electronico</th>
                                                </tr>
                                            </thead>
                                            <tbody  id="tbody">
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="2">
                                                        <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#add-contact">Agregar nuevo suscrito</button>
                                                    </td>
                                                    <div id="add-contact" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel">Agregar nuevo suscrito</h4>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                                                                </div>
                                                                <div class="modal-body">
                                                                    <from class="form-horizontal form-material">
                                                                        <div class="form-group">
                                                                            <div class="col-md-12 m-b-20">
                                                                                <input onkeyup="validaEmail()" id="correo" type="text" class="form-control" placeholder="correo"> <br><br> <span id="smsmail"></span></div>
                                                                        </div>
                                                                    </from>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button id="but" type="submit" data-dismiss="modal" onclick="Guardar()" class="btn btn-info waves-effect">Save</button>
                                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>
                                                    <td colspan="7">
                                                        <div class="text-right">
                                                            <ul class="pagination"> </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <!-- .left-aside-column-->
                                </div>
                                <!-- /.left-right-aside-column-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
            </div>

            <?php include('footer.php');?>

        </div>
    </div>
    <button class='boton' id='boton'>
                <span id='guardando'>Guardando...</span>
                <span id='guardado'>Guardado Exitosamente</span>
                <span id='error'>Error Al Guardar Verifique Su Conexion</span>
            </button>
    <?php include('librerias.php');?>
    <script type='text/javascript'>
    <?php 
    $Pubicaciones=mysql_query("SELECT * FROM nwqeouwuiofbufwfsfswer");
	while($fila=mysql_fetch_assoc($Pubicaciones)){
	@$ID=$fila["ID"];

echo"
	function Borrar".@$ID."(){
		var borrar = document.getElementById('$ID');
		var ID_Publicacion = '".$ID."';
        var parametros = {
                'ID' : ID_Publicacion
            }
		$.ajax({
            url: 'newsletter/borrar.php',
            type: 'POST',
            data: parametros,
            success: function () {
            	borrar.style.display = 'none';
            },
            error: function(){
            	           
            }  
        });
	}
    function  validaEmail".$ID."() 
      {
        var paramscorreo = { 'Correo' : $('#correo".$ID."').val() };
        console.log(paramscorreo)
        $.ajax({
            url: 'newsletter/validarmail.php',
            type: 'POST',
            data: paramscorreo,
            success: function (data) {
              if (data == 1) 
              {
                $('#smsmail".$ID."').html('este correo ya existe');
                document.getElementById('but".$ID."').disabled = true;
              } else {
                document.getElementById('but".$ID."').disabled = false;

              }
            }  
        });
      }
    function  Actualizar".$ID."() 
      { 
        guardando.style.display = 'inherit';
        guardado.style.display = 'none';
        error.style.display = 'none';
        var correo = $('#correo".$ID."').val();
        var ID = '".$ID."';
        var parametrosaguardar = {
                'correo' : correo,
                'ID' : ID
        };
        $.ajax({
            url: 'newsletter/actualizar.php',
            type: 'POST',
            data: parametrosaguardar,
            success: function (data) {
                var busqueda = document.getElementById('busqueda').value;
        var parametrosbuscar = {
            busqueda : busqueda
        };
        $.ajax({
            url: 'newsletter/consulta.php',
            type: 'GET',
            data: parametrosbuscar,
            success: function (data) {
                if (data!=0) {
                document.getElementById('tbody').innerHTML = data;
            }
            else{
                document.getElementById('tbody').innerHTML = 'No se encontraron resultados';
            }       
            },
            error: function(){
                document.getElementById('tbody').innerHTML = 'Error';
            }});
                guardando.style.display = 'none';
                guardado.style.display = 'inherit';
                error.style.display = 'none';
                setTimeout(function(){ 
                guardado.style.display = 'none'; 
            },5000);
                var tema = document.getElementById('Tema');
                tema.value = '';        
            },
            error: function(){
                error.style.display = 'inherit';
                guardando.style.display = 'none';
                guardado.style.display = 'none';            
            }  
        });
    }";
  }

  echo "
  function  Guardar() 
      {	
        guardando.style.display = 'inherit';
        guardado.style.display = 'none';
        error.style.display = 'none';
        var correo = $('#correo').val();
        var parametrosaguardar = {
                'correo' : correo
        };
        $.ajax({
            url: 'newsletter/guardar.php',
            type: 'POST',
            data: parametrosaguardar,
            success: function (data) {
            	if (data!=0) {
            	var insertar = document.getElementById('tbody');
				insertar.insertAdjacentHTML('beforeend',data);
				}
            	guardando.style.display = 'none';
            	guardado.style.display = 'inherit';
            	error.style.display = 'none';
            	setTimeout(function(){ 
            	guardado.style.display = 'none'; 
			},5000);
            	var tema = document.getElementById('Tema');
				tema.value = '';       	
            },
            error: function(){
            	error.style.display = 'inherit';
            	guardando.style.display = 'none';
            	guardado.style.display = 'none';            
            }  
        });
    }
   function  validaEmail() 
      {
        var params = { 'Correo' : $('#correo').val() };
        $.ajax({
            url: 'newsletter/validarmail.php',
            type: 'POST',
            data: params,
            success: function (data) {
              if (data == 1) 
              {
                $('#smsmail').html('este correo ya existe');
                document.getElementById('but').disabled = true;
              } else {
                $('#smsmail').html('');
                document.getElementById('but').disabled = false;
              }
            }  
        });
      }
        function  busqueda() 
      { 
        var busqueda = document.getElementById('busqueda').value;
        var parametrosbuscar = {
            busqueda : busqueda
        };
        $.ajax({
            url: 'newsletter/consulta.php',
            type: 'GET',
            data: parametrosbuscar,
            success: function (data) {
                if (data!=0) {
                document.getElementById('tbody').innerHTML = data;
            }
            else{
                document.getElementById('tbody').innerHTML = 'No se encontraron resultados';
            }       
            },
            error: function(){
                document.getElementById('tbody').innerHTML = 'Error';
            }});
        }
</script>";
     ?>

     
</body>
</html>
