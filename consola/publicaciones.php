
<?php session_start(); 
if (isset($_SESSION['administrador'])) {

}
else{
    header("Location:../consola/login/");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="ckeditor/ckeditor.js"></script>
    <script src="ckeditor/samples/js/sample.js"></script>
    <link rel="stylesheet" href="ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css">
    <script src='https://code.jquery.com/jquery-1.11.1.min.js'></script>
<?php include('head.php');?>

</head>

<body class="fix-header fix-sidebar card-no-border" onload="
busqueda()">
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>

    <div id="main-wrapper">

        <header class="topbar">
            <?php include('menu-top.php');?>
        </header>

        <aside class="left-sidebar">
            <?php include('menu-lateral.php');?>
        </aside>

        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Publicaciones</h3>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <!-- .left-right-aside-column-->
                            <div class="contact-page-aside">
                                <div>
                                    <div class="right-page-header">
                                        <div class="d-flex">
                                            <div class="align-self-center">
                                                <h4 class="card-title m-t-10"></h4></div>
                                            <div class="ml-auto">
                                                <input type="text" id="busqueda" onkeyup="busqueda()" placeholder="Buscar" class="form-control"> </div>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="demo-foo-addrow" class="table m-t-30 table-hover no-wrap contact-list" data-page-size="10">
                                            <thead>
                                                <tr>
                                                    <th>Titulo</th>
                                                    <th>Autor</th>
                                                    <th class='text-center'>Img</th>
                                                    <th>Categoria</th>
                                                    <th>Tema</th>
                                                    <th>Fecha</th>
                                                    <th>Premium</th>
                                                    <th>Destac.</th>
                                                    <th>Acciones</th>
                                                    <th>Estatus</th>
                                                </tr>
                                            </thead>
                                            <tbody  id="tbody">
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="2">
                                                        <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#add-categoria">Agregar Publicación</button>
                                                    </td>
                                                    <div id='add-categoria' class='modal fade in' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
                                                        <div class='modal-dialog' style='max-width: 65%;'>
                                                            <div class='modal-content'>
                                                                <div class='modal-header'>
                                                                    <h4 class='modal-title' id='myModalLabel'>Agregar Publicacion</h4>
                                                                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>

                                                                </div>
                                                                <form enctype='multipart/form-data' id='formuploadajaxadd' method='POST'>
                                                                <div class='modal-body'>
                                                                    <from class='form-horizontal form-material'>
                                                                        <div class='form-group'>
                                                                            <div class='col-md-12 m-b-20'>
                                                                                <input name='titulo' id='titulo' type='text' class='form-control' placeholder='Titulo'> </div>
                                                                                <div class='col-md-12 m-b-20'>
                                                                                    <label>Autor: </label>
                                                                                    <select name='Autor' class='form-control'>
                                                                                    <?php
                                                                                    $autor=mysql_query("SELECT * FROM aewrwf9345233dasde ORDER BY Nombre_Autor ASC");
                                                                                    while($fila=mysql_fetch_assoc($autor)){
                                                                                    @$ID_Autor=$fila["ID_Autor"];
                                                                                    @$Nombre=$fila["Nombre_Autor"];
                                                                                    echo"
                                                                                    <option value='$ID_Autor'>$Nombre</option>
                                                                                    ";}?>
                                                                                    <option selected disabled>Seleccione Autor</option>
                                                                                    </select>

                                                                                </div>
                                                                                
                                                                                 <div class='col-md-12 m-b-20'>
                                                                                    <label>Portada:</label>
                                                                                <input name='archivo' type='file' id='archivo' name='archivo'/> </div>
                                                                                <div class='col-md-12 m-b-20'>
                                                                                <select id="mySelect" onchange="myFunction()" name='categoria' class='form-control'>
                                                                                <?php
                                                                                $Pubicaciones=mysql_query("SELECT * FROM coewerw9hfwefouwefsdf");
                                                                                while($fila=mysql_fetch_assoc($Pubicaciones)){
                                                                                @$IDCategoria=$fila["ID_Categoria"];
                                                                                @$Nombre=$fila["Nombre_Categoria"];
                                                                                echo"
                                                                                <option value='$IDCategoria'>$Nombre</option>
                                                                                ";}?>
                                                                                <option selected disabled>Seleccione categoria</option>
                                                                                </select>

                                                                                 </div>
                                                                                 <div class='col-md-12 m-b-20'>
                                                                                <select name='tema' class='form-control'>
                                                                                <?php
                                                                                $tema=mysql_query("SELECT * FROM tr02j0wer82350j2asdw");
                                                                                while($fila=mysql_fetch_assoc($tema)){
                                                                                @$ID_Tema=$fila["ID_Tema"];
                                                                                @$Nombre=$fila["Nombre_Tema"];
                                                                                @$Categoria=$fila["IDCategoria"];
                                                                                echo"
                                                                                <option  style='display: none;' class='opcion$Categoria' value='$ID_Tema'>$Nombre</option>
                                                                                ";}?>
                                                                                <option selected disabled>Seleccione tema</option>
                                                                                </select>
                                                                                
                                                                                 </div>
                                                                                 <div class='col-md-12 m-b-20'>
                                                                                    <label>Descripción corta:</label>
                                                                                    <textarea class="ckeditor" name="DescripcionCorta" placeholder=" Descripcion Corta o resumen">
                                                                                        Introdusca un breve resumen para mostrar a los usuarios free no inserte imagenes
                                                                                    </textarea>
                                                                                </div>
                                                                                <div class='col-md-12 m-b-20'>
                                                                                    <textarea class="ckeditor" name='DescripcionLarga' placeholder=""><h1>pegue aqui su documento y editelo</h1></textarea>
                                                                                </div>
                                                                                <div class='col-md-12 m-b-20'>
                                                                                    <label>Archivo Pdf:</label>
                                                                                <input name='Pdf' type='file' id='Pdf'/> </div>
                                                                                

                                                                                <div class='col-md-12 m-b-20'>
                                                                                    <input name='fecha' id='fecha' type='datetime-local' value='2019-01-01T00:00' class='form-control' >
                                                                                </div>
                                                                                <div class='col-md-12 m-b-20'>
                                                                                    <label>Premium:</label>
                                                                                <select name='premium' class='form-control'>";
                                                                                <option value='1'>Si</option>
                                                                                <option value='0'>No</option>";
                                                                                </select>
                                                                                 </div>
                                                                                <div class='col-md-12 m-b-20'>
                                                                                    <label>Destacado:</label>
                                                                                <select name='destacado' class='form-control'>";
                                                                                    <option selected value='1'>Si</option>
                                                                                    <option value='0'>No</option>";
                                                                                    
                                                                                </select>
                                                                                </div>
                                                                                <div class='col-md-12 m-b-20'>
                                                                                    <label>Estatus</label>
                                                                                <select name='estatus' class='form-control'>";
                                                                                    <option selected value='1'>Activo</option>
                                                                                    <option value='0'>Oculto</option>";
                                                                                    
                                                                                </select>
                                                                                </div>
                                                                                
                                                                                
                                                                        </div>
                                                                    </from>
                                                                </div>
                                                                <div class='modal-footer'>
                                                                    <button id='but' type='submit' data-dismiss='modal' onclick='guardar()' class='btn btn-info waves-effect'>Save</button>
                                                                    <button type='button' class='btn btn-default waves-effect' data-dismiss='modal'>Cancel</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>
                                                    <td colspan="7">
                                                        <div class="text-right">
                                                            <ul class="pagination"> </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <!-- .left-aside-column-->
                                </div>
                                <!-- /.left-right-aside-column-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
            </div>

            <?php include('footer.php');?>

        </div>
    </div>  
    <?php 
    $libros=mysql_query("SELECT * FROM l32ddiasfbafbiatreiasdfw INNER JOIN coewerw9hfwefouwefsdf ON Categoria = ID_Categoria INNER JOIN tr02j0wer82350j2asdw ON Tema = ID_Tema");
                    while($fila=mysql_fetch_assoc($libros)){
                        @$ID=$fila["ID"];
                        @$Titulo=$fila["Titulo"];
                        @$IDAutor=$fila["ID_Autor"];
                        @$Autor=$fila["Nombre_Autor"];
                        @$Portada=$fila["Portada"];
                        @$Categoria=$fila["IDCategoria"];
                        @$Nombre_Categoria=$fila["Nombre_Categoria"];
                        @$Tema=$fila["Tema"];
                        @$Nombre_Tema=$fila["Nombre_Tema"];
                        @$Fecha=$fila["Fecha"];
                        @$Premium=$fila["Premium"];
                        @$Destacado=$fila["Destacado"];
                        @$Estatus=$fila["Estatus"];
                        @$DescripcionLarga=$fila["DescripcionLarga"];
                        @$DescripcionCorta=$fila["DescripcionCorta"];
    echo"
    <div id='edit-contact".$ID."' class='modal fade in' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
                                                        <div class='modal-dialog' style='max-width: 65%;'>
                                                             <form enctype='multipart/form-data' id='formuploadajax".$ID."' method='POST'>
                                                            <div class='modal-content'>
                                                                <div class='modal-header'>
                                                                    <h4 class='modal-title' id='myModalLabel'>Editar Publicacion</h4>
                                                                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>

                                                                </div>
                                                               
                                                                <div class='modal-body'>
                                                                    <from class='form-horizontal form-material'>
                                                                        <div class='form-group'>
                                                                            <div class='col-md-12 m-b-20'>
                                                                                <label>Titulo: </label>
                                                                                <input name='ID' id='ID".$ID."' type='hidden' value='$ID'>
                                                                                <input name='titulo' id='titulo".$ID."' type='text' class='form-control' placeholder='Titulo' value='$Titulo'> </div>
                                                                            <div class='col-md-12 m-b-20'>
                                                                                <label>Autor: </label>
                                                                                <select name='Autor' class='form-control'>
                                                                                    ";
                                                                                    $autor=mysql_query("SELECT * FROM aewrwf9345233dasde ORDER BY Nombre_Autor ASC");
                                                                                    while($fila=mysql_fetch_assoc($autor)){
                                                                                    @$ID_Autor=$fila["ID_Autor"];
                                                                                    @$Nombre=$fila["Nombre_Autor"];
                                                                                    if ($IDAutor==$ID_Autor) {
                                                                                        echo "<option selected value='$ID_Autor'>$Nombre</option>";
                                                                                    }
                                                                                    else{
                                                                                        echo"
                                                                                    <option value='$ID_Autor'>$Nombre</option>
                                                                                    ";
                                                                                    }
                                                                                    }
                                                                                    echo"
                                                                                    </select>
                                                                                    </div>
                                                                                 <div class='col-md-12 m-b-20'>
                                                                                 <label>Portada: </label>
                                                                                <input name='archivo".$ID."' type='file' id='archivo".$ID."' name='archivo".$ID."'/> </div>
                                                                                <div class='col-md-12 m-b-20'>
                                                                                    <label>Categoria:</label>
                                                                                <select id='mySelect$ID' onchange='myFunction$ID()' name='categoria' class='form-control'>
                                                                               

                                                                                ";
                                                                                $Pubicaciones=mysql_query("SELECT * FROM coewerw9hfwefouwefsdf");
                                                                                while($fila=mysql_fetch_assoc($Pubicaciones)){
                                                                                @$IDCategoria=$fila["ID_Categoria"];
                                                                                @$Nombre=$fila["Nombre_Categoria"];
                                                                                if ($IDCategoria==$Categoria) {
                                                                                    echo " <option selected value='$Categoria'>$Nombre_Categoria</option>";
                                                                                }else{


                                                                                echo"
                                                                                <option value='$IDCategoria'>$Nombre</option>
                                                                                ";}
                                                                            }?>
                                                                                </select>

                                                                                 </div>
                                                                                 <div class='col-md-12 m-b-20'>
                                                                                    <label>Tema:</label>
                                                                                <select name='tema' class='form-control'>
                                                                                <?php
                                                                                $tema=mysql_query("SELECT * FROM tr02j0wer82350j2asdw");
                                                                                while($fila=mysql_fetch_assoc($tema)){
                                                                                @$ID_Tema=$fila["ID_Tema"];
                                                                                @$Nombre=$fila["Nombre_Tema"];
                                                                                @$Categoria=$fila["IDCategoria"];
                                                                                if ($ID_Tema==$Tema) {
                                                                                    echo " <option selected value='$ID_Tema' class='opcion$Categoria'>$Nombre_Tema</option>";
                                                                                }else{


                                                                                echo"
                                                                                <option style='display: none;' class='opcion$Categoria' value='$ID_Tema'>$Nombre</option>
                                                                                ";}
                                                                                }
                                                                                echo"
                                                                                 </div>
                                                                                 <div class='col-md-12 m-b-20'>
                                                                                    <label>Descripción corta:</label>
                                                                                    <textarea class='ckeditor' name='DescripcionCorta".$ID."' placeholder='Descripcion Corta o resumen'>$DescripcionCorta</textarea>
                                                                                </div>
                                                                                <div class='col-md-12 m-b-20'>
                                                                                    <textarea class='ckeditor' name='DescripcionLarga".$ID."' placeholder=''>$DescripcionLarga</textarea>
                                                                                </div>
                                                                                <div class='col-md-12 m-b-20'>
                                                                                    <label>Archivo Pdf:</label>
                                                                                <input name='Pdf' type='file' id='Pdf'/> 
                                                                                </div>
                                                                                <div class='col-md-12 m-b-20'>
                                                                                    <label>Fecha:</label>
                                                                                    <input name='fecha' id='fecha".$ID."' type='datetime-local' value='' class='form-control'  value='$Fecha'>
                                                                                </div>
                                                                                <div class='col-md-12 m-b-20'>
                                                                                    <label>Premium:</label>
                                                                                <select name='premium' class='form-control'>";
                                                                                    if ($Premium==1) {
                                                                                        echo "<option selected value='1'>Si</option>
                                                                                        <option value='0'>No</option>";
                                                                                    }
                                                                                    else{
                                                                                        echo "<option value='1'>Si</option>
                                                                                        <option selected value='0'>No</option>";
                                                                                    }
                                                                                    echo "
                                                                                </select>
                                                                                 </div>
                                                                                <div class='col-md-12 m-b-20'>
                                                                                    <label>Destacado:</label>
                                                                                <select name='destacado' class='form-control'>";
                                                                                    if ($Destacado==1) {
                                                                                        echo "<option selected value='1'>Si</option>
                                                                                        <option value='0'>No</option>";
                                                                                    }
                                                                                    else{
                                                                                        echo "<option value='1'>Si</option>
                                                                                        <option selected value='0'>No</option>";
                                                                                    }
                                                                                    echo "
                                                                                </select>
                                                                                </div>
                                                                                <div class='col-md-12 m-b-20'>
                                                                                    <label>Estatus</label>
                                                                                <select name='estatus' class='form-control'>";
                                                                                    if ($Estatus==1) {
                                                                                        echo "<option selected value='1'>Activo</option>
                                                                                        <option value='0'>Oculto</option>";
                                                                                    }
                                                                                    else{
                                                                                        echo "<option value='1'>Activo</option>
                                                                                        <option selected value='0'>Oculto</option>";
                                                                                    }
                                                                                    echo "
                                                                                </select>
                                                                                </div>
                                                                                
                                                                                
                                                                        </div>
                                                                    </from>
                                                                </div>
                                                                <div class='modal-footer'>
                                                                    <button id='but".$ID."' type='submit' data-dismiss='modal' onclick='actualizar".$ID."()' class='btn btn-info waves-effect'>Save</button>
                                                                    <button type='button' class='btn btn-default waves-effect' data-dismiss='modal'>Cancel</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>
                                                    <div id='view-contact".$ID."' class='modal fade in' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
                                                        <div class='modal-dialog'>
                                                             <form enctype='multipart/form-data' id='formuploadajax".$ID."' method='POST'>
                                                            <div class='modal-content'>
                                                                <div class='modal-header'>
                                                                    <h4 class='modal-title' id='myModalLabel'>$Titulo</h4>
                                                                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>

                                                                </div>
                                                               
                                                                <div class='modal-body'>
                                                                    <from class='form-horizontal form-material'>
                                                                        <div class='form-group'>
                                                                            <div class='col-md-12 m-b-20'>
                                                                                <label>Titulo: </label>
                                                                                $Titulo
                                                                            </div>
                                                                            <div class='col-md-12 m-b-20'>
                                                                                <label>Autor: </label>
                                                                                $Autor
                                                                            </div>
                                                                            <div class='col-md-12 m-b-20'>
                                                                                <label>Portada: </label>
                                                                                <img src='$Portada' style='width: 100%;'>
                                                                            </div>
                                                                                <div class='col-md-12 m-b-20'>
                                                                                    <label>Tema:</label>
                                                                                ";
                                                                                $Pubicaciones=mysql_query("SELECT * FROM coewerw9hfwefouwefsdf WHERE ID_Categoria='$Categoria'");
                                                                                $fila=mysql_fetch_assoc($Pubicaciones);
                                                                                @$IDCategoria=$fila["ID_Categoria"];
                                                                                @$Nombre=$fila["Nombre_Categoria"];
                                                                                echo "$Nombre
                                                                                 </div>
                                                                                <div class='col-md-12 m-b-20'>
                                                                                    <label>Fecha:</label>
                                                                                    $Fecha
                                                                                </div>
                                                                                <div class='col-md-12 m-b-20'>
                                                                                    <label>Premium:</label>
                                                                                ";
                                                                                    if ($Premium==1) {
                                                                                        echo"Si";
                                                                                    }
                                                                                    else{
                                                                                        echo "No";
                                                                                    }
                                                                                    echo "
                                                                                 </div>
                                                                                <div class='col-md-12 m-b-20'>
                                                                                    <label>Destacado:</label>
                                                                                ";
                                                                                    if ($Destacado==1) {
                                                                                        echo "Si";
                                                                                    }
                                                                                    else{
                                                                                        echo "No";
                                                                                    }
                                                                                    echo "
                                                                                </div>
                                                                                <div class='col-md-12 m-b-20'>
                                                                                    <label>Estatus</label>
                                                                                ";
                                                                                    if ($Estatus==1) {
                                                                                        echo "Activo";
                                                                                    }
                                                                                    else{
                                                                                        echo "Oculto";
                                                                                    }
                                                                                    echo "
                                                                                </select>
                                                                                </div>
                                                                                
                                                                                
                                                                        </div>
                                                                    </from>
                                                                </div>
                                                                
                                                            </div>
                                                        </form>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>";}
                                                    ?>
    <button class='boton' id='boton'>
                <span id='guardando'>Guardando...</span>
                <span id='guardado'>Guardado Exitosamente</span>
                <span id='error'>Error Al Guardar Verifique Su Conexion</span>
            </button>
    <?php include('librerias.php');
    echo"
    <script src='https://code.jquery.com/jquery-1.11.1.min.js'></script>
    <script type='text/javascript'>
        function myFunction() {
    var select = document.getElementById('mySelect').value;";
    $Pubicaciones=mysql_query("SELECT * FROM coewerw9hfwefouwefsdf");
    while($fila=mysql_fetch_assoc($Pubicaciones)){
    @$ID=$fila["ID_Categoria"];

echo"
    if (select == '$ID') {
        var option = document.getElementsByClassName('opcion$ID');
        var i;
        for (i = 0; i < option.length; i++) {
        option[i].style.display = 'initial';
      }
    }
    else{
        var option = document.getElementsByClassName('opcion$ID');
        var i;
        for (i = 0; i < option.length; i++) {
        option[i].style.display = 'none';
      }
    }";
}
    echo"}";
    
    $Pubicaciones=mysql_query("SELECT * FROM l32ddiasfbafbiatreiasdfw");
	while($fila=mysql_fetch_assoc($Pubicaciones)){
	@$ID=$fila["ID"];
    @$Fecha=$fila["Fecha"];

echo"

    var cadena = '$Fecha';
    patron = ' ';
    nuevoValor    = 'T';
    nuevaCadena = cadena.replace(patron, nuevoValor);
    document.getElementById('fecha".$ID."').value = nuevaCadena;
    function myFunction$ID() {
    var select$ID = document.getElementById('mySelect$ID').value;
     console.log(select$ID);
    ";
    $categorias=mysql_query("SELECT * FROM coewerw9hfwefouwefsdf");
    while($filacategoria=mysql_fetch_assoc($categorias)){
    @$ID_Categoria=$filacategoria["ID_Categoria"];

echo"
   
    if (select$ID == '$ID_Categoria') {
        var option = document.getElementsByClassName('opcion$ID_Categoria');
        console.log(option);
        var i;
        for (i = 0; i < option.length; i++) {
        option[i].style.display = 'initial';
      }
    }
    else{
        var option = document.getElementsByClassName('opcion$ID_Categoria');
        console.log(option);
        var i;
        for (i = 0; i < option.length; i++) {
        option[i].style.display = 'none';
      }
    }
";
}
    echo"}
	function Borrar".@$ID."(){
		var borrar = document.getElementById('$ID');
		var ID_Publicacion = '".$ID."';
        var parametros = {
                'ID' : ID_Publicacion
            }
		$.ajax({
            url: 'publicaciones/borrar.php',
            type: 'POST',
            data: parametros,
            success: function () {
            	borrar.style.display = 'none';
            },
            error: function(){
            	           
            }  
        });
	}
    function actualizar".$ID."(){
            CKEDITOR.instances.DescripcionCorta".$ID.".destroy();
            CKEDITOR.instances.DescripcionLarga".$ID.".destroy();
            setTimeout(function(){
            guardando.style.display = 'inherit';
            guardado.style.display = 'none';
            error.style.display = 'none';
            var formData = new FormData(document.getElementById('formuploadajax".$ID."'));
            formData.append('dato', 'valor');
            //formData.append(f.attr('name'), $(this)[0].files[0]);
            $.ajax({
                url: 'publicaciones/actualizar.php',
                type: 'post',
                dataType: 'html',
                data: formData,
                cache: false,
                contentType: false,
         processData: false
            })
                .done(function(res){
                    location.href = 'publicaciones.php';
                    });
        }, 500);};
";}
  echo "
  function  guardar() 
      {	
        CKEDITOR.instances.DescripcionCorta.destroy();
        CKEDITOR.instances.DescripcionLarga.destroy();
        setTimeout(function(){
        guardando.style.display = 'inherit';
            guardado.style.display = 'none';
            error.style.display = 'none';
            var formData = new FormData(document.getElementById('formuploadajaxadd'));
            formData.append('dato', 'valor');
            //formData.append(f.attr('name'), $(this)[0].files[0]);
            $.ajax({
                url: 'publicaciones/guardar.php',
                type: 'post',
                dataType: 'html',
                data: formData,
                cache: false,
                contentType: false,
         processData: false
            })
                .done(function(res){
                    console.log(res);
                if (res==1) {
                    alert('Todos los campos son requeridos por favor Verifique he intente nuevamente');
                }
            var busqueda = document.getElementById('busqueda').value;
        var parametrosbuscar = {
            busqueda : busqueda
        };
            $.ajax({
            url: 'publicaciones/consultas.php',
            type: 'GET',
            data: parametrosbuscar,
            success: function (data) {
                if (data!=0) {
                document.getElementById('tbody').innerHTML = data;
            }
            else{
                document.getElementById('tbody').innerHTML = 'No se encontraron resultados';
            }       
            },
            error: function(){
                document.getElementById('tbody').innerHTML = 'Error';
            }});
            guardando.style.display = 'none';
                guardado.style.display = 'inherit';
                error.style.display = 'none';
                setTimeout(function(){ 
                guardado.style.display = 'none'; 
            },5000);
                });
        initSample();}, 500);

        }
    
   function  validaEmail() 
      {
        var params = { 'Correo' : $('#correo').val() };
        $.ajax({
            url: 'administradores/validarmail.php',
            type: 'POST',
            data: params,
            success: function (data) {
              if (data == 1) 
              {
                $('#smsmail').html('este correo ya existe');
                document.getElementById('but').disabled = true;
              } else {
                document.getElementById('but').disabled = false;
              }
            }  
        });
      }
        function  busqueda() 
      { 
        var busqueda = document.getElementById('busqueda').value;
        var parametrosbuscar = {
            busqueda : busqueda
        };
        $.ajax({
            url: 'publicaciones/consultas.php',
            type: 'GET',
            data: parametrosbuscar,
            success: function (data) {
                if (data!=0) {
                document.getElementById('tbody').innerHTML = data;
            }
            else{
                document.getElementById('tbody').innerHTML = 'No se encontraron resultados';
            }       
            },
            error: function(){
                document.getElementById('tbody').innerHTML = 'Error';
            }});
        }
        initSample();
</script>

";
     ?>

     
</body>
</html>


