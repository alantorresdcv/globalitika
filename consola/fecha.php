<?php
$cumple = "2019-01-07";
list( $ano, $mes, $dia ) = explode( "-", $cumple );
    $dia_cumple = date( "z", mktime( 0, 0, 0, $mes, $dia, $ano ) );
    $dia_actual = date( "z" );
    $dias_restantes = $dia_cumple - $dia_actual;
    if ( $dias_restantes < 0 ) {
        $dias_restantes += 365;
        if ( date( "L", mktime( 0, 0, 0, 0, 0, $ano+1 ) ) ) {
            if ( date( "m", mktime( 0, 0, 0, $mes, $dia, $ano ) ) > 2 ) {
                $dias_restantes++;
            }
        }
    }
    echo $dias_restantes;
 
?>