<!-- Sidebar scroll-->
<div class="scroll-sidebar">
    <!-- User profile -->
    <div class="user-profile" style="background: url(assets/images/background/user-info.jpg) no-repeat;">
        <!-- User profile image -->
        <div class="profile-img"> <img src="assets/images/users/profile.png" alt="user" /> </div>
    </div>
    <!-- End User profile text-->
    <!-- Sidebar navigation-->
    <nav class="sidebar-nav">
        <ul id="sidebarnav">
            <li> 
                <ul>

                    <li><a href="administradores.php">Administradores</a></li>
                    
                    <li><a href="nuevastransacciones.php">Nuevas Transacciones </a></li>
                    <li><a href="renovacionesproximas.php">Renovaciones proximas</a></li>
                    <li><a href="listadomembresias.php">Listado de membresias</a></li>
                    <li><a href="publicaciones.php">Publicaciones</a></li>
                    <li><a href="autores.php">Autores</a></li>
                    <li><a href="newsletter.php">Newsletter</a></li>
                    <li><a href="categorias.php">Categorias</a></li>
                    <li><a href="temas.php">Temas</a></li>

                </ul>
            </li>
        </ul>
    </nav>
    <!-- End Sidebar navigation -->
</div>
<!-- End Sidebar scroll-->
<!-- Bottom points-->
<div class="sidebar-footer">
    <!-- item--><a href="<?php echo "$Link/Register/cerrarsesion.php" ?>" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a> </div>
<!-- End Bottom points-->