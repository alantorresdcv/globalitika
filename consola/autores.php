
<?php session_start(); 
if (isset($_SESSION['administrador'])) {

}
else{
    header("Location:../consola/login/");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include('head.php');?>
</head>

<body class="fix-header fix-sidebar card-no-border" onload="
busqueda()">
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>

    <div id="main-wrapper">

        <header class="topbar">
            <?php include('menu-top.php');?>
        </header>

        <aside class="left-sidebar">
            <?php include('menu-lateral.php');?>
        </aside>

        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Autores</h3>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <!-- .left-right-aside-column-->
                            <div class="contact-page-aside">
                                <div>
                                    <div class="right-page-header">
                                        <div class="d-flex">
                                            <div class="align-self-center">
                                                <h4 class="card-title m-t-10"></h4></div>
                                            <div class="ml-auto">
                                                <input type="text" id="busqueda" onkeyup="busqueda()" placeholder="Buscar" class="form-control"> </div>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="demo-foo-addrow" class="table m-t-30 table-hover no-wrap contact-list" data-page-size="10">
                                            <thead>
                                                <tr>
                                                    <th>ID #</th>
                                                    <th>Nombre</th>
                                                    <th>Ocupacion</th>
                                                    <th>Cantidad de Articulos</th>
                                                    <th>Foto</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody  id="tbody">
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="2">
                                                        <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#add-contact">Agregar nuevo autor</button>
                                                    </td>
                                                    <div id="add-contact" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel">Agregar nuevo autor</h4>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                                                                </div>
                                                                <div class="modal-body">
                                                                	<form enctype='multipart/form-data' id='formuploadajaxadd' method='POST'>
                                                                    <from class="form-horizontal form-material">
                                                                        <div class="form-group">
                                                                            <div class="col-md-12 m-b-20">
                                                                                <input id="Nombre" type="text" class="form-control" placeholder="Nombre" name="Nombre"> </div>
                                                                            <div class="col-md-12 m-b-20">
                                                                                <textarea style='height:200px' id='Ocupacion' name='Ocupacion' type='text' class='form-control' placeholder='Ocupacion' ></textarea> </div>
                                                                            
                                                                                <div class="col-md-12 m-b-20">
                                                                                <input name='Foto' type='file' id='Foto' name='Foto'/> <br><br></div>
                                                                                
                                                                        </div>
                                                                    </from>
                                                                </form>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button id="but" type="submit" data-dismiss="modal" onclick="Guardar()" class="btn btn-info waves-effect">Save</button>
                                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>
                                                    <td colspan="7">
                                                        <div class="text-right">
                                                            <ul class="pagination"> </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <!-- .left-aside-column-->
                                </div>
                                <!-- /.left-right-aside-column-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
            </div>

            <?php include('footer.php');?>

        </div>
    </div>
    <button class='boton' id='boton'>
                <span id='guardando'>Guardando...</span>
                <span id='guardado'>Guardado Exitosamente</span>
                <span id='error'>Error Al Guardar Verifique Su Conexion</span>
            </button>
    <?php include('librerias.php');
    $Administradores=mysql_query("SELECT * FROM aewrwf9345233dasde");
					while($fila=mysql_fetch_assoc($Administradores)){
						@$ID=$fila["ID_Autor"];
						@$Nombre=$fila["Nombre_Autor"];
						@$Ocupacion=$fila["Ocupacion"];
						@$FotoAutor=$fila["FotoAutor"];
    echo"
    <div id='edit-contact".$ID."' class='modal fade in' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
                                                        <div class='modal-dialog'>
                                                            <div class='modal-content'>
                                                                <div class='modal-header'>
                                                                    <h4 class='modal-title' id='myModalLabel'>Editar autor</h4>
                                                                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>

                                                                </div>
                                                                <div class='modal-body'>
                                                                    <form enctype='multipart/form-data' id='formuploadajaxadd".$ID."' method='POST'>
                                                                        <input type='hidden' name='ID' value='$ID'>
                                                                    <from class='form-horizontal form-material'>
                                                                        <div class='form-group'>
                                                                            <div class='col-md-12 m-b-20'>
                                                                                <strong>Nombre autor:</strong>
                                                                                <input name='Nombre' id='Nombre".$ID."' type='text' class='form-control' placeholder='Nombre' value='$Nombre'> </div>
                                                                            <div class='col-md-12 m-b-20'><strong>Ocupacion:</strong>
                                                                                <textarea style='height:200px' id='Ocupacion".$ID."' type='text' name='Ocupacion' class='form-control' placeholder='Ocupacion' >$Ocupacion</textarea>
                                                                                </div> 
                                                                                <div class='col-md-12 m-b-20'><strong>Foto Autor:</strong>
                                                                                <input name='Foto' type='file' id='Foto' name='Foto'/>
                                                                                </div>   
                                                                        </div>
                                                                    </from>
                                                                </form>
                                                                </div>
                                                                <div class='modal-footer'>
                                                                    <button id='but".$ID."' type='submit' data-dismiss='modal' onclick='Actualizar".$ID."()' class='btn btn-info waves-effect'>Save</button>
                                                                    <button type='button' class='btn btn-default waves-effect' data-dismiss='modal'>Cancel</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>";
                                                }
    ?>
    <script type='text/javascript'>
    <?php 
    $Pubicaciones=mysql_query("SELECT * FROM aewrwf9345233dasde");
	while($fila=mysql_fetch_assoc($Pubicaciones)){
	@$ID=$fila["ID_Autor"];

echo"
	function Borrar".@$ID."(){
		var borrar = document.getElementById('$ID');
		var ID_Publicacion = '".$ID."';
        var parametros = {
                'ID' : ID_Publicacion
            }
		$.ajax({
            url: 'autores/borrar.php',
            type: 'POST',
            data: parametros,
            success: function () {
            	borrar.style.display = 'none';
            },
            error: function(){
            	           
            }  
        });
	}
    function  Actualizar".$ID."() {
      		guardando.style.display = 'inherit';
            guardado.style.display = 'none';
            error.style.display = 'none';
            var formData = new FormData(document.getElementById('formuploadajaxadd".$ID."'));
            formData.append('dato', 'valor');
            //formData.append(f.attr('name'), $(this)[0].files[0]);
            $.ajax({
                url: 'autores/actualizar.php',
                type: 'post',
                dataType: 'html',
                data: formData,
                cache: false,
                contentType: false,
         processData: false
            })
                .done(function(res){
                    console.log(res);
                if (res==1) {
                    alert('Todos los campos son requeridos por favor Verifique he intente nuevamente');
                }
            var busqueda = document.getElementById('busqueda').value;
        var parametrosbuscar = {
            busqueda : busqueda
        };
            $.ajax({
            url: 'autores/consulta.php',
            type: 'GET',
            data: parametrosbuscar,
            success: function (data) {
                if (data!=0) {
                document.getElementById('tbody').innerHTML = data;
            }
            else{
                document.getElementById('tbody').innerHTML = 'No se encontraron resultados';
            }       
            },
            error: function(){
                document.getElementById('tbody').innerHTML = 'Error';
            }});
            guardando.style.display = 'none';
                guardado.style.display = 'inherit';
                error.style.display = 'none';
                setTimeout(function(){ 
                guardado.style.display = 'none'; 
            },5000);
                });
        }";
  }

  echo "
  function  Guardar() 
      {	
        guardando.style.display = 'inherit';
            guardado.style.display = 'none';
            error.style.display = 'none';
            var formData = new FormData(document.getElementById('formuploadajaxadd'));
            formData.append('dato', 'valor');
            //formData.append(f.attr('name'), $(this)[0].files[0]);
            $.ajax({
                url: 'autores/guardar.php',
                type: 'post',
                dataType: 'html',
                data: formData,
                cache: false,
                contentType: false,
         processData: false
            })
                .done(function(res){
                    console.log(res);
                if (res==1) {
                    alert('Todos los campos son requeridos por favor Verifique he intente nuevamente');
                }
            var busqueda = document.getElementById('busqueda').value;
        var parametrosbuscar = {
            busqueda : busqueda
        };
            $.ajax({
            url: 'autores/consulta.php',
            type: 'GET',
            data: parametrosbuscar,
            success: function (data) {
                if (data!=0) {
                document.getElementById('tbody').innerHTML = data;
            }
            else{
                document.getElementById('tbody').innerHTML = 'No se encontraron resultados';
            }       
            },
            error: function(){
                document.getElementById('tbody').innerHTML = 'Error';
            }});
            guardando.style.display = 'none';
                guardado.style.display = 'inherit';
                error.style.display = 'none';
                setTimeout(function(){ 
                guardado.style.display = 'none'; 
            },5000);
                });
        }
        function  busqueda() 
      { 
        var busqueda = document.getElementById('busqueda').value;
        var parametrosbuscar = {
            busqueda : busqueda
        };
        $.ajax({
            url: 'autores/consulta.php',
            type: 'GET',
            data: parametrosbuscar,
            success: function (data) {
                if (data!=0) {
                document.getElementById('tbody').innerHTML = data;
            }
            else{
                document.getElementById('tbody').innerHTML = 'No se encontraron resultados';
            }       
            },
            error: function(){
                document.getElementById('tbody').innerHTML = 'Error';
            }});
        }
</script>";
     ?>

     
</body>
</html>


git add consola/administradores.php
git add consola/administradores/actualizar.php
git add consola/administradores/borrar.php
git add consola/administradores/consulta.php
git add consola/administradores/guardar.php
git add consola/administradores/validarmail.php
