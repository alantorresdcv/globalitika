<!DOCTYPE html>
<html lang="en">
<head>
<?php include('head.php');?>
</head>

<body class="fix-header fix-sidebar card-no-border">
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>

    <div id="main-wrapper">

        <header class="topbar">
            <?php include('menu-top.php');?>
        </header>

        <aside class="left-sidebar">
            <?php include('menu-lateral.php');?>
        </aside>

        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Reportes</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Reportes</li>
                        </ol>
                    </div>
                    <div class="col-md-7 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end">
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                <div class="chart-text m-r-10">
                                    <h6 class="m-b-0"><small>THIS MONTH</small></h6>
                                    <h4 class="m-t-0 text-info">$58,356</h4></div>
                                <div class="spark-chart">
                                    <div id="monthchart"></div>
                                </div>
                            </div>
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                <div class="chart-text m-r-10">
                                    <h6 class="m-b-0"><small>LAST MONTH</small></h6>
                                    <h4 class="m-t-0 text-primary">$48,356</h4></div>
                                <div class="spark-chart">
                                    <div id="lastmonthchart"></div>
                                </div>
                            </div>
                            <div class="">
                                <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-12">
                        <div class="dt-buttons">
                            <a class="dt-button buttons-pdf buttons-html5" tabindex="0" aria-controls="example23" href="usuarios-interna.php"><span>Regresar</span></a>
                        </div>

                        <div class="card">
                            <div class="card-body">

                              <div class="row m-t-40">
                                    <!-- Column -->
                                    <div class="col-md-6 col-lg-6 col-xlg-6">
                                        <img src="assets/images/propiedades/venue-01.jpg" alt="Venue">
                                    </div>
                                    <!-- Column -->
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="form-group validate">
                                            <h5>Detalle de la propiedad <span class="text-danger">*</span></h5>
                                            <fieldset class="controls">
                                                <label class="custom-control custom-radio">
                                                <input type="radio" value="1" name="styled_radio" required="" id="styled_radio1" class="custom-control-input" aria-invalid="false"><span class="custom-control-label">Bloquear</span> </label>
                                            <div class="help-block"></div></fieldset>
                                            <fieldset>
                                                <label class="custom-control custom-radio">
                                                <input type="radio" value="2" name="styled_radio" id="styled_radio2" class="custom-control-input" aria-invalid="false"><span class="custom-control-label">Reactivar</span> </label>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <!-- Column -->
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="form-group">
                                            <h5>Propiedad Destacada <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <select name="select" id="select" required="" class="form-control" aria-invalid="false">
                                                    <option value="1">Categoria</option>
                                                    <option value="2">Categoria</option>
                                                    <option value="3">Categoria</option>
                                                    <option value="4">Categoria</option>
                                                    <option value="5">Categoria</option>
                                                </select>
                                            <div class="help-block"></div></div>
                                        </div>
                                    </div>
                              </div>

                              <div class="col-12 col-lg-12">
                                  <h2 class="font-weight-norma text-uppercase titulos"><b>ST. Regis Hotel</b></h2>
                                  <div class="margin-bottom-30">
                                    <p>Reforma, Ciudad de México.</p>
                                    <h4 class="iconos-venue"><b><img src="assets/images/iconos/personas.png" alt="Amenidades del Venue" class="iconos-venue" style="width:2%;">50-250 personas</b></h4>
                                    <h4 class="iconos-venue"><b><img src="assets/images/iconos/presupuesto.png" alt="Amenidades del Venue" class="iconos-venue" style="width:2%;">$100,000 - $200,000</b></h4>
                                  </div><br>
                              
                                <h6 class="heading-uppercase no-margin"><b>Descripción</b></h6>
                                El hotel más importante de México. Contamos con 3 salones distintos para eventos privados. Somos un venue de calidad, tradición y vanguardia.<br>
                                Ubicado en la avenida más importante de la capital del país, tener un evento en nuestras instalaciones es estar en el corazón de lo más bello de esta ciudad. 
                                Ofrecemos distintos precios, paquetes y opciones para que celebres con nosotros los eventos más importantes de tu vida.<br>
                                Con más de 50 años de experiencia somos el primer destino del turismo de primer nivel y nuestros servicios son de la más alta calidad.
                              </div>

                              <!-- Contact Info section -->
                              <div class="col-12 col-lg-12" style="padding:  50px 0px;">
                                  <div class="row margin-bottom-20 iconos-venue">
                                    <!-- Icon text box 1 -->
                                    <div class="col-12 col-sm-6 col-md-3 icon-3xl iconos-venue text-center">
                                      <img src="assets/images/iconos/personas.png" alt="Amenidades del Venue" class="iconos-venue" style="width:10%;">
                                      <h6 class="heading-uppercase no-margin"><b>PARA INVITADOS</b></h6>
                                    </div>
                                    <!-- Icon text box 2 -->
                                    <div class="col-12 col-sm-6 col-md-3 icon-3xl text-center">
                                      <img src="assets/images/iconos/estacionamiento.png" alt="Amenidades del Venue" class="iconos-venue" style="width:10%;">
                                      <h6 class="heading-uppercase no-margin"><b>Estacionamiento</b></h6>
                                    </div>
                                    <!-- Icon text box 3 -->
                                    <div class="col-12 col-sm-6 col-md-3 icon-3xl text-center">
                                      <img src="assets/images/iconos/capilla.png" alt="Amenidades del Venue" class="iconos-venue" style="width:10%;">
                                      <h6 class="heading-uppercase no-margin"><b>Capilla</b></h6>
                                    </div>
                                    <!-- Icon text box 4 -->
                                    <div class="col-12 col-sm-6 col-md-3 icon-3xl text-center">
                                      <img src="assets/images/iconos/catering.png" alt="Amenidades del Venue" class="iconos-venue" style="width:10%;">
                                      <h6 class="heading-uppercase no-margin"><b>Catering</b></h6>
                                    </div>
                                  </div><!-- end row -->

                                  <div class="row">
                                    <!-- Icon text box 1 -->
                                    <div class="col-12 col-sm-6 col-md-3 icon-3xl text-center">
                                      <img src="assets/images/iconos/planta.png" alt="Amenidades del Venue" class="iconos-venue" style="width:10%;">
                                      <h6 class="heading-uppercase no-margin"><b>Áreas verdes</b></h6>
                                    </div>
                                    <!-- Icon text box 2 -->
                                    <div class="col-12 col-sm-6 col-md-3 icon-3xl text-center">
                                      <img src="assets/images/iconos/bar.png" alt="Amenidades del Venue" class="iconos-venue" style="width:10%;">
                                      <h6 class="heading-uppercase no-margin"><b>Servicio de Bar</b></h6>
                                    </div>
                                  </div><!-- end row -->
                              </div>
                              <!-- end Contact Info section -->

                              <div class="col-12 col-lg-12">
                                  <div class="margin-bottom-30">
                                  <p><b>Reforma 439, Colonia Centro, Ciudad de México.</b></p>
                                  </div>
                                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15050.342250269225!2d-99.16839093237355!3d19.430304625878488!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1ff4be2f41ead%3A0xe7179355336f6a97!2sSt+Regis!5e0!3m2!1ses-419!2smx!4v1540947814818" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                              </div>

                              <div class="col-sm-12">
                                  <button class="btn btn-success">Guardar cambios</button>
                              </div>

                          </div>

                        </div>
                    </div>
                </div>
                <!-- Row -->
            </div>

            <?php include('footer.php');?>

        </div>
    </div>
    <?php include('librerias.php');?>
</body>
</html>