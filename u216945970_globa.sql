-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 05-02-2019 a las 19:17:28
-- Versión del servidor: 10.2.17-MariaDB
-- Versión de PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `u216945970_globa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administradores`
--

CREATE TABLE `administradores` (
  `ID` int(11) UNSIGNED NOT NULL,
  `Nombre` varchar(200) NOT NULL,
  `Apellido` varchar(200) NOT NULL,
  `AgregadoFecha` date NOT NULL,
  `Correo` varchar(200) NOT NULL,
  `Clave` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `administradores`
--

INSERT INTO `administradores` (`ID`, `Nombre`, `Apellido`, `AgregadoFecha`, `Correo`, `Clave`) VALUES
(21, 'wilmer', 'peres', '2018-12-30', 'jocsan455@gmail.com', '1234'),
(26, 'María', 'de Buen', '2019-01-08', 'maria@bluepixel.mx', ''),
(30, 'yuneisi', 'de mora', '2019-01-12', 'yuneisi4@gmail.com', '1234');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `autores`
--

CREATE TABLE `autores` (
  `ID_Autor` int(11) UNSIGNED NOT NULL,
  `Nombre_Autor` varchar(100) NOT NULL,
  `Ocupacion` varchar(2000) NOT NULL,
  `FotoAutor` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `autores`
--

INSERT INTO `autores` (`ID_Autor`, `Nombre_Autor`, `Ocupacion`, `FotoAutor`) VALUES
(1, 'kathryn e. clark', 'Ceo de telewormas, investigadora, escritora con sede en CDMX, Mexico', 'http://tuprogramador.com.ve/globalitika/consola/publicaciones/img/2708344630.25.jpg'),
(2, 'paulo coelho', 'Ceo de telewormas, investigadora, escritora con sede en CDMX, Mexico', 'http://tuprogramador.com.ve/globalitika/consola/publicaciones/img/2708343765.75.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `ID_Categoria` int(11) UNSIGNED NOT NULL,
  `Nombre_Categoria` varchar(100) NOT NULL,
  `Portada_Categoria` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`ID_Categoria`, `Nombre_Categoria`, `Portada_Categoria`) VALUES
(28, 'Arte', 'http://tuprogramador.com.ve/globalitika/consola/categorias/img/2243017304.7.jpg'),
(29, 'Cultura', 'http://tuprogramador.com.ve/globalitika/consola/categorias/img/2243017358.35.jpg'),
(30, 'Literatura', 'http://tuprogramador.com.ve/globalitika/consola/categorias/img/2244868376.15.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas`
--

CREATE TABLE `facturas` (
  `IDFactura` int(11) NOT NULL,
  `IDUsuario` int(11) NOT NULL,
  `Factura` varchar(200) NOT NULL,
  `MetodoPago` varchar(200) NOT NULL,
  `Fecha` varchar(200) NOT NULL,
  `EstatusFactura` varchar(200) NOT NULL,
  `Total` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `facturas`
--

INSERT INTO `facturas` (`IDFactura`, `IDUsuario`, `Factura`, `MetodoPago`, `Fecha`, `EstatusFactura`, `Total`) VALUES
(1, 1, 'http://tuprogramador.com.ve/globalitika/libros/11', '****9586', 'Sep 9, 2018', 'Pagado', 'MX$99,00'),
(2, 2, 'http://tuprogramador.com.ve/globalitika/libros/11', '****9586', 'Sep 9, 2018', 'Pagado', 'MX$99,00'),
(3, 1, 'http://tuprogramador.com.ve/globalitika/libros/11', '****9586', 'Sep 9, 2018', 'Pagado', 'MX$99,00'),
(4, 1, 'http://tuprogramador.com.ve/globalitika/libros/11', '****9586', 'Sep 9, 2018', 'Pagado', 'MX$99,00'),
(5, 3, 'http://tuprogramador.com.ve/globalitika/libros/11', '****9586', 'Sep 9, 2018', 'Pendiente', 'MX$99,00'),
(6, 7, '$Link/Cuenta/factura', '', 'Enero 19, 2019', 'Pagado', 'MX$99,00'),
(7, 8, '$Link/Cuenta/factura', '', 'Enero 19, 2019', 'Pagado', 'MX$99,00'),
(8, 8, '$Link/Cuenta/factura', 'mora@gmail.com', 'Enero 19, 2019', 'Pagado', 'MX$99,00'),
(9, 9, '$Link/Cuenta/factura', 'yuneisi4@gmail.com', 'Enero 19, 2019', 'Pagado', 'MX$99,00'),
(10, 0, '$Link/Cuenta/factura', '', 'Enero 19, 2019', 'Pendiente', 'MX$99,00'),
(11, 9, '$Link/Cuenta/factura', 'yuneisi4@gmail.com', 'Enero 19, 2019', 'Pendiente', 'MX$99,00'),
(12, 9, '$Link/Cuenta/factura', 'yuneisi4@gmail.com', 'Enero 19, 2019', 'Cancelada', 'MX$99,00'),
(13, 9, '$Link/Cuenta/factura', 'yuneisi4@gmail.com', 'Enero 19, 2019', 'Pagado', 'MX$99,00'),
(14, 9, '$Link/Cuenta/factura', 'yuneisi4@gmail.com', 'Enero 19, 2019', 'Cancelada', 'MX$99,00'),
(15, 9, '$Link/Cuenta/factura', 'yuneisi4@gmail.com', 'Enero 19, 2019', 'Pagado', 'MX$99,00'),
(16, 9, '$Link/Cuenta/factura', 'yuneisi4@gmail.com', 'Enero 19, 2019', 'Pagado', 'MX$99,00'),
(17, 9, '$Link/Cuenta/factura', 'yuneisi4@gmail.com', 'Enero 19, 2019', 'Pagado', 'MX$99,00'),
(18, 9, '$Link/Cuenta/factura', 'yuneisi4@gmail.com', 'Enero 19, 2019', 'Pagado', 'MX$99,00'),
(19, 9, '$Link/Cuenta/factura', 'yuneisi4@gmail.com', 'Enero 19, 2019', 'Pagado', 'MX$99,00'),
(20, 14, '$Link/Cuenta/factura', 'jocsan455@gmail.com', 'Enero 19, 2019', 'Pagado', 'MX$99,00'),
(21, 14, '$Link/Cuenta/factura', 'jocsan455@gmail.com', 'Enero 19, 2019', 'Pagado', 'MX$99,00'),
(22, 15, '$Link/Cuenta/factura', 'jocsan455@gmail.com', 'Enero 22, 2019', 'Pagado', 'MX$99,00'),
(23, 0, '$Link/Cuenta/factura', '', 'Enero 22, 2019', 'Pagado', 'MX$99,00'),
(24, 17, '$Link/Cuenta/factura', 'maria.duvon@gmail.com', 'Enero 22, 2019', 'Cancelada', 'MX$99,00'),
(25, 17, '$Link/Cuenta/factura', 'maria.duvon@gmail.com', 'Enero 22, 2019', 'Pagado', 'MX$99,00'),
(26, 18, '$Link/Cuenta/factura', 'pedriotws@gmail.com', 'Enero 30, 2019', 'Pagado', 'MX$99,00'),
(27, 18, '$Link/Cuenta/factura', 'pedriotws@gmail.com', 'Enero 30, 2019', 'Pagado', 'MX$99,00'),
(28, 18, '$Link/Cuenta/factura', 'yunleos@gmail.com', 'Enero 30, 2019', 'Pagado', 'MX$99,00'),
(29, 18, '$Link/Cuenta/factura', 'yunleos@gmail.com', 'Enero 30, 2019', 'Pagado', 'MX$99,00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libros`
--

CREATE TABLE `libros` (
  `ID` int(11) UNSIGNED NOT NULL,
  `Autor` varchar(50) NOT NULL,
  `Categoria` varchar(50) NOT NULL,
  `Tema` int(4) NOT NULL,
  `Fecha` datetime NOT NULL,
  `Titulo` varchar(100) NOT NULL,
  `Destacado` int(10) NOT NULL,
  `DescripcionCorta` longtext NOT NULL,
  `DescripcionLarga` longtext NOT NULL,
  `Resumen` varchar(215) NOT NULL,
  `Portada` varchar(200) NOT NULL,
  `Premium` int(1) NOT NULL,
  `Pdf` varchar(200) NOT NULL,
  `Estatus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `libros`
--

INSERT INTO `libros` (`ID`, `Autor`, `Categoria`, `Tema`, `Fecha`, `Titulo`, `Destacado`, `DescripcionCorta`, `DescripcionLarga`, `Resumen`, `Portada`, `Premium`, `Pdf`, `Estatus`) VALUES
(1, '1', '28', 4, '2018-10-28 13:02:00', 'Once minutos por hora', 1, '', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam commodo, augue quis tincidunt molestie, orci lectus porttitor enim, sed pretium magna elit vel lectus. Etiam convallis molestie augue quis scelerisque', 'http://tuprogramador.com.ve/globalitika/consola/publicaciones/img/2243040409.jpg', 1, 'http://tuprogramador.com.ve/globalitika/libros/Once-minutos.pdf', 1),
(241, '2', '28', 4, '2019-01-01 00:00:00', 'El alquimista', 0, 'El alquimista (O Alquimista, 1988) es un libro escrito por el brasileño Paulo Coelho que ha sido traducido a más de 63 lenguas y publicado en 150 países, y que ha vendido 65 millones de copias en todo el mundo (2012).1? El libro trata sobre los sueños y los medios que utilizamos para alcanzarlos, sobre el azar en nuestra vida y las señales que se p', 'El alquimista (O Alquimista, 1988) es un libro escrito por el brasileño Paulo Coelho que ha sido traducido a más de 63 lenguas y publicado en 150 países, y que ha vendido 65 millones de copias en todo el mundo (2012).1? El libro trata sobre los sueños y los medios que utilizamos para alcanzarlos, sobre el azar en nuestra vida y las señales que se presentan a lo largo de esta.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam commodo, augue quis tincidunt molestie, orci lectus porttitor enim, sed pretium magna elit vel lectus. Etiam convallis molestie augue quis scelerisque', 'http://tuprogramador.com.ve/globalitika/consola/publicaciones/img/2243040426.4.jpg', 0, 'http://tuprogramador.com.ve/globalitika/libros/Once-minutos.pdf', 1),
(242, '2', '28', 4, '2019-01-01 00:00:00', 'Once minutos', 1, 'Once minutos es un libro del escritor brasileno Paulo Coelho, publicado en el ano 2003. La novela relata la vida de Maria, una joven de una aldea remota de Brasil. \"Habia una vez una prostituta llamada Maria...\" Como un cuento de hadas contado para adultos, asi comienza la novela que conmovio al mundo. ', 'Once minutos es un libro del escritor brasileno Paulo Coelho, publicado en el ano 2003. La novela relata la vida de Maria, una joven de una aldea remota de Brasil. \"Habia una vez una prostituta llamada Maria...\" Como un cuento de hadas contado para adultos, asi comienza la novela que conmovio al mundo. A partir de desengaños amorosos en una etapa pronta de adolescentes, María desarrolla un odio por el amor y decide buscar fortuna en Suiza. Allí descubre que la realidad es mucho más difícil de lo que esperaba. Después de trabajar en un club nocturno como bailarina de samba durante un tiempo, se da cuenta de que esto no es lo que quiere. Durante de una acalorada discusión con su gerente, ella sale corriendo y comienza a buscar una carrera como modelo. Realiza una larga búsqueda de trabajo sin resultado, y como ella comienza a quedarse sin dinero, se vende, por 1.000 francos para \"una noche\" con un hombre árabe.  Encantada con el dinero fácil y después de comprometerse con su alma, ella aterriza en un burdel de la rue de Berne, el corazón de la luz roja de Ginebra del distrito. Allí se hace amiga de Nyah que le da consejos sobre su \"nueva profesión\", y Milán, el dueño del burdel, que le enseña trucos del oficio. María comienza a trabajar con su cuerpo y la mente cerrando todas las puertas para el amor y manteniendo su corazón abierto sólo para su diario. Su carrera se convierte un éxito y famosos y sus colegas comienzan a envidiarla.  Pasan los meses y María se convierte en una prostituta profesional preparada, que no sólo relaja la mente de sus clientes, sino también calma su alma al hablar con ellos acerca de sus problemas. Su mundo gira al revés cuando conoce a Ralf Hart, un pintor. María ahora está dividida entre sus fantasías sexuales y verdadero amor por Ralf. Eventualmente, ella decide que es hora de irse de Ginebra con su memoria de Ralf, porque se da cuenta que son de mundos distintos. Pero antes de irse, ella decide volver a encender el muerto fuego sexual en', '', 'http://tuprogramador.com.ve/globalitika/consola/publicaciones/img/2243040629.4.jpg', 1, 'http://tuprogramador.com.ve/globalitika/libros/Once-minutos.pdf', 1),
(243, '1', '28', 4, '2019-01-01 00:00:00', 'Once minutos', 1, 'Once minutos es un libro del escritor brasileno Paulo Coelho, publicado en el ano 2003. La novela relata la vida de Maria, una joven de una aldea remota de Brasil. \"Habia una vez una prostituta llamada Maria...\" Como un cuento de hadas contado para adultos, asi comienza la novela que conmovio al mundo. ', 'Once minutos es un libro del escritor brasileno Paulo Coelho, publicado en el ano 2003. La novela relata la vida de Maria, una joven de una aldea remota de Brasil. \"Habia una vez una prostituta llamada Maria...\" Como un cuento de hadas contado para adultos, asi comienza la novela que conmovio al mundo. A partir de desengaños amorosos en una etapa pronta de adolescentes, María desarrolla un odio por el amor y decide buscar fortuna en Suiza. Allí descubre que la realidad es mucho más difícil de lo que esperaba. Después de trabajar en un club nocturno como bailarina de samba durante un tiempo, se da cuenta de que esto no es lo que quiere. Durante de una acalorada discusión con su gerente, ella sale corriendo y comienza a buscar una carrera como modelo. Realiza una larga búsqueda de trabajo sin resultado, y como ella comienza a quedarse sin dinero, se vende, por 1.000 francos para \"una noche\" con un hombre árabe.  Encantada con el dinero fácil y después de comprometerse con su alma, ella aterriza en un burdel de la rue de Berne, el corazón de la luz roja de Ginebra del distrito. Allí se hace amiga de Nyah que le da consejos sobre su \"nueva profesión\", y Milán, el dueño del burdel, que le enseña trucos del oficio. María comienza a trabajar con su cuerpo y la mente cerrando todas las puertas para el amor y manteniendo su corazón abierto sólo para su diario. Su carrera se convierte un éxito y famosos y sus colegas comienzan a envidiarla.  Pasan los meses y María se convierte en una prostituta profesional preparada, que no sólo relaja la mente de sus clientes, sino también calma su alma al hablar con ellos acerca de sus problemas. Su mundo gira al revés cuando conoce a Ralf Hart, un pintor. María ahora está dividida entre sus fantasías sexuales y verdadero amor por Ralf. Eventualmente, ella decide que es hora de irse de Ginebra con su memoria de Ralf, porque se da cuenta que son de mundos distintos. Pero antes de irse, ella decide volver a encender el muerto fuego sexual en', '', 'http://tuprogramador.com.ve/globalitika/consola/publicaciones/img/2243040643.9.jpg', 1, 'http://tuprogramador.com.ve/globalitika/libros/Once-minutos.pdf', 1),
(244, '2', '28', 4, '2019-01-01 00:00:00', 'El alquimista', 0, 'El alquimista (O Alquimista, 1988) es un libro escrito por el brasileño Paulo Coelho que ha sido traducido a más de 63 lenguas y publicado en 150 países, y que ha vendido 65 millones de copias en todo el mundo (2012).1? El libro trata sobre los sueños y los medios que utilizamos para alcanzarlos, sobre el azar en nuestra vida y las señales que se p', 'El alquimista (O Alquimista, 1988) es un libro escrito por el brasileño Paulo Coelho que ha sido traducido a más de 63 lenguas y publicado en 150 países, y que ha vendido 65 millones de copias en todo el mundo (2012).1? El libro trata sobre los sueños y los medios que utilizamos para alcanzarlos, sobre el azar en nuestra vida y las señales que se presentan a lo largo de esta.', '', 'http://tuprogramador.com.ve/globalitika/consola/publicaciones/img/2243040668.55.jpg', 0, 'http://tuprogramador.com.ve/globalitika/libros/Once-minutos.pdf', 1),
(245, '2', '28', 4, '2018-10-28 02:12:00', 'Once minutos1', 1, 'Once minutos es un libro del escritor brasileno Paulo Coelho, publicado en el ano 2003. La novela relata la vida de Maria, una joven de una aldea remota de Brasil. \"Habia una vez una prostituta llamada Maria...\" Como un cuento de hadas contado para adultos, asi comienza la novela que conmovio al mundo. ', 'Once minutos es un libro del escritor brasileno Paulo Coelho, publicado en el ano 2003. La novela relata la vida de Maria, una joven de una aldea remota de Brasil. \"Habia una vez una prostituta llamada Maria...\" Como un cuento de hadas contado para adultos, asi comienza la novela que conmovio al mundo. A partir de desengaños amorosos en una etapa pronta de adolescentes, María desarrolla un odio por el amor y decide buscar fortuna en Suiza. Allí descubre que la realidad es mucho más difícil de lo que esperaba. Después de trabajar en un club nocturno como bailarina de samba durante un tiempo, se da cuenta de que esto no es lo que quiere. Durante de una acalorada discusión con su gerente, ella sale corriendo y comienza a buscar una carrera como modelo. Realiza una larga búsqueda de trabajo sin resultado, y como ella comienza a quedarse sin dinero, se vende, por 1.000 francos para \"una noche\" con un hombre árabe.  Encantada con el dinero fácil y después de comprometerse con su alma, ella aterriza en un burdel de la rue de Berne, el corazón de la luz roja de Ginebra del distrito. Allí se hace amiga de Nyah que le da consejos sobre su \"nueva profesión\", y Milán, el dueño del burdel, que le enseña trucos del oficio. María comienza a trabajar con su cuerpo y la mente cerrando todas las puertas para el amor y manteniendo su corazón abierto sólo para su diario. Su carrera se convierte un éxito y famosos y sus colegas comienzan a envidiarla.  Pasan los meses y María se convierte en una prostituta profesional preparada, que no sólo relaja la mente de sus clientes, sino también calma su alma al hablar con ellos acerca de sus problemas. Su mundo gira al revés cuando conoce a Ralf Hart, un pintor. María ahora está dividida entre sus fantasías sexuales y verdadero amor por Ralf. Eventualmente, ella decide que es hora de irse de Ginebra con su memoria de Ralf, porque se da cuenta que son de mundos distintos. Pero antes de irse, ella decide volver a encender el muerto fuego sexual en', '', 'http://tuprogramador.com.ve/globalitika/consola/publicaciones/img/2243040674.35.jpg', 1, 'http://tuprogramador.com.ve/globalitika/libros/Once-minutos.pdf', 1),
(254, '2', '28', 4, '0000-00-00 00:00:00', 'a las 6', 1, '', '', '', 'http://tuprogramador.com.ve/globalitika/consola/publicaciones/img/2244062366.1.jpg', 1, '', 1),
(267, '1', '28', 4, '2019-01-01 00:00:00', 'fsdf', 1, '<h1>Hello world!</h1>\r\n\r\n<p>I&#39;m an instance of&nbsp;<a href=\"https://ckeditor.com/\">CKEditor</a>.</p>\r\n', '<h1>Hello world!</h1>\r\n\r\n<p>I&#39;m an instance of&nbsp;<a href=\"https://ckeditor.com/\">CKEditor</a>.</p>\r\n', '', 'http://tuprogramador.com.ve/globalitika/consola/publicaciones/img/2245586300.15.jpg', 1, '', 1),
(272, '1', '29', 2, '2019-01-29 00:00:00', '¡INHUMANO! Niños presos en Venezuela', 1, '<p>Decenas de ni&ntilde;os que salieron a sus colegios el 23 de enero de 2019 en Venezuela no han regresado a sus hogares. Se encuentran maltratados, sucios y encarcelados en calabozos militares por &oacute;rdenes de jueces designados a dedo por el hombre fuerte Nicol&aacute;s Maduro.</p>\r\n\r\n<p><strong>Mari&aacute;ngela Vel&aacute;squez /<a href=\"https://es-us.noticias.yahoo.com/ninos-presos-por-protestar-el-lado-mas-oscuro-de-la-represion-de-maduro-164915654.html?soc_src=community&amp;soc_trk=fb&amp;fbclid=IwAR2sWQ8Iwa_aoR-O8g84VhI-416CCrV1NYuoa0nQv_mQ7rmjrEIA-zyvEaE\">&nbsp;yahoo.com</a></strong></p>\r\n\r\n<p>La agitaci&oacute;n pol&iacute;tica que ya cuenta con 29 muertos y casi 800 detenidos comenz&oacute; el 21 de enero, dos d&iacute;as antes de la protesta nacional convocada por el presidente interino Juan Guaid&oacute; para solicitar el cese de la usurpaci&oacute;n del poder de Maduro. Uno de los primeros fallecidos fue un adolescente. Cleiner Jos&eacute; Romero, de 17 a&ntilde;os, fue asesinado de un tiro mientras manifestaba el martes 22 de enero en Ciudad Bol&iacute;var.</p>\r\n\r\n<p><strong>Observatorio de Conflictos</strong><br />\r\n<strong>@OVCSocial</strong><br />\r\n@_Provea y @OVCSocial ponen a su disposici&oacute;n el mapa con ubicaci&oacute;n y rese&ntilde;a de las personas asesinadas en manifestaciones en 2019 #Venezuela #DDHH &ndash; https://www.google.com/maps/d/viewer?mid=1rE6nmQsWcXhcvsNXPBthPM8Pik2jyABl&amp;usp=sharing &hellip;</p>\r\n\r\n<p>Mapa de geo-referenciaci&oacute;n de las personas asesinadas durante el reci&eacute;n iniciado ciclo de protestas en Venezuela 2019.</p>\r\n\r\n<p>Para muchos de los ni&ntilde;os que se encuentran detenidos se trataba de un d&iacute;a de clases como cualquier otro mi&eacute;rcoles y quedaron atrapados en las revueltas. Otros asistieron a las manifestaciones acompa&ntilde;ados por sus padres.</p>\r\n\r\n<p>La Organizaci&oacute;n No Gubernamental Foro Penal denunci&oacute; que a la mayor&iacute;a de los ni&ntilde;os detenidos se les negaron medidas cautelares para su liberaci&oacute;n y que permanecen tras las rejas, acusados de terrorismo en distintos estados del pa&iacute;s.</p>\r\n\r\n<p><strong>Angela E Vera Lefeld</strong><br />\r\n<strong>@uguelo</strong><br />\r\nConmovedoras misivas de nuestros menores de edad secuestrados en mazmorras de la tiran&iacute;a: piden en grito de auxilio con letras a sus padres que los liberen del infierno donde los tienen.<br />\r\n&iquest;Hasta qu&eacute; d&iacute;a debe Venezuela soportar el horror con dolor?</p>\r\n\r\n<p><img alt=\"¡INHUMANO! Niños presos en Venezuela, el lado más oscuro de la represión\" height=\"320\" src=\"https://dxj1e0bbbefdtsyig.woldrssl.net/wp-content/uploads/2019/01/15-600x320.png\" title=\"¡INHUMANO! Niños presos en Venezuela, el lado más oscuro de la represión\" width=\"600\" /></p>\r\n\r\n<p>Al menos 11 ni&ntilde;os se encuentran detenidos en Yaracuy, una f&eacute;rtil regi&oacute;n agr&iacute;cola ubicada en el centro occidente del pa&iacute;s, c&eacute;lebre por ser la cuna de Mar&iacute;a Lionza, una diosa ind&iacute;gena venerada por miles de venezolanos.</p>\r\n\r\n<p>El apoyo a Guaid&oacute; en las zonas populares tom&oacute; por sorpresa a las autoridades regionales pertenecientes al gubernamental Partido Unido Socialista de Venezuela (PSUV). En Yaracuy, los partidos que apoyan la revoluci&oacute;n bolivariana hab&iacute;an ganado todas las elecciones a la Gobernaci&oacute;n desde 2004.</p>\r\n', '<h1>&iexcl;INHUMANO! Ni&ntilde;os presos en Venezuela, el lado m&aacute;s oscuro de la represi&oacute;n</h1>\r\n\r\n<p><em><strong>DolarToday / Jan 28, 2019 @ 5:30 pm</strong></em></p>\r\n\r\n<p>FacebookTwitterGmailCopy LinkM&aacute;s...855</p>\r\n\r\n<p><img alt=\"¡INHUMANO! Niños presos en Venezuela, el lado más oscuro de la represión\" height=\"320\" src=\"https://dxj1e0bbbefdtsyig.woldrssl.net/wp-content/uploads/2019/01/15-600x320.png\" title=\"¡INHUMANO! Niños presos en Venezuela, el lado más oscuro de la represión\" width=\"600\" /></p>\r\n\r\n<p><strong>Decenas de ni&ntilde;os que salieron a sus colegios el 23 de enero de 2019 en Venezuela no han regresado a sus hogares. Se encuentran maltratados, sucios y encarcelados en calabozos militares por &oacute;rdenes de jueces designados a dedo por el hombre fuerte Nicol&aacute;s Maduro.</strong></p>\r\n\r\n<p><strong>Mari&aacute;ngela Vel&aacute;squez /<a href=\"https://es-us.noticias.yahoo.com/ninos-presos-por-protestar-el-lado-mas-oscuro-de-la-represion-de-maduro-164915654.html?soc_src=community&amp;soc_trk=fb&amp;fbclid=IwAR2sWQ8Iwa_aoR-O8g84VhI-416CCrV1NYuoa0nQv_mQ7rmjrEIA-zyvEaE\">&nbsp;yahoo.com</a></strong></p>\r\n\r\n<p>La agitaci&oacute;n pol&iacute;tica que ya cuenta con 29 muertos y casi 800 detenidos comenz&oacute; el 21 de enero, dos d&iacute;as antes de la protesta nacional convocada por el presidente interino Juan Guaid&oacute; para solicitar el cese de la usurpaci&oacute;n del poder de Maduro. Uno de los primeros fallecidos fue un adolescente. Cleiner Jos&eacute; Romero, de 17 a&ntilde;os, fue asesinado de un tiro mientras manifestaba el martes 22 de enero en Ciudad Bol&iacute;var.</p>\r\n\r\n<p><strong>Observatorio de Conflictos</strong><br />\r\n<strong>@OVCSocial</strong><br />\r\n@_Provea y @OVCSocial ponen a su disposici&oacute;n el mapa con ubicaci&oacute;n y rese&ntilde;a de las personas asesinadas en manifestaciones en 2019 #Venezuela #DDHH &ndash; https://www.google.com/maps/d/viewer?mid=1rE6nmQsWcXhcvsNXPBthPM8Pik2jyABl&amp;usp=sharing &hellip;</p>\r\n\r\n<p>Mapa de geo-referenciaci&oacute;n de las personas asesinadas durante el reci&eacute;n iniciado ciclo de protestas en Venezuela 2019.</p>\r\n\r\n<p>Para muchos de los ni&ntilde;os que se encuentran detenidos se trataba de un d&iacute;a de clases como cualquier otro mi&eacute;rcoles y quedaron atrapados en las revueltas. Otros asistieron a las manifestaciones acompa&ntilde;ados por sus padres.</p>\r\n\r\n<p>La Organizaci&oacute;n No Gubernamental Foro Penal denunci&oacute; que a la mayor&iacute;a de los ni&ntilde;os detenidos se les negaron medidas cautelares para su liberaci&oacute;n y que permanecen tras las rejas, acusados de terrorismo en distintos estados del pa&iacute;s.</p>\r\n\r\n<p><strong>Angela E Vera Lefeld</strong><br />\r\n<strong>@uguelo</strong><br />\r\nConmovedoras misivas de nuestros menores de edad secuestrados en mazmorras de la tiran&iacute;a: piden en grito de auxilio con letras a sus padres que los liberen del infierno donde los tienen.<br />\r\n&iquest;Hasta qu&eacute; d&iacute;a debe Venezuela soportar el horror con dolor?</p>\r\n\r\n<p>Al menos 11 ni&ntilde;os se encuentran detenidos en Yaracuy, una f&eacute;rtil regi&oacute;n agr&iacute;cola ubicada en el centro occidente del pa&iacute;s, c&eacute;lebre por ser la cuna de Mar&iacute;a Lionza, una diosa ind&iacute;gena venerada por miles de venezolanos.</p>\r\n\r\n<p>El apoyo a Guaid&oacute; en las zonas populares tom&oacute; por sorpresa a las autoridades regionales pertenecientes al gubernamental Partido Unido Socialista de Venezuela (PSUV). En Yaracuy, los partidos que apoyan la revoluci&oacute;n bolivariana hab&iacute;an ganado todas las elecciones a la Gobernaci&oacute;n desde 2004.</p>\r\n\r\n<p><strong>carmen diaz</strong><br />\r\n<strong>@KrmnDiaz19</strong><br />\r\n<strong>Represi&oacute;n en San Felipe, Yaracuy</strong></p>\r\n\r\n<p>El Foro Penal dijo que de los 34 menores detenidos el 23 de enero en Yaracuy, 11 quedaron privados de libertad por &oacute;rdenes de la juez de control Ediluz Guedez sin &ldquo;tener ni una sola prueba&rdquo;. Los chicos, que tienen entre 11 y 16 a&ntilde;os, fueron imputados por terrorismo, obstrucci&oacute;n de justicia, agavillamiento y asociaci&oacute;n para delinquir, pese a la solicitud de libertad realizada por el Ministerio P&uacute;blico y la defensa.</p>\r\n\r\n<p><strong>Julio C&eacute;sar Rivas</strong><br />\r\n<strong>@JULIOCESARRIVAS</strong><br />\r\n11 ni&ntilde;os presentados en tribunales en Yaracuy algunos en uniformes escolares. Golpeados, heridos y sucios.</p>\r\n\r\n<p>Juez dicta privativa, agravando petici&oacute;n de la fiscal&iacute;a que era medida cautelar.</p>\r\n\r\n<p>Una de las ni&ntilde;as se tira al suelo en llanto &ldquo;&iquest;Porque me destrozan la vida?&rdquo; Pregunto</p>\r\n\r\n<p>Adem&aacute;s de los j&oacute;venes detenidos, Yaracuy tambi&eacute;n lloraba un muerto. Daniel V&eacute;liz, de 18 a&ntilde;os, muri&oacute; de un tiro en la Plaza Sucre de San Felipe, la capital del estado.</p>\r\n\r\n<p>En Amazonas, un estado selv&aacute;tico en el sur del pa&iacute;s, tambi&eacute;n hay ni&ntilde;os presos desde que comenzaron las protestas. Cinco de los 23 detenidos acusados de terrorismo en la ciudad de Puerto Ayacucho son menores de edad. Uno de ellos es una ni&ntilde;a ind&iacute;gena de 14 a&ntilde;os.</p>\r\n\r\n<p><strong>A.C. KAPE KAPE</strong><br />\r\n<strong>@ackapekape</strong><br />\r\n#KapeKape exige el cumplimiento de la Lopna para la proteccion de los derechos de la joven indigena de 14 a&ntilde;os detenida en Amazonas en actuacion de los cuerpos de seguridad ante las protestas de los ultimos dias</p>\r\n\r\n<p>Otra detenci&oacute;n que caus&oacute; consternaci&oacute;n entre los defensores de derechos humanos fue la de Luis Jos&eacute; Flores, un joven de 16 a&ntilde;os que padece c&aacute;ncer y se encuentra en tratamiento.</p>\r\n\r\n<p>La movilizaci&oacute;n de la comunidad de Tinaquillo, en el estado Cojedes, logr&oacute; la liberaci&oacute;n del joven, que inicialmente fue imputado por el juez Jos&eacute; Adonay. Sin embargo, activistas locales han denunciado que a&uacute;n permanecen bajo las rejas al menos 12 menores de edad, entre ellos un muchacho con Sindrome de Down.</p>\r\n', '<p>Decenas de ni&ntilde;os que salieron a sus colegios el 23 de enero de 2019 en Venezuela no han regresado a sus hogares. Se encuentran maltratados, sucios y encarcelados en calabozos militares por &oacute;rdenes d', 'http://tuprogramador.com.ve/globalitika/consola/publicaciones/img/2245700828.4.png', 1, '0', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `newsletter`
--

CREATE TABLE `newsletter` (
  `ID` int(11) UNSIGNED NOT NULL,
  `Correo` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `newsletter`
--

INSERT INTO `newsletter` (`ID`, `Correo`) VALUES
(19, 'jocsan455@gmail.com'),
(20, 'wilmerf'),
(21, 'dfw'),
(22, 'sdfsd@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temas`
--

CREATE TABLE `temas` (
  `ID_Tema` int(11) UNSIGNED NOT NULL,
  `Nombre_Tema` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `IDCategoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `temas`
--

INSERT INTO `temas` (`ID_Tema`, `Nombre_Tema`, `IDCategoria`) VALUES
(2, 'grande', 29),
(4, 'arte', 28),
(5, 'telefono', 29);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transacciones`
--

CREATE TABLE `transacciones` (
  `ID` int(11) UNSIGNED NOT NULL,
  `Usuario` varchar(200) NOT NULL,
  `Correo` varchar(100) NOT NULL,
  `Fecha` date NOT NULL,
  `Membresia` varchar(50) NOT NULL,
  `Periodo` varchar(50) NOT NULL,
  `FechaVencimiento` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `transacciones`
--

INSERT INTO `transacciones` (`ID`, `Usuario`, `Correo`, `Fecha`, `Membresia`, `Periodo`, `FechaVencimiento`) VALUES
(9, 'wilmer', 'MetodoPago', '2019-01-20', 'estudiante', 'anual', '0000-00-00'),
(10, 'wilmer', 'MetodoPago', '2019-01-22', 'estudiante', 'anual', '0000-00-00'),
(11, '', 'MetodoPago', '2019-01-22', 'estudiante', 'anual', '0000-00-00'),
(12, 'María', 'MetodoPago', '2019-01-22', 'estudiante', 'anual', '0000-00-00'),
(13, 'pedrito', 'MetodoPago', '2019-01-30', 'estudiante', 'anual', '0000-00-00'),
(14, 'pedrito', 'MetodoPago', '2019-01-30', 'estudiante', 'anual', '0000-00-00'),
(15, 'pedrito', 'MetodoPago', '2019-01-30', 'estudiante', 'anual', '0000-00-00'),
(16, 'pedrito', 'MetodoPago', '2019-01-30', 'estudiante', 'anual', '0000-00-00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `ID` int(11) UNSIGNED NOT NULL,
  `Nombre` varchar(200) NOT NULL,
  `Apellido` varchar(200) NOT NULL,
  `Correo` varchar(200) NOT NULL,
  `Paypal` varchar(200) NOT NULL,
  `Clave` varchar(200) NOT NULL,
  `Foto` varchar(200) NOT NULL,
  `Plan` varchar(200) NOT NULL,
  `Ciclo` varchar(200) NOT NULL,
  `Estatus` varchar(200) NOT NULL,
  `UltimoPago` varchar(255) NOT NULL,
  `SiguientePago` varchar(255) NOT NULL,
  `FechaExpiracion` varchar(200) NOT NULL,
  `FechaExpiraciondate` date NOT NULL,
  `Acceso` varchar(100) NOT NULL,
  `Verificado` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`ID`, `Nombre`, `Apellido`, `Correo`, `Paypal`, `Clave`, `Foto`, `Plan`, `Ciclo`, `Estatus`, `UltimoPago`, `SiguientePago`, `FechaExpiracion`, `FechaExpiraciondate`, `Acceso`, `Verificado`) VALUES
(1, 'danielito', 'mora', 'd.nelson@nelsonsays.commimama', '', '1234', 'http://127.0.0.1/globalitika/usuarios/fotoperfil/colerico.jpg', 'estudiante', 'anual', 'pendiente', 'abril 25, 2018', 'abril 25, 2019', 'abril 25, 2019', '2019-01-21', 'Acceso a todo', 1),
(4, 'maria', 'de buen', 'maria@bluepixel.mx', '', 'Pixazul2017', 'http://tuprogramador.com.ve/globalitika/Register/img/922493419671940620.jpg', 'profesional', 'anual', 'activo', 'abril 25, 2018', 'abril 25, 2019', 'abril 25, 2019', '2019-01-22', 'Acceso a todo', 1),
(9, 'yuneisi', 'de mora', 'yuneisi4@gmail.com', '', '1234', 'http://tuprogramador.com.ve/globalitika/Register/img/409799102125926096.jpg', 'estudiante', 'anual', 'activo', 'Enero 19, 2019', 'Enero 19, 2020', 'Enero 19, 2020', '2020-01-19', 'Acceso a todo', 1),
(16, 'pedro', 'peres', 'pedritoe@gmail.com', '', '1234', 'http://tuprogramador.com.ve/globalitika/Register/img/212228956912230436.jpg', 'Free', 'ilimitado', 'activo', 'Ninguno', 'Ninguno', 'Ninguno', '0000-00-00', 'contenido gratuito', 1),
(17, 'María', 'De Buen', 'maria.duvon@gmail.com', '', 'Pixazul2017', 'http://tuprogramador.com.ve/globalitika/Register/img/467985439583355134.jpg', 'estudiante', 'anual', 'activo', 'Enero 22, 2019', 'Enero 22, 2020', 'Enero 22, 2020', '2020-01-22', 'Acceso a todo', 1),
(18, 'pedrito', 'infante', 'pedriotws@gmail.com', 'yunleos@gmail.com', 'dsfdf', 'http://tuprogramador.com.ve/globalitika/Register/img/1938424260477318240.jpg', 'Free', 'ilimitado', 'activo', 'Enero 30, 2019', 'Ninguno', 'Ninguno', '2020-01-30', 'contenido gratuito', 1),
(25, 'wilmer', 'mora', 'jocsan455@gmail.com', 'jocsan455@gmail.com', '1234', 'http://tuprogramador.com.ve/globalitika/Register/img/1877461776465553872.jpg', 'Free', 'ilimitado', 'activo', 'Ninguno', 'Ninguno', 'Ninguno', '0000-00-00', 'contenido gratuito', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administradores`
--
ALTER TABLE `administradores`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `autores`
--
ALTER TABLE `autores`
  ADD PRIMARY KEY (`ID_Autor`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`ID_Categoria`);

--
-- Indices de la tabla `facturas`
--
ALTER TABLE `facturas`
  ADD PRIMARY KEY (`IDFactura`);

--
-- Indices de la tabla `libros`
--
ALTER TABLE `libros`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID` (`ID`);

--
-- Indices de la tabla `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID` (`ID`);

--
-- Indices de la tabla `temas`
--
ALTER TABLE `temas`
  ADD KEY `ID` (`ID_Tema`);

--
-- Indices de la tabla `transacciones`
--
ALTER TABLE `transacciones`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID` (`ID`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administradores`
--
ALTER TABLE `administradores`
  MODIFY `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `autores`
--
ALTER TABLE `autores`
  MODIFY `ID_Autor` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `ID_Categoria` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `facturas`
--
ALTER TABLE `facturas`
  MODIFY `IDFactura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `libros`
--
ALTER TABLE `libros`
  MODIFY `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=273;

--
-- AUTO_INCREMENT de la tabla `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `temas`
--
ALTER TABLE `temas`
  MODIFY `ID_Tema` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `transacciones`
--
ALTER TABLE `transacciones`
  MODIFY `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
