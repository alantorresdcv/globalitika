<?php require('cuenta/consultas.php') ?>
<div class="fondoblanco">
	<div class="container">
				<div class="arriba" style="padding-top: 30px;">
					<div class="col-md-4 text-center float-left">
		      			<i class="iconosociales fab fa-facebook"></i>
		      			<i class="iconosociales fab fa-twitter"></i>
		      			<i class="iconosociales fab fa-linkedin"></i>
		      		</div>
		      		<div class="col-md-4 text-center float-left">
		      			<a href="<?php echo $Link; ?>">
								<img src="../assets/images/logo-globalitika.png" alt="Logo Globalitika">
						</a>
		      		</div>
					<div class="divbuscar2 hidden-phone text-right float-left">
							<form method="GET" action="<?php echo $Link; ?>/investigacion">
		      					<div class="text-right inline-block selectbuscar ocultarbuscar" style="width: 90%;">

			      				<select id="buscar" name="buscar" class="selectpicker col-md-12 center-block" data-show-subtext="true" data-live-search="true">
			      						<option selected style="color:transparent !important;">Buscar aqui</option>
								<?php

								$con = connect();
								$consulta = "SELECT * FROM l32ddiasfbafbiatreiasdfw";
								$resultado = mysqli_query($con , $consulta);
								$contador=0;

								while($misdatos = mysqli_fetch_assoc($resultado)){ $contador++;?>
								<option data-subtext="<?php echo $misdatos["Autor"]; ?>"><?php echo $misdatos["Titulo"]; ?></option>
								<?php }?>
								</select>
							</div>
							<div class="col-md-1 inline-block cursor">
									<button id="submit" type="submit" disabled><label onclick="buscar()" class="iconobuscar cursor"><i class="fa fa-search"></i></label></button>
								</div>
							</form>
		      		</div>
		      		<div class="divbuscar hidden-tablet hidden-desktop">
							<form method="GET" action="<?php echo $Link; ?>/investigacion">
		      					<div class="inline-block selectbuscar buscar movil" >

			      				<select id="buscar" name="Titulo" class="selectpicker col-md-12 center-block" data-show-subtext="true" data-live-search="true">
			      						<option selected style="color:transparent !important;">Buscar aqui</option>
								<?php

								$con = connect();
								$consulta = "SELECT * FROM l32ddiasfbafbiatreiasdfw WHERE  Estatus='1' AND Fecha <= now() ORDER BY Fecha DESC";
								$resultado = mysqli_query($con , $consulta);
								$contador=0;

								while($misdatos = mysqli_fetch_assoc($resultado)){ $contador++;?>
								<option data-subtext="<?php echo $misdatos["Autor"]; ?>"><?php echo $misdatos["Titulo"]; ?></option>
								<?php }?>
								</select>
							</div>
							<div class="inline-block cursor">
									<button id="submit" type="submit"><label onclick="buscar()" class="iconobuscar2 cursor"><i class="fa fa-search"></i></label></button>
								</div>
							</form>

		      		</div>
		      		<div class="col-md-1 float-right">
		      			<?php
		      			if (isset($_SESSION['Nombre'])) {
		      			echo "
		      			<img onmouseover='abriropciones()'  class='fotoperfil cursor' src='$Foto'>
		      			</div>
		      			<div id='opcionesuser' class='otromenu' onmouseover='abriropciones()' onmouseout='cerraropciones()'>
		      				<ul>
		      					<a class='cursor' href='$Link/cuenta'><li><i class='fas fa-user-circle'></i><span>Mi cuenta</span></li></a>
		      					<a class='cursor' href='$Link/cuenta'><li><i class='far fa-question-circle'></i><span>Ayuda</span></li></a>
		      					<a class='cursor' href='$Link/cuenta'><li><i class='fas fa-cog'></i><span>Configuración</span></li></a>
		      					<a class='cursor' href='$Link/Register/cerrarsesion.php'><li><i class='fas fa-sign-out-alt'></i><span>Cerrar Sesión</span></li></a>
		      				</ul>

		      			</div>";
		      		}
		      		else{
		      			echo "
		      			<img onmouseover='abriropciones()'  class='fotoperfil cursor' src='$Link/assets/images/user.png'>
		      			</div>
		      			<div id='opcionesuser' onmouseover='abriropciones()' onmouseout='cerraropciones()'>
		      				<ul>

		      					<a onclick='abrir()' class='cursor'><li><i class='fas fa-sign-in-alt'></i><span>Iniciar sesión</span></li></a>
		      				</ul>

		      			</div>";
		      			}
		      			?>



				</div>
			</div>
			<header class="hidden-phone" >
				<!-- navbar -->
				<nav class="navbar"><!-- add 'navbar-dark/navbar-grey/navbar-transparent/navbar-transparent-dark' -->
					<div class="container">
						<ul class="nav2"><!-- add 'dropdown-dark/dropdown-grey/dropdown-transparent/dropdown-transparent-dark' -->
							<!-- Basic link -->
							<li>
								<a class=" active " href="../" >Home</a>
							</li>

							<!-- Dropdown -->
							<li class="nav-item nav-megadropdown">
								<a class="" href="../investigacion">Investigación</a>
							</li>

							<!-- Sub Dropdown -->
							<li>
								<a class="" href="../membresias">Membresías</a>
							</li>

							<!-- Mega Dropdown -->
							<li>
								<a class="" href="../acercade">Acerca de</a>
							</li>
							<li>
								<a class="" href="../contacto">Contacto</a>
							</li>
						</ul><!-- end nav -->
						<!-- Nav Toggle button -->
						<button class="nav-toggle-btn">
				            <span class="lines"></span>
				        </button><!-- toggle button will show when screen resolution is less than 992px -->
					</div><!-- end container -->
				</nav><!-- end navbar -->
			</header>
			<header class="hidden-tablet hidden-desktop">
				<!-- navbar -->
				<nav class='navbar'><!-- add 'navbar-dark/navbar-grey/navbar-transparent/navbar-transparent-dark' -->
					<div class='container'>
						<a class='navbar-brand' href="<?php echo $Link; ?>">
						<img src="../assets/images/logo-globalitika.png" alt="Logo Globalitika">
						</a>
						<ul class="nav"><!-- add 'dropdown-dark/dropdown-grey/dropdown-transparent/dropdown-transparent-dark' -->
							<!-- Basic link -->
							<li class="nav-item">
								<a class="nav-link active" href="<?php echo $Link; ?>" >Home</a>
							</li>

							<!-- Dropdown -->
							<li class="nav-item nav-megadropdown">
							<a class="nav-link" href="<?php echo $Link; ?>/investigacion">Investigación</a>
							<div class="mega-menu">
								<div class="row">
									<!-- Mega menu column 1 -->
									<div class="col-12 col-lg-6" style="padding: 0 50px;">
										<h6 class="heading-uppercase margin-bottom-20">Categorias</h6>
										<ul class="list-unstyled">
											<?php
											$categorias=mysql_query("SELECT * FROM coewerw9hfwefouwefsdf ORDER BY Nombre_Categoria");
										while($fila=mysql_fetch_assoc($categorias)){
											@$ID=$fila["ID_Categoria"];
					                        @$Nombre=$fila["Nombre_Categoria"];
					                       echo" <li><a href='$Link/investigacion/?Categoria=$ID'>$Nombre</a></li>";
					                   }
					                         ?>
										</ul>
									</div>
									<!-- Mega menu column 2 -->
									<div class="col-12 col-lg-6" style="padding: 0 50px;">
										<h6 class="heading-uppercase margin-bottom-20">Temas populares</h6>
										<ul class="list-unstyled">
											<?php
											$temas=mysql_query("SELECT * FROM tr02j0wer82350j2asdw ORDER BY Nombre_Tema");
										while($fila=mysql_fetch_assoc($temas)){
											@$ID=$fila["ID_Tema"];
					                        @$Nombre=$fila["Nombre_Tema"];
					                       echo" <li><a href='$Link/investigacion/?Tema=$ID'>$Nombre</a></li>";
					                   }
					                         ?>
										</ul>
									</div>

								</div><!-- end row -->
							</div>
						</li>

							<!-- Sub Dropdown -->
							<li class="nav-item ">
								<a class="nav-link" href="<?php echo"$Link/"; ?>membresias">Membresías</a>
							</li>

							<!-- Mega Dropdown -->
							<li class="nav-item ">
								<a class="nav-link" href="<?php echo"$Link/#Nosotros"; ?>">Sobre nosotros</a>
							</li>
							<li class="nav-item ">
								<a class="nav-link" href="<?php echo"$Link/#sectioncontacto"; ?>">Contacto</a>
							</li>
						</ul><!-- end nav -->
						<!-- Nav Toggle button -->
						<button class="nav-toggle-btn">
				            <span class="lines"></span>
				        </button><!-- toggle button will show when screen resolution is less than 992px -->
					</div><!-- end container -->
				</nav><!-- end navbar -->
			</header>

			<!-- Scroll to top button -->
			<div class="scrolltotop">
				<a class="button-circle button-circle-sm button-circle-dark" href="#"><i class="ti-arrow-up"></i></a>
			</div>
			<!-- end Scroll to top button -->
			</div>

			<div id='fixed' style="top: 0;display: none;">
			<div class='container'>
				<div id="fondosesion">
					<div class="col-md-1 inline-block"></div>
					<div class="col-md-5 inline-block" style="padding: 50px 5px;">
						<h2 class="white">¿No tienes una cuenta?</h2>
						<p class="white font17">¡Crear una cuenta es gratis! Si quieres tener todos los beneficios de Globalitika, suscribete con la membresia que mas te guste.</p><br><br>
						<button class="botoncrear">Crear cuenta</button>
					</div>
					<div class="col-md-5 inline-block divform">

							<label>Ingresa</label>
							<div class="colorbajo" style=""></div><br><br>
							<input id="email" type="text" name="" placeholder="Correo">
							<input id="password" type="password" name="" placeholder="Contraseña">
							<a href="" class="olvidaste">Olvidaste tu contraseña?</a>
							<br> <span id="msg" style="color: red;text-shadow: none;font-size: 15px;position: absolute;margin-top: 10px;margin-left: 20px;"></span>
							<button id="but" onclick="Login()" class="ingresar">Ingresar</button>

					</div>
				</div>
				<br><br>
				<p onclick='cerrar()' class='cerrar'>X</p>

			</div>
			</div>
